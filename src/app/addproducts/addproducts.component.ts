import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormGroupName, FormControl, NgModel } from '@angular/forms';
import { DatacallsService } from '../datacalls.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as _ from "lodash";

@Component({
  selector: 'app-addproducts',
  templateUrl: './addproducts.component.html',
  styleUrls: ['./addproducts.component.css']
})
export class AddproductsComponent implements OnInit {
  isLinear = true;
  showLogo = [];
  product_id;
  sub_category_id;
  sub_category_id_from_api;
  categories;
  brands;
  product_ids=[];
  approve;
  calculated;
  uploadImg;
  imagepath
  uploadImg1;
  imagepath1;
  imagepath2;
  imagepath3;
  imagepath4;
  imagepath5
  products;
  type;
  data=[];
  data1=[];
  disabled;
  quantity=[];
  subcategories;
  subcategoriesfields;
  users;
  user;
  brand_id;
  duplicate;
  total=[];
  total_make;
  total_models;
  total_years;
  total_vehicles;
  usertype;
  dataSource;
  showPN = 'hidden';
  showProducts = 'hidden';
  checkProduct = [];
  checkProduct1 = [];
  categoryGroup: FormGroup;
  fieldsGroup: FormGroup;
  usersGroup: FormGroup;
  brandsGroup: FormGroup;
  productsGroup: FormGroup;
  mappingGroup: FormGroup;
  additionalGroup: FormGroup;
  config = new MatSnackBarConfig();

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  @ViewChild(MatSort) sort: MatSort;

  constructor(private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.usersGroup = this._formBuilder.group({
      usertype: ['']
    });
    this.brandsGroup = this._formBuilder.group({
      user_id: [''],
      addresses: this._formBuilder.array([
        this.initAddress(),
      ]),
    });
    this.fieldsGroup = this._formBuilder.group({
    });
    this.productsGroup = this._formBuilder.group({
      product_id: [0],
      category_id: [''],
      sub_category_id: [''],
      product_name: [''],
      product_type: [''],
      part_number: ['', Validators.compose([Validators.required, Validators.pattern(/^$|^[A-Za-z0-9]+$/)])],
      consumer_price: [''],
      retailer_price: [''],
      distributer_price: [''],
      all_quantity:[''],
      product_total_quantity: [''],
      description:[''],

      // description:['', Validators.compose([Validators.required, Validators.pattern(/^$|^[ '_A-Za-z0-9!#$%&*+-/=?^_{|}~]+$/)])],
      // gst: [''],
      consumer_price1: [{ value: '', disabled: true }],
      retailer_price1: [{ value: '', disabled: true }],
      distributer_price1: [{ value: '', disabled: true }],
      product_total_quantity1: [{ value: '', disabled: true }],
      weight:['', Validators.compose([Validators.required, Validators.pattern(/^$|^[.0-9]+$/)])],
      height:['', Validators.compose([Validators.required, Validators.pattern(/^$|^[.0-9]+$/)])],
      width:['', Validators.compose([Validators.required, Validators.pattern(/^$|^[.0-9]+$/)])],
      length:['', Validators.compose([Validators.required, Validators.pattern(/^$|^[.0-9]+$/)])],
    });
  } 

  ngOnInit() {

console.log('d',this.data)

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

if(this.usertype.user!='Admin'){
 
this.brandsGroup.value.user_id= this.usertype.user_id;
this.usersGroup.value.usertype = this.usertype.user;

console.log('userrrrrrrrrrrrrr',this.brandsGroup.value.user_id)
console.log('userrrrrrrrrrrrrr++5+5',this.usersGroup.value.usertype)
}


    this.product_id = this.activatedRoute.snapshot.params['id']
    if(this.usertype.user=='Admin'){
      this._DatacallsService.GETCategoryMaster(null, 0,null).subscribe(posts => {
        this.categories = posts.result;
      });
    }
    else{

      this._DatacallsService.view_category_user(this.usertype.user_id).subscribe(posts => {
        this.categories = posts.result;
      });
    }
   
if(this.usertype.user=='Admin'){
  this._DatacallsService.GETBrandMaster(null, 0,null).subscribe(posts => {
    this.brands = posts.result;
    console.log('user_ID ADMIN',this.usertype.user_id)
  });
}
else{
  this._DatacallsService.view_brand_user(this.usertype.user_id).subscribe(posts => {
    this.brands = posts.result;
    console.log('user_ID ---kiran',this.usertype.user_id)
  });
}
     

    // this._DatacallsService.GETUserMaster('Manufacturer', null).subscribe(posts => {
    //   this.users = posts.result;
    // });

    // this._DatacallsService.GETProductMaster(null, null, null, null).subscribe(posts => {
    //   this.products = posts.result;
    // }); 

    var make = [];
    this._DatacallsService.GETVehicleMaster(null, null, null, null, null, null, null, null, null, 0).subscribe(posts => {
      for (var i = 0; i < posts.result.length; i++) {
        make.push(posts.result[i].make);
      }
      this.total_make = _.uniqWith(make, _.isEqual);
      // console.log('total_make', this.total_make[0])
    });

    if (this.product_id != undefined) {
      this.showPN = 'true';
      this._DatacallsService.GETProductMaster(this.product_id, null, null, null, null, null, null, 0,null).subscribe(posts => {

        // this.user = posts.result[0].usertype;
        console.log('rhjxdcfvgbhnj gbhnjmk vgbhnjm',posts.result);

this.approve = posts.result[0].approve;

this.duplicate = posts.result[0].duplicate;


        if(posts.result[0].product_type=='Configurable'){

            // this.product_ids=[posts.result[0].configurable_products[0].product_group_id];
        for (var i = 0; i < posts.result[0].configurable_products.length; i++) {
          console.log('i',i)
          this.product_ids.push(posts.result[0].configurable_products[i].product_group_id);
        }

      // this.checkProduct.push(this.product_ids);
      // console.log('id already yet ahet',this.checkProduct)

        }
      

        console.log('duplicate',this.duplicate);
        console.log('configurable_products', this.product_ids);
        console.log('brand_details',posts.result[0].brand_details);
        this.subcategories = posts.result
        this.subcategoriesfields = posts.result
        this.sub_category_id_from_api = posts.result[0].sub_category_id


        var a = posts.result[0].product_image;
        var b = posts.result[0].product_image2;
        var c= posts.result[0].product_image3;
        var d = posts.result[0].product_image4;
        var e = posts.result[0].product_image5;
       
console.log('a',a)
console.log('b',b)
console.log('c',c)
console.log('d',d)
console.log('e',e)
this.showLogo =posts.result[0].images
this.imagepath = this.showLogo
console.log('posts.result[0].images',posts.result[0].images)
 
        // this.showLogo=[posts.result[0].product_image,posts.result[0].product_image2,posts.result[0].product_image3,posts.result[0].product_image4,posts.result[0].product_image5]
        
        // this.imagepath1 = posts.result[0].product_image
        // this.imagepath2 = posts.result[0].product_image2
        // this.imagepath3 = posts.result[0].product_image3
        // this.imagepath4 = posts.result[0].product_image4
        // this.imagepath5 = posts.result[0].product_image5
   
        console.log('showlogo',this.showLogo)
        this.productsGroup.patchValue({
          'product_id': posts.result[0].product_id,
           'user_id': posts.result[0].user_id,
          'category_id': posts.result[0].category_id,
          'sub_category_id': posts.result[0].sub_category_id,
          'product_name': posts.result[0].product_name, 
          'product_type': posts.result[0].product_type,
          'part_number': posts.result[0].part_number,
          'consumer_price': posts.result[0].consumer_price,
          'retailer_price': posts.result[0].retailer_price,
          'distributer_price': posts.result[0].distributer_price,
          'product_total_quantity': posts.result[0].product_total_quantity,
          "description":posts.result[0].description,
          // "gst": posts.result[0].gst,
          "approve":posts.result[0].approve,
          "weight":posts.result[0].weight,
          "height":posts.result[0].height,
          "width":posts.result[0].width,
          "length":posts.result[0].length
        })

        this.usersGroup.patchValue({
          'usertype': posts.result[0].usertype
        })  

        this.brandsGroup.patchValue({
          'user_id': posts.result[0].user_id
         
        })
        console.log('userid',posts.result[0].user_id);

        const control = <FormArray>this.brandsGroup.controls['addresses'];

        for (var i = 0; i < posts.result[0].brand_details.length; i++) {
            control.push(this._formBuilder.group({
            
            brand_id: posts.result[0].brand_details[i].brand_id,
          
          brand_reference: [posts.result[0].brand_details[i].brand_reference, Validators.compose([Validators.required, Validators.pattern(/^$|^[A-Za-z0-9]+$/)])]
          
          }));
          this.brand_id =posts.result[0].brand_details[0].brand_id;
         
        }
        this.removeAddress(0);

        this._DatacallsService.GETUserMaster(this.usertype.user, null, 0).subscribe(posts => {
          this.users = posts.result;
          console.log('brandsGroup', this.brandsGroup)
        });

        var inputs;
        var input_values;
        var dropdowns;
        var input_dropdowns;
        for (var i = 0; i < this.subcategoriesfields.length; i++) {
          for (var j = 0; j < this.subcategoriesfields[i].fields.inputFields.length; j++) {
            inputs = this.subcategoriesfields[i].fields.inputFields[j].input.replace(/\s/g, "")
            input_values = this.subcategoriesfields[i].fields.inputFields[j].value
            this.fieldsGroup.addControl(inputs, new FormControl(input_values));
            if (this.subcategoriesfields[i].fields.dropdownFields != null) {
              for (var k = 0; k < this.subcategoriesfields[i].fields.dropdownFields.length; k++) {
                dropdowns = this.subcategoriesfields[i].fields.dropdownFields[k].dropdown.replace(/\s/g, "")
                input_dropdowns = this.subcategoriesfields[i].fields.dropdownFields[k].value
                this.fieldsGroup.addControl(dropdowns, new FormControl(input_dropdowns));
              }
            }
          }
        }

        //configurable products ke liye
if(posts.result[0].configurable_products!=null){
  for(i=0;i<posts.result[0].configurable_products.length;i++){
 this.checkProduct.push(posts.result[0].configurable_products[i].product_group_id)
    this['all_quantity'+i] = posts.result[0].configurable_products[i].all_quantity

 var data=  {
      "id": posts.result[0].configurable_products[i].product_group_id,
      "quantity": posts.result[0].configurable_products[i].all_quantity
    }
    this.checkProduct1.push(data);
  }
  console.log('checkProduct for configurational product',this.checkProduct)
} 
// console.log('checkProduct for configurational product',this.checkProduct)
if(posts.result[0].product_type=='Configurable'){
  this.showProd('Configurable')
}

      });
    }


  }

  initAddress() {
    return this._formBuilder.group({
      brand_reference: ['', Validators.compose([Validators.required, Validators.pattern(/^$|^[A-Za-z0-9]+$/)])],
      brand_id: [''],
    });
    
  } 

  addAddress() {
    const control = <FormArray>this.brandsGroup.controls['addresses'];
    control.push(this.initAddress());
  }

  search(product_search){
    console.log('chaltoy',product_search);

    if(product_search==''){

      this._DatacallsService.view_product_filter_new(null).subscribe(posts => {
        this.products = posts.result;
     
      });
    }else{
      this._DatacallsService.view_product_filter_new(product_search).subscribe(posts => {
        this.products = posts.result;
     
      });
    }
   
  }

  removeAddress(i: number) {
    const control = <FormArray>this.brandsGroup.controls['addresses'];
    control.removeAt(i);
    //this.waypoints_details.splice(i, 1);
  }

  getModel(make) {
    var model = [];
    this._DatacallsService.GETVehicleMaster(null, this.mappingGroup.value.make, null, null, null, null, null, null, null, 0).subscribe(posts => {
      for (var i = 0; i < posts.result.length; i++) {
        model.push(posts.result[i].model);
      }
      this.total_models = _.uniqWith(model, _.isEqual);
      // console.log('total_make', this.total_models)
    });
  }

  getYear(model) {
    var year = [];
    // console.log('make', this.mappingGroup.value.make)
    // console.log('model', this.mappingGroup.value.model)
    this._DatacallsService.GETVehicleMaster(null, this.mappingGroup.value.make, this.mappingGroup.value.model, null, null, null, null, null, null, 0).subscribe(posts => {
      // console.log('posts', posts.result)
      for (var i = 0; i < posts.result.length; i++) {
        year.push(posts.result[i].model_year);
      }
      this.total_years = _.uniqWith(year, _.isEqual);
      // console.log('total_make', this.total_years)
    });
  }

  getVehicles(model) {
    var version = [];
    this._DatacallsService.GETVehicleMaster(null, this.mappingGroup.value.make, this.mappingGroup.value.model, this.mappingGroup.value.model_year, null, null, null, null, null, 0).subscribe(posts => {
      this.total_vehicles = posts.result;
    });
  }

  reset(){
    this.productsGroup.reset();
    this.brandsGroup.reset();
    this.fieldsGroup.reset();
  }

  getSubCategory(category_id) {
    this._DatacallsService.GETSubCategoryMaster(category_id, null, 0).subscribe(posts => {
      this.subcategories = posts.result;
    });
  }

  getSubCategoryFields(sub_category_id) {
    this.sub_category_id = sub_category_id
    this._DatacallsService.GETSubCategoryMaster(this.productsGroup.value.category_id, sub_category_id, 0).subscribe(posts => {
      this.subcategoriesfields = posts.result;
      this.showPN = 'true';

      var inputs;
      var dropdowns;
      for (var i = 0; i < this.subcategoriesfields.length; i++) {

        // console.log("inputs", this.subcategoriesfields)
        for (var j = 0; j < this.subcategoriesfields[i].fields.inputFields.length; j++) {
          inputs = this.subcategoriesfields[i].fields.inputFields[j].input.replace(/\s/g, "")
          this.fieldsGroup.addControl(inputs, new FormControl(''));
          if (this.subcategoriesfields[i].fields.dropdownFields != null) {
            for (var k = 0; k < this.subcategoriesfields[i].fields.dropdownFields.length; k++) {
              dropdowns = this.subcategoriesfields[i].fields.dropdownFields[k].dropdown.replace(/\s/g, "")
              this.fieldsGroup.addControl(dropdowns, new FormControl(''));
            }
          }
        }
      }
      // console.log('this.fieldsGroup', this.fieldsGroup)
    });
  }

  matcheck(a, id,value,total) {
    console.log('a',a);
    if (a == true) {
      // console.log('a',a);
      console.log('id',id);
      console.log('qty',value)

      var data2={
        "id":id,
        "quantity":value,
        "total":total
      }
      
this.total.push(data2);

var data={
  "id":id,
  "quantity":value
}


      this.checkProduct.push(data)
      console.log('checkProduct',this.checkProduct)
      this.calculate();
    }
    else {
      const index = this.checkProduct.indexOf(id);
      
        this.checkProduct.splice(index, 1);
        console.log('checkProduct',this.checkProduct) 
     
      // _.remove(this.checkProduct, { 'id': id});
   
      // console.log('checkProduct',abc)

      

      // const index = this.checkProduct.indexOf(id);
      // if (index !== -1) {
      //   this.checkProduct.splice(index, 1);
      //   console.log('checkProduct',this.checkProduct) 
      // }
      this.calculate();
    }
  }

  checkidexist(id){
// console.log('product_ids:-',this.product_ids)

for(var j = 0 ; j < this.product_ids.length ; j++){
console.log('j',j)
  if(this.product_ids[j] == id){
    return true;
  }
  else{
return false;
  }

}
  }

  getRadio(value) {
    console.log('data', value);
    this.type = value
    this._DatacallsService.GETUserMaster(this.type, null, 0).subscribe(posts => {
      this.users = posts.result;
    });
  }

  showProd(value) {
    if (value == 'Configurable') {
      this.showProducts = 'true';
      this._DatacallsService.view_product_master_combo(null, this.usertype.user == 'Admin' ? null : this.usertype.user_id, null, null, null, null, null, 0,0,this.product_id).subscribe(posts => {
        
        var box_checked = []
        // var box_unchecked = []
        for(var i=0;i < posts.result.length ;i++){
          for(var j=0;j < this.product_ids.length;j++){
            if(this.product_ids[j] == posts.result[i].product_id){
              box_checked.push(posts.result[i])
            }
            // else{
            //   box_unchecked.push(posts.result[i])
            // }
          }
        }

      var boxxxx=  _.difference(posts.result,box_checked)

console.log('uuuunchecked',boxxxx);

        this.products = [...box_checked , ...boxxxx];
       
        console.log('box all:',this.products)

        console.log('box checked:',this.product_ids)
        this.dataSource = new MatTableDataSource(this.products);
        this.dataSource.sort = this.sort;
      });
    }
    if (value == 'Single') {
      this.showProducts = 'false';
    }
  }



calculate(){

  

  for(var i=0;i<this.total.length;i++){
this.quantity.push(this.total[i].total-this.total[i].quantity)
  }
  console.log('quantity',this.quantity);
  
  
  var final_quantity = Math.min.apply(null,this.quantity);
   
  console.log('final quantity',final_quantity);

  this.productsGroup.patchValue({
    'product_total_quantity': final_quantity
  })

  var a= [];

  for(var i=0;i< this.checkProduct.length;i++){
   a.push(this.checkProduct[i].id)
  }

  console.log('a',a);
  var data={
    "product_id":a
  }
    this._DatacallsService.get_products_prices(data).subscribe(posts => {
      this.calculated = posts.result;
  console.log('checkproduct',this.checkProduct);

  this.productsGroup.patchValue({
    'consumer_price1': posts.result[0].consumer_price,
    'retailer_price1': posts.result[0].retailer_price,
    'distributer_price1': posts.result[0].distributer_price
     
  })
    }); 
  
}
  
  getDetails() {
    this._DatacallsService.GETUserMaster(null, null, 0).subscribe(posts => {
      this.subcategories = posts.result;
    });
  }

  
  uploadfile(event){
    
   
    this.uploadImg = event.file;
    // this.uploadImg1 = this.uploadImg1['name'].replace(/\s/g, "");
    // var a  = event.file.lastModified
    // var b  = event.file.lastModifiedDate
    // var c  = event.file.webkitRelativePath
    // var d  = event.file.size
    // var e  = event.file.type
    this.data.push(
      // {name: this.uploadImg1, lastModified: a, lastModifiedDate: b, webkitRelativePath: c, size: d, type:e}
      this.uploadImg
      )
    // .replace(/\s/g, "");
    // console.log("fileInput-------------------->",event.file)
    // this.uploadImg = {name: event.file.name.replace(/\s/g, ""), lastModified: a, lastModifiedDate: b, webkitRelativePath: c, size: d, type:e};

    console.log('imagesK', this.data);
    
    console.log('imagesAK', this.uploadImg);
  } 

//   onRemoved(event){
// console.log('aa',this.data);
//   }

  // onRemoved(event){
  //   console.log('event',event.src);
  //       if(event.src.startsWith("data:image/")){
  //          console.log("inside file if")
  //          console.log("data---1-->",this.data)
  //         this.data =  this.data.filter(e => e.name !== event.file.name);
  //          console.log("data---2-->",this.data)
  //       }
  //   else{
  //       console.log("event-------------->",event)
  //       this.uploadImg = this.uploadImg.filter(e => e !== event.src);
  //       console.log("this.sendImage------->",this.uploadImg)
  //   }
  //     }


  onRemoved(event){
   console.log("")
    if(event.src.startsWith("data:image/")){
           console.log("data---1-->",this.data)
          //  console.log('event',event.file.name.replace(/\s/g, ""))
          this.data =  this.data.filter(e => e.name !== event.file.name);
           console.log("data---2-->",this.data)
        }
        // this.data = this.data.filter(e => e !== event.name);
    else{
        console.log("event-------------->",event)
        this.imagepath = this.imagepath.filter(e => e !== event.src);
        console.log("this.imagepath------->",this.imagepath)
    }
      }
  
  // uploadImage(fileInput: any) {
  //   this.uploadImg = fileInput.target.files['0'];

  //   this.data.push(fileInput.target.files['0'])
  //   console.log('data',this.data);
  //   console.log('files', fileInput.target.files);
  //   console.log('logo', this.showLogo)
  // }

  onSubmit() {
    this._DatacallsService.GETSubCategoryMaster(this.productsGroup.value.category_id, this.sub_category_id == undefined ? this.sub_category_id_from_api : this.sub_category_id, 0).subscribe(posts => {
      for (var i = 0; i < posts.result.length; i++) {
        for (var j = 0; j < posts.result[i].fields.inputFields.length; j++) {
          for (var z in this.fieldsGroup.value) {
            if (this.fieldsGroup.value.hasOwnProperty(z)) {
              if (z == posts.result[i].fields.inputFields[j].input.replace(/\s/g, "")) {
                posts.result[i].fields.inputFields[j]["value"] = this.fieldsGroup.value[z]
                posts.result[i].fields.inputFields[j]["control_name"] = posts.result[i].fields.inputFields[j].input.replace(/\s/g, "")
              }
              if (posts.result[i].fields.dropdownFields != null) {
                for (var k = 0; k < posts.result[i].fields.dropdownFields.length; k++) {
                  if (z == posts.result[i].fields.dropdownFields[k].dropdown.replace(/\s/g, "")) {
                    posts.result[i].fields.dropdownFields[k]["value"] = this.fieldsGroup.value[z]
                    posts.result[i].fields.dropdownFields[k]["control_name"] = posts.result[i].fields.dropdownFields[k].dropdown.replace(/\s/g, "")
                  }
                }
              }
            }
          }
        }
      }

      var fields;
      fields = ({
        'inputFields': posts.result[0].fields.inputFields,
        'dropdownFields': posts.result[0].fields.dropdownFields
      });

      var reference;
      for (var i = 0; i < this.brandsGroup.value.addresses.length; i++) {
        reference = this.brandsGroup.value.addresses;
        console.log('reference',reference)
      }

      var usertype
      if (this.productsGroup.value.usertype == undefined) {
        usertype = this.usertype.user;
      }
      else {
        usertype = this.productsGroup.value.usertype
      }

      var user_id
      if (this.productsGroup.value.user_id == undefined) {
        user_id = this.usertype.user_id;
      }
      else {
        user_id = this.productsGroup.value.user_id
      }

      debugger
// console.log(this.data[0])
// var files1 = this.data[0]
// var files2 = this.data[1]
// var files3 = this.data[2]
// var files4 = this.data[3]
// var files5 = this.data[4]
var form: any = new FormData();
console.log('this.chekkkkkkk',this.checkProduct1)
debugger
      form.append("product_id", this.productsGroup.value.product_id)
      form.append("user_id", this.brandsGroup.value.user_id == "" || this.brandsGroup.value.user_id == undefined ? this.usertype.user_id : this.brandsGroup.value.user_id);
      form.append("category_id", this.productsGroup.value.category_id);
      form.append("sub_category_id", this.productsGroup.value.sub_category_id);
      form.append("usertype", this.type);
      form.append("product_name", this.productsGroup.value.product_name);
      form.append("product_type", this.productsGroup.value.product_type);
      form.append("part_number", this.productsGroup.value.part_number);
      // form.append("product_config", this.checkProduct == undefined ? "" : JSON.stringify(this.checkProduct).replace(/\[/g, '{').replace(/]/g, '}'));
      form.append("product_config",JSON.stringify(this.checkProduct1.length == 0 ? this.checkProduct : this.checkProduct1));
      form.append("reference", JSON.stringify(reference));
      form.append("product_type_quantity", null);
      form.append("product_total_quantity", this.productsGroup.value.product_total_quantity);
      form.append("description",this.productsGroup.value.description);
      // form.append("gst",this.productsGroup.value.gst);
      form.append("weight",this.productsGroup.value.weight);
      form.append("height",this.productsGroup.value.height);
      form.append("width",this.productsGroup.value.width);
      form.append("length",this.productsGroup.value.length);
      if(this.productsGroup.value.product_id==0){
        form.append("approve",0);
      }
      else{
        form.append("approve",this.approve);
      }

      for(let i =0; i < this.data.length; i++){
        form.append("file", this.data[i]);
      }
    // form.append("file1", files1);
      // form.append("file2", files2);
      // form.append("file3", files3);
      // form.append("file4", files4);
      // form.append("file5", files5);
      form.append("imagepath", JSON.stringify(this.imagepath ? this.imagepath : []));
      // form.append("imagepath2", this.imagepath2);
      // form.append("imagepath3", this.imagepath3);
      // form.append("imagepath4", this.imagepath4);
      // form.append("imagepath5", this.imagepath5);
      form.append("consumer_price", this.productsGroup.value.consumer_price);
      form.append("retailer_price", this.productsGroup.value.retailer_price);
      form.append("distributer_price", this.productsGroup.value.distributer_price);
      form.append("product_details", JSON.stringify(fields));
      form.append("barcode", "");
      form.append("show_products_users", 0);
      form.append("show_products_marketplace", 0);
      form.append("active_status", 1);

      if(this.brand_id == this.brandsGroup.value.addresses[0].brand_id && this.duplicate == 0){
        form.append("duplicate", 0);
      }
      else{
        form.append("duplicate", 1);
      }
      
       
 
      this._DatacallsService.POSTProductMaster(form).subscribe(posts => {
        
        if (posts.result) {
          // if (posts.result[0].insert_product_master == -1) {
          //   this.snackBar.open("Reference Number Already Exists", "Done", this.config);
          // }
          if (posts.result[0].insert_product_master == -2) {
            this.snackBar.open("Part Number Already Exists", "Done", this.config);
          }
          else {
            if (this.productsGroup.value.product_id == 0) {
              this.config.duration = 3000
              this.snackBar.open("Product Added Successfully", "Done", this.config);
            }
            else {
              this.config.duration = 3000
              this.snackBar.open("Product Edited Successfully", "Done", this.config);
            }
           
            if(this.approve==1){
              window.setTimeout(() => {
                this.Router.navigate(['approve-product']);
              }, 3000);
            }
            else{
              window.setTimeout(() => {
                this.Router.navigate(['products']);
              }, 3000);
            }
          }
        }
        else {
          this.snackBar.open("Please try again later", "Done");
        }
      });
    });
  }

}
