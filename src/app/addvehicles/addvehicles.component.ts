
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatacallsService } from '../datacalls.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-addvehicles',
  templateUrl: './addvehicles.component.html',
  styleUrls: ['./addvehicles.component.css']
})
export class AddvehiclesComponent implements OnInit {
  isLinear = true;
  vehicle_id = 0;
  cars;
  models;
  variants;
  sub_models;
  years;
  years_id;
  months;
  to_year;
  to_month;
  makes;
  sendMake;
  sendMakeApi;
  modelGroup: FormGroup;
  bodyGroup: FormGroup;
  engineGroup: FormGroup;
  drivetrainGroup: FormGroup;
  performanceGroup: FormGroup;
  config = new MatSnackBarConfig();

  constructor(private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.modelGroup = this._formBuilder.group({
      vehicle_id: [0],
      make_id: ['', Validators.required],
      model_id: ['', Validators.required],
      variant_id: [''],
      sub_model_id: [''],
      // year_id: [''],
      vin: [''],
      cc: [''],
      engine: [''],
      power: [''],
      cylinder: [''],
      valves: [''],
      cam_type: [''],
      vehicle_type: [''],                
      drive: [''],
      fuel: [''],
      fuel_type: [''],
      timing: [''],
      turbo_type: [''],
      country: [''],
      description: [''],
      // month:['']
    });
    this.bodyGroup = this._formBuilder.group({
      body_type: [''],
      no_of_doors: [''],
      no_of_seats: [''],
      engine_place: [''],
      drivetrain: [''],
      wheel_base: [''],
      track_front: [''],
      track_rear: [''],
      length: [''],
      width: [''],
      height: [''],
      curb_weight: [''],
      gross_weight: [''],
      roof_load: [''],
      cargo_space: [''],
      tow_weight: [''],
      gas_tank: ['']
    });
    this.engineGroup = this._formBuilder.group({
      cylinders: [''],
      displacement: [''],
      power_kw: [''],
      power_ps: [''],
      power_rpm: [''],
      torque_nm: [''],
      torque_rpm: [''],
      bore_x_stroke: [''],
      compression_ratio: [''],
      valves_per_cylinder: [''],
      crank_shaft: [''],
      fuel_injection: [''],
      supercharger: [''],
      catalytic: [''],
      manual: [''],
      automatic: ['']
    });
    this.drivetrainGroup = this._formBuilder.group({
      suspension_front: [''],
      suspension_rear: [''],
      assisted_steering: [''],
      turning_circle: [''],
      brakes_front: [''],
      brakes_rear: [''],
      abs: [''],
      esp: [''],
      tire_size: ['']
    });
    this.performanceGroup = this._formBuilder.group({
      zero_to_hundred: [''],
      max_speed: [''],
      fuel_efficiency: [''],
      engine_type: [''],
      fuel_type: ['']
    });
  }

  ngOnInit() {
    this.vehicle_id = this.activatedRoute.snapshot.params['id']

    this._DatacallsService.GETMakeMaster(null,0).subscribe(posts => {
      this.makes = posts.result;
    });

   

    if (this.vehicle_id != undefined) {
      this._DatacallsService.GETVehicleMaster(this.vehicle_id, null, null, null, null, null, null, null, null,0).subscribe(
        data => {
          console.log('data------------->',data.result[0])
          this.sendMakeApi = data.result[0].make_id;
          this.vehicle_id = data.result[0].vehicle_id;
          this.modelGroup.patchValue({
            'vehicle_id': data.result[0].vehicle_id,
            'make_id': data.result[0].make_id,
            'model_id': data.result[0].model_id,
            'model_year': data.result[0].model_year,
            'model_version': data.result[0].model_version,
            'chassis': data.result[0].chassis,
            "vin": data.result[0].vin,
            "cc": data.result[0].cc,
            "engine": data.result[0].engine,
            "power": data.result[0].power,
            "cylinder": parseInt(data.result[0].cylinder),
            "valves": parseInt(data.result[0].valves),
            "cam_type": data.result[0].cam_type,
            "vehicle_type": data.result[0].vehicle_type,
            "drive": data.result[0].drive,
            "fuel": data.result[0].fuel,
            "fuel_type": data.result[0].fuel_type,
            "timing": data.result[0].timing,
            "turbo_type": data.result[0].turbo_type,
            "country": data.result[0].country,
            "description": data.result[0].description,
            "month": data.result[0].month,
            "variant_id": data.result[0].variant_id,
            "sub_model_id": data.result[0].sub_model_id
          });
          this.bodyGroup.patchValue({
            'body_type': data.result[0].body_data.body_type,
            'no_of_doors': data.result[0].body_data.no_of_doors,
            'no_of_seats': data.result[0].body_data.no_of_seats,
            'engine_place': data.result[0].body_data.engine_place,
            'drivetrain': data.result[0].body_data.drivetrain,
            'wheel_base': data.result[0].body_data.wheel_base,
            'track_front': data.result[0].body_data.track_front,
            'track_rear': data.result[0].body_data.track_rear,
            'length': data.result[0].body_data.length,
            'width': data.result[0].body_data.width,
            'height': data.result[0].body_data.height,
            'curb_weight': data.result[0].body_data.curb_weight,
            'gross_weight': data.result[0].body_data.gross_weight,
            'roof_load': data.result[0].body_data.roof_load,
            'cargo_space': data.result[0].body_data.cargo_space,
            'tow_weight': data.result[0].body_data.tow_weight,
            'gas_tank': data.result[0].body_data.gas_tank
          });
          this.engineGroup.patchValue({
            'cylinders': data.result[0].engine_data.cylinders,
            'displacement': data.result[0].engine_data.displacement,
            'power_kw': data.result[0].engine_data.power_kw,
            'power_ps': data.result[0].engine_data.power_ps,
            'power_rpm': data.result[0].engine_data.power_rpm,
            'torque_nm': data.result[0].engine_data.torque_nm,
            'torque_rpm': data.result[0].engine_data.torque_rpm,
            'bore_x_stroke': data.result[0].engine_data.bore_x_stroke,
            'compression_ratio': data.result[0].engine_data.compression_ratio,
            'valves_per_cylinder': data.result[0].engine_data.valves_per_cylinder,
            'crank_shaft': data.result[0].engine_data.crank_shaft,
            'fuel_injection': data.result[0].engine_data.fuel_injection,
            'supercharger': data.result[0].engine_data.supercharger,
            'catalytic': data.result[0].engine_data.catalytic,
            'manual': data.result[0].engine_data.manual,
            'automatic': data.result[0].engine_data.automatic
          });
          this.drivetrainGroup.patchValue({
            'suspension_front': data.result[0].drivetrain_data.suspension_front,
            'suspension_rear': data.result[0].drivetrain_data.suspension_rear,
            'assisted_steering': data.result[0].drivetrain_data.assisted_steering,
            'turning_circle': data.result[0].drivetrain_data.turning_circle,
            'brakes_front': data.result[0].drivetrain_data.brakes_front,
            'brakes_rear': data.result[0].drivetrain_data.brakes_rear,
            'abs': data.result[0].drivetrain_data.abs,
            'esp': data.result[0].drivetrain_data.esp,
            'tire_size': data.result[0].drivetrain_data.tire_size
          });
          this.performanceGroup.patchValue({
            'zero_to_hundred': data.result[0].performance_data.zero_to_hundred,
            'max_speed': data.result[0].performance_data.max_speed,
            'fuel_efficiency': data.result[0].performance_data.fuel_efficiency,
            'engine_type': data.result[0].performance_data.engine_type,
            'fuel_type': data.result[0].performance_data.fuel_type,
          });

          this._DatacallsService.GETModelMaster(data.result[0].model_id, data.result[0].make_id,0).subscribe(posts => {
            for (var i = 0; i < posts.result.length; i++) {
              this.variants = posts.result[i].variant_name;
              console.log('data---->v',this.variants)
            }
          }); 

          this._DatacallsService.GETModelMaster(null, null,0).subscribe(posts => {
            this.models = posts.result;
            this.years = posts.result;
            this.makes = posts.result;
            console.log('Years', this.years)
          });

          

          this._DatacallsService.GETMakeMaster(null,0).subscribe(posts => {
            this.cars = posts.result;
            // for (var i =0; i < posts.result.length; i++) {
            //   this.cars.push(posts.result[i].car_name)
            // }
            console.log('cars', this.cars)
          });
        }
      );
    }
  }

  getModel(make_id) {
    this._DatacallsService.GETModelMaster(null, make_id,0).subscribe(posts => {
      this.models = posts.result;
    });
  }

  getVariant(model_id) {
    this._DatacallsService.GETModelMaster(model_id, this.modelGroup.value.make_id,0).subscribe(posts => {
      for (var i = 0; i < posts.result.length; i++) {
        this.variants = posts.result[i].variant_name;
      }
    });
  }

  getSubModel(make) {
    this._DatacallsService.GETSubModelMaster(null, this.modelGroup.value.variant_id, this.modelGroup.value.model_id, this.modelGroup.value.make_id,0).subscribe(posts => {
      this.sub_models = posts.result;
      for (var i = 0; i < posts.result.length; i++) {
        this.years = posts.result[i].year_name;
        this.years_id = posts.result[i].year_name[0].year_id;
        this.months = posts.result[i].year_name[0].month;
        this.to_year = posts.result[i].year_name[0].to_year;
        this.to_month = posts.result[i].year_name[0].to_month;

        console.log('years',this.years);
        console.log('years_id',this.years_id);
        console.log('months',this.months);
        console.log('to_year',this.to_year);
        console.log('to_month',this.to_month);
      }
    });
  }

  onSubmit() {
    // var model_data = {
    //   'make': this.modelGroup.value.make,
    //   "model": this.modelGroup.value.model,
    //   "model_years": this.modelGroup.value.model_years,
    //   "model_body_years": this.modelGroup.value.model_body_years,
    //   "model_version": this.modelGroup.value.model_version,
    //   "model_version_years": this.modelGroup.value.model_version_years,
    //   "sold_in": this.modelGroup.value.sold_in,
    //   "model_classification": this.modelGroup.value.model_classification
    // }

    var body_data = {
      "body_type": this.bodyGroup.value.body_type,
      "no_of_doors": this.bodyGroup.value.no_of_doors,
      "no_of_seats": this.bodyGroup.value.no_of_seats,
      "engine_place": this.bodyGroup.value.engine_place,
      "drivetrain": this.bodyGroup.value.drivetrain,
      "wheel_base": this.bodyGroup.value.wheel_base,
      "track_front": this.bodyGroup.value.track_front,
      "track_rear": this.bodyGroup.value.track_rear,
      "length": this.bodyGroup.value.length,
      "width": this.bodyGroup.value.width,
      "height": this.bodyGroup.value.height,
      "curb_weight": this.bodyGroup.value.curb_weight,
      "gross_weight": this.bodyGroup.value.gross_weight,
      "roof_load": this.bodyGroup.value.roof_load,
      'cargo_space': this.bodyGroup.value.cargo_space,
      'tow_weight': this.bodyGroup.value.tow_weight,
      'gas_tank': this.bodyGroup.value.gas_tank
    }

    var engine_data = {
      "cylinders": this.engineGroup.value.cylinders,
      "displacement": this.engineGroup.value.displacement,
      "power_kw": this.engineGroup.value.power_kw,
      "power_ps": this.engineGroup.value.power_ps,
      "power_rpm": this.engineGroup.value.power_rpm,
      "torque_nm": this.engineGroup.value.torque_nm,
      "torque_rpm": this.engineGroup.value.torque_rpm,
      "bore_x_stroke": this.engineGroup.value.bore_x_stroke,
      "compression_ratio": this.engineGroup.value.compression_ratio,
      "valves_per_cylinder": this.engineGroup.value.valves_per_cylinder,
      "crank_shaft": this.engineGroup.value.crank_shaft,
      "fuel_injection": this.engineGroup.value.fuel_injection,
      "supercharger": this.engineGroup.value.supercharger,
      "catalytic": this.engineGroup.value.catalytic,
      "manual": this.engineGroup.value.manual,
      "automatic": this.engineGroup.value.automatic
    }

    var drivetrain_data = {
      "suspension_front": this.drivetrainGroup.value.suspension_front,
      "suspension_rear": this.drivetrainGroup.value.suspension_rear,
      "assisted_steering": this.drivetrainGroup.value.assisted_steering,
      "turning_circle": this.drivetrainGroup.value.turning_circle,
      "brakes_front": this.drivetrainGroup.value.brakes_front,
      "brakes_rear": this.drivetrainGroup.value.brakes_rear,
      "abs": this.drivetrainGroup.value.abs,
      "esp": this.drivetrainGroup.value.esp,
      "tire_size": this.drivetrainGroup.value.tire_size
    }

    var performance_data = {
      "zero_to_hundred": this.performanceGroup.value.zero_to_hundred,
      "max_speed": this.performanceGroup.value.max_speed,
      "fuel_efficiency": this.performanceGroup.value.fuel_efficiency,
      "engine_type": this.performanceGroup.value.engine_type,
      "fuel_type": this.performanceGroup.value.fuel_type
    }

    var bc = this.modelGroup.value.vehicle_id;
    var form: any = new FormData();

    console.log('test', this.sendMake)

    console.log('variant_id',this.modelGroup.value.variant_id); 
    console.log('sub_model_id',this.modelGroup.value.sub_model_id); 
    console.log('years_id',this.years_id); 
    
    form.append("vehicle_id", this.modelGroup.value.vehicle_id);
    form.append("make_id", this.modelGroup.value.make_id);
    form.append("model_id", this.modelGroup.value.model_id);
    form.append("variant_id", this.modelGroup.value.variant_id == '' ? null:this.modelGroup.value.variant_id);
    form.append("sub_model_id", this.modelGroup.value.sub_model_id == '' ? null: this.modelGroup.value.sub_model_id);
    form.append("year_id", this.years_id == undefined ? null:  this.years_id);
    form.append("vin", this.modelGroup.value.vin);
    form.append("cc", this.modelGroup.value.cc);
    form.append("engine", this.modelGroup.value.engine);
    form.append("power", this.modelGroup.value.power);
    form.append("cylinder", this.modelGroup.value.cylinder);
    form.append("valves", this.modelGroup.value.valves);
    form.append("cam_type", this.modelGroup.value.cam_type);
    form.append("vehicle_type", this.modelGroup.value.vehicle_type);
    form.append("drive", this.modelGroup.value.drive);
    form.append("fuel", this.modelGroup.value.fuel);
    form.append("fuel_type", this.modelGroup.value.fuel_type);
    form.append("timing", this.modelGroup.value.timing);
    form.append("turbo_type", this.modelGroup.value.turbo_type);
    form.append("country", this.modelGroup.value.country);
    form.append("month", this.months);
    form.append("to_year", this.to_year);
    form.append("to_month", this.to_month);
    form.append("from_year", this.years);
    form.append("description", "");
    form.append("body_data", JSON.stringify(body_data));
    form.append("engine_data", JSON.stringify(engine_data));
    form.append("drivetrain_data", JSON.stringify(drivetrain_data));
    form.append("performance_data", JSON.stringify(performance_data));

    this._DatacallsService.POSTVehicleMaster(form).subscribe(posts => {
      if (posts.result) {
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Vehicle Added Successfully", "Done", this.config);
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Vehicle Edited Successfully", "Done", this.config);
        }
        window.setTimeout(() => {
          this.Router.navigate(['vehicles']);
        }, 3000);
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });

  }

}