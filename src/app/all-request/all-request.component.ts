import { Component, OnInit, NgModule,ElementRef,ViewChild, Inject } from '@angular/core';
import {DatacallsService} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router'
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
// import {HttpClient} from '@angular/common/http';
// import {merge, Observable, of as observableOf} from 'rxjs';
// import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {SelectionModel} from '@angular/cdk/collections';
import {deleteobj} from '../deletearray'

export interface UserData {
  id: string;
  name: string;
  mobile_no: string;
  password: string;
  email:string;
  city:string;
  state:string;
  country:string
}

@Component({
  selector: 'app-all-request',
  templateUrl: './all-request.component.html',
  styleUrls: ['./all-request.component.css']
})
export class AllRequestComponent implements OnInit {
  sel=false;
  jsonPara3={};
  jsonPara4={};
  usertype;
  idarray=[];
  allsel=false;
  intsel=false;
  displayedColumns = ['make_request_id', 'name', 'mobile_no','email', 'address','country','unique_code','action'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
   posts;
   checkProduct = [];
   resultsLength = 0;
   checkDistributor = [];
   json={};
   filteredData;

   isLoadingResults = true;
   isRateLimitReached = false;

   applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  constructor(private _DatacallsService:DatacallsService,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute,private breakpointObserver: BreakpointObserver,private fb:FormBuilder,private Router:Router,public dialog: MatDialog, public snackBar: MatSnackBar) { 

  }

  ngOnInit() {


    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    var form = {
        "receiver_id" :this.usertype.user_id,
        "status":0
    }
    this._DatacallsService.view_make_request(form).subscribe(posts => {
      console.log(posts);
       
      this.posts = posts.result
   
      console.log('result',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
       
           
     });

  }

  sendIDS(a, id) {
    if (a == true) {
      console.log('id araha hai',id);
      // this.json={
      //   "sender_id":this.usertype.user_id,
      //   "receiver_id":id
      // }

      this.checkDistributor.push(id)
      console.log('data',this.checkDistributor)
    }
    else {
      
      const index = this.checkDistributor.indexOf(id);
      if (index !== -1) {
        this.checkDistributor.splice(index, 1);
      } 
      console.log('deletearray :',this.checkDistributor); 
    }

  }


  request() {

    if(this.checkDistributor.length==0){
      this.snackBar.open('please select atleast one request.', '', {
        duration: 4000

      });
    }
    else{
    

var form ={
  "make_request_id" :this.checkDistributor
}
    this._DatacallsService.change_make_request(form).subscribe(posts => {
      console.log(posts);
       
      this.posts = posts.result
       
      if(this.posts){

        this.snackBar.open('Request Accept Successfully', '', {
          duration: 4000
  
        });
        this.ngOnInit();
      }
      else{

        this.snackBar.open('Something got wrong', '', {
          duration: 4000
  
        });

      }
     });
    }
  }
 

 

}




