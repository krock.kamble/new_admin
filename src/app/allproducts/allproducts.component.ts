
import { DatacallsService } from '../datacalls.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';

import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-allproducts',
  templateUrl: './allproducts.component.html',
  styleUrls: ['./allproducts.component.css']
})
export class AllproductsComponent implements OnInit {
  posts;
  posts1;
  posts2;
  posts3; 
  posts4;
  kiran;
  searchForm;
  json={};
  brand_id;
  category_id;
  sub_category_id;
  postsSearch;
  totalPosts;
  jsonPara3 = {};
  jsonPara4 = {};
  dataSource;
  totalPostsSearch;
  checkProduct = [];
  usertype;
  type;
  config = new MatSnackBarConfig();
  showPosts = 12;
  finished = 'true';

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _fb: FormBuilder,private _DatacallsService: DatacallsService,private Router: Router,public snackBar: MatSnackBar) {
    
    this.searchForm = this._fb.group({
      type:[''],
      brand_id:[''],
      category_id:[''],
      sub_category_id:['']
  });

   }

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    this._DatacallsService.GETProductMaster(null,null, null, null, null, null, null,0,0).subscribe(posts => 
        {

      console.log('posts.result :', posts.result)

      // this.totalPosts = posts.result;
      // if (this.totalPosts != undefined) {
      //   this.posts = posts.result.slice(0, this.showPosts);
      //   if (this.totalPosts.length == 0) {
      //     this.finished = 'undefined';
      //   }
      // }
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'))
//user datacalls
    this._DatacallsService.GETUserMaster(this.usertype.user, this.usertype.user_id,0).subscribe(posts => {
      this.kiran = posts.result;
      console.log('GETUserMaster',this.kiran[0].usertype);
      console.log('data yetoy',this.kiran);
    });

    this._DatacallsService.GETUserMaster(null,null,0).subscribe(posts => {
      this.posts4 = posts.result;
      console.log('GETUserMaster',this.kiran[0].usertype);
      console.log('data yetoy',this.kiran);
    });


//brand datacalls
    this._DatacallsService.GETBrandMaster(null,0,null).subscribe(posts => {
      this.posts1 = posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
//category datacalls
    this._DatacallsService.GETCategoryMaster(null,0,null).subscribe(posts => {
      this.posts2 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //subcategory datacalls
    this._DatacallsService.GETSubCategoryMaster(null, null,0).subscribe(posts => {
      this.posts3 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }


  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        type: [''],
        brand_id: [''],
        category_id: [''],
        sub_category_id: ['']
      })
    }
  }

  
  sendIDS(a, id) { 
    if (a == true) {
      console.log('a',a);
      console.log('id',id);
      this.checkProduct.push(id)
    }
    else {
      const index = this.checkProduct.indexOf(id);
      if (index !== -1) {
        this.checkProduct.splice(index, 1);
      }
    }
    //this.shareData(this.checkProduct);
  } 

  shareData(data) {
    this._DatacallsService.sendAnything(data) // to send something
  }

  delete(){
    var data={
      "delete_status":1,
      "product_id":this.checkProduct
    }
      this._DatacallsService.DELETEProductMaster(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.Router.navigate(['deleted-product']);
      });
    
      
    }
    onchange(a, id) {

      console.log(a)
      console.log(id)
  
      if (a == true && (this.kiran[0].usertype == 'Distributor'||this.kiran[0].usertype=='Manufacturer') && this.kiran[0].allow_user==1) {
        console.log('user type',this.kiran[0].usertype)
        this.jsonPara3 = { "online_status": 1, "product_id": id }
        this._DatacallsService.change_status(this.jsonPara3).subscribe(posts => {
          this.posts = posts.result
          this.snackBar.open('you allow this produt to sale on E-commerce portal.', '', {
            duration: 4000
           
          });
        });
      }
      else {
        console.log('direct ithe yetoy')
  if(a== false && (this.kiran[0].usertype == 'Distributor'||this.kiran[0].usertype=='Manufacturer') && this.kiran[0].allow_user==1){
    this.jsonPara4 = { "online_status": 0, "product_id": id }
    this._DatacallsService.change_status(this.jsonPara4).subscribe(posts => {
      this.posts = posts.result
      this.snackBar.open('you are not allowing this products to sale on E-commerce portal.', '', {
        duration: 4000
       
      });
    });
  }
  else{
    this.snackBar.open('you are not allow to sale products on E-commerce portal.', '', {
      duration: 4000
     
    });
  }
       
      }
    }


    onSubmit(){
      this.type=this.searchForm.value.type==''?null:this.searchForm.value.type;
      console.log('usertype',this.type);
      console.log('usertype1',this.searchForm.value.type)
      this.brand_id=this.searchForm.value.brand_id==''?null:this.searchForm.value.brand_id;
      this.category_id=this.searchForm.value.category_id==''?null:this.searchForm.value.category_id;
      this.sub_category_id=this.searchForm.value.sub_category_id==''?null:this.searchForm.value.sub_category_id
      this.json={
        "product_id":null,
        "user_id":null,
        "usertype":this.type,
        "brand_id":this.brand_id,
        "category_id":this.category_id,
        "sub_category_id":this.sub_category_id,
        "show_products_users":null,
        "delete_status":0,
        "approve":0
      }
      this._DatacallsService.POST_ProductMaster(this.json).subscribe(posts => {
        
        this.posts = posts.result;
        this.dataSource = new MatTableDataSource(this.posts);
        this.dataSource.sort = this.sort;
        if (this.posts != undefined) {
          this.dataSource.paginator = this.paginator;
        }
      });
      
       
      
      
    }





} 

			  
			