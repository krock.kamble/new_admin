import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';
import { FormControl, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
import { AuthguardGuard } from './authguard.guard';
import { AgmCoreModule } from '@agm/core';

import {
  
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatCardModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatChipsModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTabsModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { ImageUploadModule } from 'angular2-image-upload';
import { AngularGooglePlaceModule } from 'angular-google-place';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AmazingTimePickerModule } from 'amazing-time-picker';

import { AppComponent } from './app.component';
import { PublicComponent } from './public/public.component';
import { SecureComponent } from './secure/secure.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { AddvehiclesComponent } from './addvehicles/addvehicles.component';
import { UsersComponent,UsersDialog ,DetailDialog} from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { MFDashboardComponent } from './mf-dashboard/mf-dashboard.component';
import { AddproductsComponent } from './addproducts/addproducts.component';
import { CategoriesComponent } from './categories/categories.component';
import { SubcategoriesComponent } from './subcategories/subcategories.component';
import { MakeComponent } from './make/make.component';
import { ModelComponent } from './model/model.component';
import { MapproductsComponent } from './mapproducts/mapproducts.component';
import { MappedlistComponent } from './mappedlist/mappedlist.component';
import { BrandsComponent } from './brands/brands.component';
import { SubmodelComponent } from './submodel/submodel.component';
import{DistributerlistComponent} from './distributerlist/distributerlist.component';
import { OrderComponent,OrdersDialog,ComboDialog } from './order/order.component';
import { MapdistributorComponent } from './mapdistributor/mapdistributor.component';
import { MappeddistributorComponent } from './mappeddistributor/mappeddistributor.component';
import { ApprovedproductComponent } from './approvedproduct/approvedproduct.component';
import { MapusersComponent } from './mapusers/mapusers.component';
import { MappedusersComponent,catDialog } from './mappedusers/mappedusers.component';
import { AllproductsComponent } from './allproducts/allproducts.component';
import { DeletedBrandComponent } from './deleted-brand/deleted-brand.component';
import { DeletedCategoryComponent } from './deleted-category/deleted-category.component';
import { DeletedSubcategoryComponent } from './deleted-subcategory/deleted-subcategory.component';
import { DeletedMakeComponent } from './deleted-make/deleted-make.component';
import { DeletedModelComponent } from './deleted-model/deleted-model.component';
import { DeletedSubmodelComponent } from './deleted-submodel/deleted-submodel.component';
import { DeletedProductComponent } from './deleted-product/deleted-product.component';
import { DeletedUserComponent } from './deleted-user/deleted-user.component';
import { DeletedCarComponent } from './deleted-car/deleted-car.component';
import { OnlineProductsComponent } from './online-products/online-products.component';
import { DDashboardComponent } from './d-dashboard/d-dashboard.component';
import { ExcelComponent } from './excel/excel.component';
import { ApproveProductComponent } from './approve-product/approve-product.component';
import { WeightFreightComponent } from './weight-freight/weight-freight.component';
import { CubicFreightComponent } from './cubic-freight/cubic-freight.component';
import { AddBrandComponent } from './add-brand/add-brand.component';
import { CountryComponent } from './country/country.component';
import { DeletedCountryComponent } from './deleted-country/deleted-country.component';
import { ReviewComponent } from './review/review.component';
import { MessagesComponent } from './messages/messages.component';
import { BarRatingModule } from "ngx-bar-rating";
import { MakeRequestComponent } from './make-request/make-request.component';
import { AllRequestComponent } from './all-request/all-request.component';
import { MapComponent } from './map/map.component';
import { ProductComponent } from './product/product.component';
@NgModule({
  declarations: [
    
    AppComponent,
    PublicComponent,
    SecureComponent,
    DashboardComponent,
    SidebarComponent,
    LoginComponent,
    VehiclesComponent,
    AddvehiclesComponent,
    UsersComponent,
    ProductsComponent,
    MFDashboardComponent,
    AddproductsComponent,
    CategoriesComponent,
    SubcategoriesComponent,
    MakeComponent,
    ModelComponent,
    catDialog,
    MapproductsComponent,
    MappedlistComponent,
    BrandsComponent,
    SubmodelComponent,
    DistributerlistComponent,
    OrderComponent,
    OrdersDialog,
    UsersDialog,
    DetailDialog,
    ComboDialog,
    MapdistributorComponent,
    MappeddistributorComponent,
    ApprovedproductComponent,
    MapusersComponent,
    MappedusersComponent,
    AllproductsComponent,
    DeletedBrandComponent,
    DeletedCategoryComponent,
    DeletedSubcategoryComponent,
    DeletedMakeComponent,
    DeletedModelComponent,
    DeletedSubmodelComponent,
    DeletedProductComponent,
    DeletedUserComponent,
    DeletedCarComponent,
    OnlineProductsComponent,
    DDashboardComponent,
    ExcelComponent,
    ApproveProductComponent,
    WeightFreightComponent,
    CubicFreightComponent,
    AddBrandComponent,
    CountryComponent,
    DeletedCountryComponent,
    ReviewComponent,
    MessagesComponent,
    MakeRequestComponent,
    AllRequestComponent,
    MapComponent,
    ProductComponent
    
  ],
  imports: [
    BarRatingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    HttpModule,
    BrowserAnimationsModule,
    ImageUploadModule.forRoot(),
    AngularGooglePlaceModule,
    InfiniteScrollModule,
    AmazingTimePickerModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatChipsModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRippleModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTooltipModule,
    MatStepperModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTreeModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCtXGu9afmvDY58V3CnJdT1eOMQ1pUsgn8',
      libraries: ["places"]
    })
  ],
  entryComponents: [catDialog,OrdersDialog,ComboDialog,UsersDialog,DetailDialog],
  providers: [ProductsComponent, appRoutingProviders, AuthguardGuard, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
