import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core'

import { PublicComponent } from './public/public.component';
import { SecureComponent } from './secure/secure.component';

import { PUBLIC_ROUTES } from './public.routing';
import { SECURE_ROUTES } from './secure.routing';

import { AuthguardGuard } from './authguard.guard';

export const appRouter: Routes = [
    { path:'', redirectTo: 'login', pathMatch: 'full'},
    { path: '', component: PublicComponent,  children: PUBLIC_ROUTES , canActivate: [AuthguardGuard]},
    { path: '', component: SecureComponent,  children: SECURE_ROUTES } ,
]

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRouter);