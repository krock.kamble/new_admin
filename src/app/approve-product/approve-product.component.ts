import { DatacallsService } from '../datacalls.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { deleteobj, deleteintarray } from '../deletearray'
import { element } from '../../../node_modules/protractor';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-approve-product',
  templateUrl: './approve-product.component.html',
  styleUrls: ['./approve-product.component.css']
})
export class ApproveProductComponent implements OnInit {

   
  posts;
  posts1;
  posts2;
  posts3;
  posts4;
  posts5;
  kiran;
  array=[];
  product_id=[];
  searchForm;
  json = {};
  sel = false;
  allsel = false;
  intsel = false;
  brand_id;
  category_id;
  sub_category_id;
  postsSearch;
  totalPosts;
  jsonPara3 = {};
  jsonPara4 = {};
  dataSource;
  totalPostsSearch;
  checkProduct = [];
  usertype;
  public productForm: FormGroup;
  type;
  config = new MatSnackBarConfig();
  showPosts = 12;
  finished = 'true';
  filteredData;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _fb: FormBuilder, private _DatacallsService: DatacallsService, private Router: Router, public snackBar: MatSnackBar) {

    this.searchForm = this._fb.group({
      type: [''],
      brand_id: [''],
      category_id: [''],
      sub_category_id: ['']
    });

    this.productForm = this._fb.group({

      Excelfile: ['', Validators.required]

  });

  }

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

   
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'))
    //user datacalls
    this._DatacallsService.GETUserMaster(this.usertype.user, this.usertype.user_id, 0).subscribe(posts => {
      this.kiran = posts.result;
      console.log('GETUserMaster', this.kiran[0].usertype);
      console.log('data yetoy', this.kiran);
    });



    //brand datacalls
    this._DatacallsService.GETBrandMaster(null, 0,null).subscribe(posts => {
      this.posts1 = posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //category datacalls
    this._DatacallsService.GETCategoryMaster(null, 0,null).subscribe(posts => {
      this.posts2 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //subcategory datacalls
    this._DatacallsService.GETSubCategoryMaster(null, null, 0).subscribe(posts => {
      this.posts3 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    this._DatacallsService.GETProductMaster(null, this.usertype.user == 'Admin' ? null : this.usertype.user_id, null, null, null, null, null, 0,1).subscribe(posts => {

      // console.log('posts.result :', posts.result)

      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    }); 
  }

  alltogglecheck(b, array) {
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.product_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.product_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel = !this.sel
      this.checkProduct = []
      console.log('unchaked', b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai', id);


      this.checkProduct.push(id)
      console.log('data', this.checkProduct)
    }
    else {
      this.checkProduct = deleteintarray(this.checkProduct, id)
      console.log('deletearray :', this.checkProduct);

    }
    console.log('check thi list ', this.checkProduct)
    console.log('length ', length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel = !this.intsel && this.checkProduct.length != 0
    console.log('intsel : ' + this.intsel + ' && allsel:', this.allsel)
  }








  // sendIDS(a, id) { 
  //   if (a == true) {
  //     console.log('a',a);
  //     console.log('id',id);
  //     this.checkProduct.push(id)
  //   }
  //   else {
  //     const index = this.checkProduct.indexOf(id);
  //     if (index !== -1) {
  //       this.checkProduct.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // } 

  shareData(data) {
    this._DatacallsService.sendAnything(data) // to send something
  }

  delete() {
    var data = {
      "delete_status": 1,
      "product_id": this.checkProduct
    }
    this._DatacallsService.DELETEProductMaster(data).subscribe(posts => {
      console.log('checkproduct', this.checkProduct);
      this.Router.navigate(['deleted-product']);
    });


  }
  onchange(a, id) {

    console.log(a)
    console.log(id)

    if (a == true && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer') && this.kiran[0].allow_user == 1) {
      console.log('user type', this.kiran[0].usertype)
      this.jsonPara3 = { "online_status": 1, "product_id": id }
      this._DatacallsService.change_status(this.jsonPara3).subscribe(posts => {
        this.posts = posts.result
        this.snackBar.open('you allow this produt to sale on E-commerce portal.', '', {
          duration: 4000

        });
      });
    }
    else {
      console.log('direct ithe yetoy')
      if (a == false && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer') && this.kiran[0].allow_user == 1) {
        this.jsonPara4 = { "online_status": 0, "product_id": id }
        this._DatacallsService.change_status(this.jsonPara4).subscribe(posts => {
          this.posts = posts.result
          this.snackBar.open('you are not allowing this products to sale on E-commerce portal.', '', {
            duration: 4000

          });
        });
      }
      else {
        this.snackBar.open('you are not allow to sale products on E-commerce portal.', '', {
          duration: 4000

        });
      }

    }
  }

  change_all_product_status() {
console.log('checkproduct159753', this.checkProduct.length);
    if (this.checkProduct.length!=0 && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer') && this.kiran[0].allow_user == 1) {
      console.log('user type', this.kiran[0].usertype)
      var data = {
        "product_id": this.checkProduct
      }
      this._DatacallsService.change_all_status(data).subscribe(posts => {
        this.posts = posts.result
        console.log('checkproduct', this.checkProduct);
        this.snackBar.open('you allow all produts to sale on E-commerce portal.', '', {
          duration: 4000
        });
        setTimeout(function(){
          location.reload();
        },2000);
      });
    }
    else if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one product.', '', {
        duration: 4000

      });
    }
     else {
        this.snackBar.open('you are not allow to sale products on E-commerce portal.', '', {
          duration: 4000

        });
      }

    
  }

change(){


var data = {
  "product_id": this.checkProduct
}

if(this.checkProduct.length==0){
  this.snackBar.open('please select atleast one product.', '', {
    duration: 4000

  }); 
}
else{

  console.log('checkProduct',this.checkProduct);

  this._DatacallsService.view_no_image(data).subscribe(posts => {
    
    this.posts5 = posts.result;

    for(var i=0;i< this.posts5.length;i++){

if(this.posts5[i].lengths==null || this.posts5[i].lengths== 0){

  this.product_id.push(this.posts5[i].product_id)
  this.array.push(this.posts5[i].product_name)
}
    }
    console.log('posts5',this.posts5);

    console.log('length',this.product_id.length);

    // console.log('array',JSON.stringify(this.array).replace(/\[/g, "").replace(/\]/g, ""));


    if(this.product_id.length==0){

this._DatacallsService.ChangeProductMaster(data).subscribe(posts => {
  console.log('datachangezala',data);
    this.Router.navigate(['products']);
  });

    }
    else{

      var names = JSON.stringify(this.array).replace(/\[/g, "").replace(/\]/g, "")

    // this.snackBar.open(names+",Thease Products images are not available please put images,without images we can not approve","OK");
    
    var  message = names+",Products images are not available please put images,without images we can not approve"; 
      status = 'success';  
 this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'bottom', duration: 5000 });

    console.log('names',names);

   this.array=[];
    }
 
    });


  // this._DatacallsService.ChangeProductMaster(data).subscribe(posts => {
  // console.log('datachangezala',data);
  //   this.Router.navigate(['products']);
  // });
}
}

  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        type: [''],
        brand_id: [''],
        category_id: [''],
        sub_category_id: ['']
      })
    }
  }

  
  onSubmit() {
    this.type = this.searchForm.value.type == '' ? null : this.searchForm.value.type;
    console.log('usertype', this.type);
    console.log('usertype1', this.searchForm.value.type)
    this.brand_id = this.searchForm.value.brand_id == '' ? null : this.searchForm.value.brand_id;
    this.category_id = this.searchForm.value.category_id == '' ? null : this.searchForm.value.category_id;
    this.sub_category_id = this.searchForm.value.sub_category_id == '' ? null : this.searchForm.value.sub_category_id
    this.json = {
      "product_id": null,
      "user_id": null,
      "usertype": this.type,
      "brand_id": this.brand_id,
      "category_id": this.category_id,
      "sub_category_id": this.sub_category_id,
      "show_products_users": null,
      "delete_status": 0,
      "approve":1
    }
    this._DatacallsService.POST_ProductMaster(this.json).subscribe(posts => {

      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts); 
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }
  file: File;
  filesToUpload: Array<File>;


  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }


  UploadExcel(){
    debugger
    console.log(this.filesToUpload)
        var formData: any = new FormData();
      // formData.append("app_id",15); 
   
    this.makeFileRequest("http://13.236.78.106/api/uploadacmeexcel",formData, this.filesToUpload).then((result) => {
         console.log(result);
          if(result['Success']==true){
             this.snackBar.open("Excel Uploaded Successfully", "Done");
             this.ngOnInit();
      // location.reload();
           }
           
           else {
        
           }
       }, (error) => {
         console.error(error);
       });
   
   }

makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


  downloadexcel(){
  
    if(this.usertype.user=='Admin'){
      this._DatacallsService.GETProductexcel(null).subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="productExcelDownload.xlsx";
                    link.click();
                    
      });
    }
    else{
      this._DatacallsService.GETProductexcel(this.usertype.user_id).subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="productExcelDownload.xlsx";
                    link.click();
                    
      });
    }
  

}

}


