import { Component, OnInit,ViewChild,Inject} from '@angular/core';
import { DatacallsService } from '../datacalls.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormBuilder, FormGroup, FormArray, Validators, FormGroupName, FormControl } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import * as _ from "lodash";

import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-approvedproduct',
  templateUrl: './approvedproduct.component.html',
  styleUrls: ['./approvedproduct.component.css']
})
export class ApprovedproductComponent implements OnInit {

  posts;
  kiran;
  postsSearch;
  totalPosts;
  totalPostsSearch;
  checkProduct = [];
  dataSource;
  posts1;
  posts2;
  posts3; 
  posts4;
  searchForm;
  array=[];
  allsel=false;
  intsel=false;
  sel=false;
  type;
  posts9;
  json={};
  brand_id;
  category_id;
  sub_category_id;
  checkdetails = [];
  usertype;
  part_number;
  part_number1;
  not_equal;
  not_equal2=[];
  showPosts = 12;
  finished = 'true';
  public data;
  filteredData;
  productsGroup: FormGroup;
  config = new MatSnackBarConfig();
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _fb: FormBuilder,private _DatacallsService: DatacallsService, private Router: Router,public dialog: MatDialog,private _formBuilder: FormBuilder,public snackBar: MatSnackBar,) { 
 
    this.searchForm = this._fb.group({
      brand_id:['',Validators.required],
      category_id:['',Validators.required],
      sub_category_id:['',Validators.required]
  });
 
 
    this.productsGroup = this._formBuilder.group({

      ref_number:['',Validators.compose([Validators.required, Validators.pattern(/^$|^[A-Za-z0-9]+$/)])]
   
    });
  }

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    this._DatacallsService.GETProductMaster(null, this.usertype.user == 'Admin' ? null : this.usertype.user_id, null, null, null, null, null, 0,0).subscribe(posts => {
      this.posts9 = posts.result;

      this.part_number =this.posts9.map(x => x.part_number);
     

      // this.part_number =this.posts9.map(x => x.part_number.replace(/(^")|("$)/g, ""));

      console.log('part number old',this.part_number);
    });
    
    this._DatacallsService.GETMappedProductMaster(this.usertype.user_id).subscribe(posts => {

      console.log('posts.result :', posts.result)

      this.posts = posts.result;

      this.part_number1 =this.posts.map(x =>x.part_number);

      // this.part_number1 =this.posts.map(x =>x.part_number.replace(/(^")|("$)/g, ""));

      console.log('part number new',this.part_number1);

  this.not_equal = _.differenceWith(this.part_number, this.part_number1, _.isEqual)


  console.log('different number',this.not_equal);

  


    //  this.not_equal2.push(JSON.parse(JSON.stringify(this.not_equal)));

    // this.not_equal2=(a).replace(/\[/g, '{').replace(/]/g, '}');

    // console.log('sssssssssssssssss',this.not_equal2);

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }


    });




//user datacalls
    this._DatacallsService.GETUserMaster(this.usertype.user, this.usertype.user_id,0).subscribe(posts => {
      this.kiran = posts.result;
      console.log('GETUserMaster',this.kiran[0].usertype);
      console.log('data yetoy',this.kiran);
    });

    this._DatacallsService.GETUserMaster(null,null,0).subscribe(posts => {
      this.posts4 = posts.result;
      // console.log('GETUserMaster',this.kiran[0].usertype);
      console.log('data yetoy',this.kiran);
    });


//brand datacalls
    this._DatacallsService.GETBrandMaster(null,0,null).subscribe(posts => {
      this.posts1 = posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
//category datacalls
    this._DatacallsService.GETCategoryMaster(null,0,null).subscribe(posts => {
      this.posts2 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //subcategory datacalls
    this._DatacallsService.GETSubCategoryMaster(null, null,0).subscribe(posts => {
      this.posts3 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

  


  }


  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        brand_id: [''],
        category_id: [''],
        sub_category_id: ['']
      })
    }
  }


  alltogglecheck(b,array){
    
    // if(b==true){
    //   this.checkProduct=[];   
    //   array.forEach(element => {this.checkProduct.push(element)})
     
    //   this.sel=!this.sel
    //   console.log('all_id',this.checkProduct);
    //   console.log('chacked',b);
      
  
    // }  

    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }

    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
    
      this.checkProduct.push(id)
      console.log('data',this.checkProduct)
    }
    else {
      this.checkProduct =deleteintarray(this.checkProduct,id)
      console.log('deletearray :',this.checkProduct);  
    }
    console.log('check thi list ',this.checkProduct)
    console.log('length ',length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel=!this.intsel && this.checkProduct.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
  }
  shareData(data) {
    this._DatacallsService.sendAnything(data) // to send something
    console.log('data',data);
  }

  // onScrollDown() {
  //   this.finished = 'true'

  //   if (this.postsSearch != undefined) {
  //     this.showPosts += this.showPosts
  //     this.postsSearch = this.totalPostsSearch.slice(0, this.showPosts);
  //     console.log("inside if scrolled!!", this.posts);
  //     if (this.postsSearch.length == this.totalPostsSearch.length) {
  //       this.finished = 'false'
  //     }
  //   }
  //   else {
  //     this.showPosts += this.showPosts
  //     this.posts = this.totalPosts.slice(0, this.showPosts);
  //     console.log("inside else scrolled!!", this.posts);
  //     if (this.posts.length == this.totalPosts.length) {
  //       this.finished = 'false'
  //     }
  //   }
  // }
  // openDialog(id,data1): void {
  //   console.log('delete this id',id)
  //   console.log('dara',data1)
 
  //   const dialogRef = this.dialog.open(approvedproduct, {
  //     width: '50%',
  //     data:{"id":id,"data":data1,"user_id":this.usertype.user_id},
      
  //   });
  // }


  onsubmit() {
   

  if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one product.', '', {
        duration: 4000

      });
    }
else{

  
for(var i=0;i < this.checkProduct.length;i++)
{
  console.log('check_product',this.checkProduct);

  var fields = [{
      'inputFields':this.checkProduct[i].fields.input,
      'dropdownFields':this.checkProduct[i].fields.dropdownFields
    }];

    var json=[{
  "brand_id":this.checkProduct[i].brand_details[0].brand_id,
  "brand_reference":this.checkProduct[i].brand_details[0].brand_reference
}];

  var jsonPara2={
    "product_id":0,
    "user_id":this.usertype.user_id,
     "category_id":this.checkProduct[i].category_id,
     "sub_category_id":this.checkProduct[i].sub_category_id,
     "usertype":"Distributor",
     "product_name":this.checkProduct[i].product_name,
     "product_type":this.checkProduct[i].product_type,
     "product_config":JSON.stringify(this.array),
     "reference":JSON.stringify(json),
     "product_type_quantity":null,
     "product_total_quantity":this.checkProduct[i].product_total_quantity,
     "imagepath":JSON.stringify(this.checkProduct[i].images),
     "consumer_price":this.checkProduct[i].consumer_price,
     "retailer_price":this.checkProduct[i].retailer_price,
     "distributer_price":this.checkProduct[i].distributer_price,
     "product_details":JSON.stringify(fields),
     "barcode":"",
     "show_products_users":0,
     "show_products_marketplace":0,
     "active_status":1,
     "part_number":this.checkProduct[i].part_number,
     "description":this.checkProduct[i].description,
     "approve":0, 
     "weight":this.checkProduct[i].weight,
     "height":this.checkProduct[i].height,
     "width":this.checkProduct[i].width,
     "length":this.checkProduct[i].length,
     "duplicate":0

}
console.log('image_path',jsonPara2);
this._DatacallsService.POSTProductMaster(jsonPara2).subscribe(posts => {
 if (posts.result) {
 
   if (posts.result[0].insert_product_master == -2) {
     this.snackBar.open("Part Number Already Exists  ", "Done", this.config);
   }
   else {
      
       this.config.duration = 3000
       this.snackBar.open("Product Added Successfully To My Products", "Done", this.config);
     
     window.setTimeout(() => {
       this.Router.navigate(['products']);
     }, 3000); 
   }
 }
 else {
   this.snackBar.open("Please try again later", "Done");
 }
});

}


}

    

    // old method
 // console.log('yrtoy',this.dialogRef.componentInstance.data)

    // console.log('you have clicked submit');
    // console.log('you have clicked submit: ', this.data);
    // console.log('brand',this.data.data.fields)
    // console.log('user_id',this.data.data.product_image);
// var json=[{
//   "brand_id":this.data.data.brand_details[0].brand_id,
//   "brand_reference":this.data.brand_details[0].brand_reference
// }];

// var fields = [{
//   'inputFields':this.data.data.fields.input,
//   'dropdownFields':this.data.data.fields.dropdownFields
// }];


} 


onSubmit1(){
  // this.type=this.searchForm.value.type==''?null:this.searchForm.value.type;
  // console.log('usertype',this.type);
  // console.log('usertype1',this.searchForm.value.type)
  this.brand_id=this.searchForm.value.brand_id==''?null:this.searchForm.value.brand_id;
  this.category_id=this.searchForm.value.category_id==''?null:this.searchForm.value.category_id;
  this.sub_category_id=this.searchForm.value.sub_category_id==''?null:this.searchForm.value.sub_category_id
  this.json={
    "brand_id":this.brand_id,
    "category_id":this.category_id,
    "sub_category_id":this.sub_category_id
  }
  this._DatacallsService.search_mapped_product(this.json).subscribe(posts => {
    
    this.posts = posts.result;
    this.dataSource = new MatTableDataSource(this.posts);
    this.dataSource.sort = this.sort;
    if (this.posts != undefined) { 
      this.dataSource.paginator = this.paginator;
    }
  });
  
   
  
  
}

}





















// @Component({
//   selector: 'approvedproduct',
//   templateUrl: 'approvedproduct.html',
//   providers : [DatacallsService]
// })
// export class approvedproduct 
// // implements OnInit
// {
//   jsonPara2={};
//   // checkProduct = [];
//   deleteid;
//   brand_reference;
//   usertype;
//   productsGroup: FormGroup;
//   config = new MatSnackBarConfig();
//   constructor(public snackBar: MatSnackBar,private Router: Router,
//     public dialogRef: MatDialogRef<approvedproduct>,
//     @Inject(MAT_DIALOG_DATA) public data,private _DatacallsService:DatacallsService,private _formBuilder: FormBuilder) {
      
//       this.productsGroup = this._formBuilder.group({

//         ref_number:['',Validators.compose([Validators.required, Validators.pattern(/^$|^[A-Za-z0-9]+$/)])]
     
//       });
//       // this.brand_reference='12345'
//     }
//     // ngOnInit() {

//     // }

//   onNoClick(): void {
//     this.dialogRef.close();
//   }
//   onsubmit(data) {
//     // console.log('yrtoy',this.dialogRef.componentInstance.data)

//     console.log('you have clicked submit: ', data);
//     console.log('you have clicked submit: ', this.data);
//     console.log('brand',this.data.data.fields)
//     console.log('user_id',this.data.data.product_image);
// var json=[{
//   "brand_id":this.data.data.brand_details[0].brand_id,
//   "brand_reference":this.data.brand_details[0].brand_reference
// }];

// var fields = [{
//   'inputFields':this.data.data.fields.input,
//   'dropdownFields':this.data.data.fields.dropdownFields
// }];

//     var jsonPara2={
//          "product_id":0,
//          "user_id":this.data.user_id,
//           "category_id":this.data.data.category_id,
//           "sub_category_id":this.data.data.sub_category_id,
//           "usertype":"Distributor",
//           "product_name":this.data.data.product_name,
//           "product_type":this.data.data.product_type,
//           "product_config":null,
//           "reference":JSON.stringify(json),
//           "product_type_quantity":null,
//           "product_total_quantity":this.data.data.product_total_quantity,
//           "imagepath1":this.data.data.product_image,
//           "consumer_price":this.data.data.consumer_price,
//           "retailer_price":this.data.data.retailer_price,
//           "distributer_price":this.data.data.distributer_price,
//           "product_details":JSON.stringify(fields),
//           "barcode":"",
//           "show_products_users":0,
//           "show_products_marketplace":0,
//           "active_status":1,
//           "part_number":this.data.data.part_number

//     }
//     this._DatacallsService.POSTProductMaster(jsonPara2).subscribe(posts => {
//       if (posts.result) {
//         // if (posts.result[0].insert_product_master == -1) {
//         //   this.snackBar.open("Reference Number Already Exists", "Done", this.config);
//         // }
//         if (posts.result[0].insert_product_master == -2) {
//           this.snackBar.open("Part Number Already Exists  ", "Done", this.config);
//         }
//         else {
           
//             this.config.duration = 3000
//             this.snackBar.open("Product Added Successfully To My Products", "Done", this.config);
          
//           // window.setTimeout(() => {
//           //   this.Router.navigate(['products']);
//           // }, 3000);
//         }
//       }
//       else {
//         this.snackBar.open("Please try again later", "Done");
//       }
//     });


// } 
//   // sendIDS(a, id) {
//   //   if (a == true) {
//   //     this.checkProduct.push(id)
//   //     console.log('a',a);
//   //     console.log('id',this.checkProduct);
//   //   }
//   //   else {
//   //     const index = this.checkProduct.indexOf(id);
//   //     if (index !== -1) {
//   //       this.checkProduct.splice(index, 1);
//   //     }
//   //   }
   
//   // }

 
// }
