import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';
@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.css']
  
})

export class BrandsComponent implements OnInit {
  
  posts;
  sel=false;
  brand_id;
  checkProduct = [];
  dataSource;
  showLogo;
  data=[];
  json={};
  imagepath1;
  filteredData;
  allsel=false;
  uploadImg;
  intsel=false;
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.categoryForm = this._formBuilder.group({
      brand_id: [0],
      brand_name: ['', Validators.required]
    });
  } 

  ngOnInit() {
    this._DatacallsService.GETBrandMaster(null,0,null).subscribe(posts => {
      this.posts = posts.result;
     

      console.log(this.posts.hasValue);
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }
  // sendIDS(a, id) {
  //   if (a == true) {
  //     this.checkProduct.push(id)
  //     console.log('a',a);
  //     console.log('id',id);
  //   }
  //   else {
  //     const index = this.checkProduct.indexOf(id);
  //     if (index !== -1) {
  //       this.checkProduct.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // }
  alltogglecheck(b,array){

    // if(b==true){
    //   this.checkProduct=[];   
    //   array.forEach(element => {this.checkProduct.push(element.brand_id)})
     
    //   this.sel=!this.sel
    //   console.log('all_id',this.checkProduct);
    //   console.log('chacked',b);
      
  
    // } 

    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.brand_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.brand_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }

    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
    
      this.checkProduct.push(id)
      console.log('data',this.checkProduct)
    }
    else {
      this.checkProduct =deleteintarray(this.checkProduct,id)
      console.log('deletearray :',this.checkProduct);  
    }
    console.log('check thi list ',this.checkProduct)
    console.log('length ',length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel=!this.intsel && this.checkProduct.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    //this.shareData(this.checkProduct);
  }


  uploadImage(fileInput: any) {
    this.uploadImg = fileInput.target.files['0'];

    this.data.push(fileInput.target.files['0'])
    console.log('data',this.data);
    console.log('files', fileInput.target.files);
    console.log('logo', this.showLogo)
  }



delete(){
var data={
  "delete_status":1,
  "brand_id":this.checkProduct
}


if(this.checkProduct.length==0){
  this.snackBar.open('please select atleast one brand.', '', {
    duration: 4000

  });  
}
else{

  this._DatacallsService.DELETEBrandMaster(data).subscribe(posts => {
console.log('checkproduct',this.checkProduct);
this.Router.navigate(['deleted-brand']);
  });
}
  
}
  resetform(file) {

    // this.categoryForm.reset();
    this.categoryForm = this._formBuilder.group({
      brand_id: [0],
      brand_name: ['', Validators.required]
    });
    file.files = []
    this.brand_id = 0;
  }

  edit(id) {

    this.showLogo=[];

    console.log('logooooooo',this.showLogo);
    this._DatacallsService.GETBrandMaster(id,0,null).subscribe(
      data => {
        this.showLogo=[];
        this.brand_id = data.result[0].brand_id;
        this.showLogo=[data.result[0].brand_image];
        this.imagepath1 = data.result[0].brand_image;

        console.log('logooooooo1',this.showLogo);
        console.log('logooooooo2',this.imagepath1);
        this.categoryForm.patchValue({
          'brand_id': data.result[0].brand_id,
          'brand_name': data.result[0].brand_name
          // 'brand_image':data.result[0].brand_image
          
        });
      }
    
    );
  }

  submit(file) {

   //var files1 = this.data[0]

    var form: any = new FormData();
    var bc = this.categoryForm.value.brand_id
    form.append("brand_id", bc);
    form.append("brand_name", this.categoryForm.value.brand_name);
    form.append("file1",this.uploadImg);
    form.append("imagepath1", this.imagepath1);

    this._DatacallsService.POSTBrandMaster(form).subscribe(posts => {
      if (posts.result) {
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Brand Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Brand Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform(file);
   
  }

}


