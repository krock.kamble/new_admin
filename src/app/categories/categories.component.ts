import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
  providers: [DatacallsService]
})
export class CategoriesComponent implements OnInit {

  posts;
  category_id;
  sel=false;
  allsel=false;
  intsel=false;
  checkProduct=[];
  uploadImg;
  dataSource;
  imagepath1;
  data=[];
  showLogo;
  filteredData;
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.categoryForm = this._formBuilder.group({
      category_id: [0],
      category_name: ['', Validators.required]
    });
  }

  ngOnInit() {
    var json={
      "category_id":null
    }
    this._DatacallsService.view_category_master(json).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  alltogglecheck(b,array){
   
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.category_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.category_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }

  delete(){
    var data={
      "category_id":this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one category.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.delete_category(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.snackBar.open('category deleted.', '', {
      duration: 4000
    }); 
       this.ngOnInit();

   

      });
    
    }
    }

    sendIDS(a, id, length) {
      if (a == true) {
        console.log('id araha hai',id);
       
  
        this.checkProduct.push(id)
        console.log('data',this.checkProduct)
      }
      else {
        this.checkProduct =deleteintarray(this.checkProduct,id)
        console.log('deletearray :',this.checkProduct);
     
      }
      console.log('check thi list ',this.checkProduct)
      console.log('length ',length)
      this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
      this.allsel=!this.intsel && this.checkProduct.length !=0
      console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    }




  resetform() {
    this.categoryForm = this._formBuilder.group({
      category_id: [0],
      category_name: ['', Validators.required]
    });
    this.category_id = 0;
  }

  edit(id) {

    this.showLogo=[];
    console.log('logooooooo',this.showLogo);
    var json ={
      "category_id":id
    }
    this._DatacallsService.view_category_master(json).subscribe(
      data => {

        this.category_id = data.result[0].category_id;
        this.categoryForm.patchValue({
          'category_id': data.result[0].category_id,
          'category_name': data.result[0].category_name
        });
      }
    );
  }

  submit() {

    var bc = this.categoryForm.value.category_id

    var form ={
      "category_id":this.categoryForm.value.category_id,
      "category_name":this.categoryForm.value.category_name
    }


    this._DatacallsService.insert_category_master(form).subscribe(posts => {
      if (posts.result) {
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Category Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Category Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform();
  }

}

