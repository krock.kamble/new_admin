import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

  posts;
  country_id;
  sel=false;
  allsel=false;
  intsel=false;
  checkProduct=[];
  uploadImg;
  dataSource;
  imagepath1;
  data=[];
  showLogo;
  filteredData;
  config = new MatSnackBarConfig();
  countryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.countryForm = this._formBuilder.group({
      country_id: [0],
      country: ['', Validators.required],
      gst: ['', Validators.required]
    });
  }

  ngOnInit() {
    this._DatacallsService.view_country_master(null,0).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  alltogglecheck(b,array){
   
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.country_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.country_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }

  delete(){
    var data={
      "delete_status":1,
      "country_id":this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one country.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.delete_countries(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.Router.navigate(['deleted-country']);
      });
    
    }
    }
    
    sendIDS(a, id, length) {
      if (a == true) {
        console.log('id araha hai',id);
       this.checkProduct.push(id)
        console.log('data',this.checkProduct)
      }
      else {
        this.checkProduct =deleteintarray(this.checkProduct,id)
        console.log('deletearray :',this.checkProduct);
       
      }
      console.log('check thi list ',this.checkProduct)
      console.log('length ',length)
      this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
      this.allsel=!this.intsel && this.checkProduct.length !=0
      console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
      //this.shareData(this.checkProduct);
    }



    

  resetform() {
    this.countryForm = this._formBuilder.group({
      country_id: [0],
      country: ['', Validators.required],
      gst: ['', Validators.required]
    });
    this.country_id = 0;
  }

  edit(id) {

 
    this._DatacallsService.view_country_master(id,0).subscribe(
      data => {


        this.country_id = data.result[0].country_id;
        this.countryForm.patchValue({
          'country_id': data.result[0].country_id,
          'country': data.result[0].country,
          'gst':data.result[0].gst
        });
      }
    );
  }

  submit() {

    
    var bc = this.countryForm.value.country_id

    console.log('country_id',this.country_id);

    var form: any = new FormData();
    form.append("country_id", this.country_id != 0 ? this.country_id : 0);
    form.append("country", this.countryForm.value.country);
    form.append("gst",this.countryForm.value.gst);
    


    this._DatacallsService.POSTCountryMaster(form).subscribe(posts => {
      if (posts.result) {
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Country Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Country Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform();
  }

}

