import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DatacallsService } from '../datacalls.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  vehicles;
  products;
  users;
  mappedproduct;
  posts;
  categorycount;
  constructor(private _DatacallsService: DatacallsService,) { }

  ngOnInit() {
    this._DatacallsService.get_qr_dashboard().subscribe(posts => {

      this.posts = posts.result[0];
console.log('data',this.posts);
    

      this.products = this.posts['productcount'];
      this.categorycount = this.posts['categorycount'];
    });
  }

}
