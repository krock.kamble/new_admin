import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Headers, Response, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
// import { runInThisContext } from 'vm';

const httpOptions = {
  headers: new Headers({ 'x-access-token':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJpYXQiOjE1NTk2MzI5MDZ9.TRiEeckUil_isFqd3V1aUA01jluH_a77UdwDPUsld_E' })
}

@Injectable({
  providedIn: 'root'  
})
export class DatacallsService {

  public products = new BehaviorSubject<any>([]);
  cast = this.products.asObservable();

  public mapdistributor = new BehaviorSubject<any>([]);
  cast1 = this.mapdistributor.asObservable();

  public mapusers = new BehaviorSubject<any>([]);
  cast2 = this.mapdistributor.asObservable();


  sendAnything(data) {
    
    this.products.next(data); 
    console.log('dayaben',this.products);
  }

  
  sendAnything1(data1) {
    this.mapdistributor.next(data1);
    console.log('send',this.cast1);
  }

  sendAnything2(data2) {
    this.mapdistributor.next(data2);
    console.log('send',this.cast1);
  }

  serviceData: string;

  // ip: string = 'http://localhost:5002';
  // ip: string = 'http://13.236.78.106/api';
  // ip2:string = 'http://35.154.89.113:8007';

  public ip1: string = 'http://35.154.89.113:5008';
  ip: string = 'http://13.236.78.106/api';  
  ip2:string = 'http://ugoodapi.nstvm.com/serverapi';
  
  constructor(private http: Http) { } 


  createAccount(json) {
    return this.http.post(this.ip1 + '/createAccount', json)
      .map(res => res.json());
  }

  getAccount_id() {
    return this.http.get(this.ip1 + '/getAccount_id')
      .map(res => res.json());
  }

  POSTLogin(json) {
    return this.http.post('http://localhost:1000/get_login', json)
      .map(res => res.json());
  }

  
  update_users_stripe(json) {
    return this.http.post(this.ip + '/update_users_stripe', json)
      .map(res => res.json());
  }
   
   
  viewdashboard() {
    return this.http.get(this.ip + '/get_acme_dashboard')
      .map(res => res.json());
  }

  viewmanufacturerdashboard(user_id) {
    return this.http.get(this.ip + '/get_manufacturer_acme_dashboard/'+user_id)
      .map(res => res.json());
  }

  view_product_filter_new(product_search) {
    return this.http.get(this.ip + '/view_product_filter_new/'+product_search)
      .map(res => res.json());
  }


  POSTVehicleMaster(json) {
    return this.http.post(this.ip + '/POSTVehicleMaster', json)
      .map(res => res.json());
  }

  GETUserMaster(usertype, user_id,delete_status) {
    return this.http.get(this.ip + '/get_user_master/1/' + usertype + '/' + user_id+'/'+delete_status)
      .map(res => res.json());
  }

  get_user_master(user_id,delete_status) {
    return this.http.get(this.ip2 + '/get_user_master/' + user_id+'/'+delete_status,httpOptions)
      .map(res => res.json());
  }

  get_qr_dashboard() {
    return this.http.get('http://localhost:1000/get_dashboard/')
      .map(res => res.json());
  }

  select_location(json){
    return this.http.post('http://myappcenter.co.in/serverapi/api/select_location',json)
     .map(res=>res.json());
  }
  
  setDistributor(json){
    return this.http.post(this.ip + '/POSTsetdistributor', json)
    .map(res => res.json());
  }

  setUsers(json){
    return this.http.post(this.ip + '/POSTsetusers', json)
    .map(res => res.json());
  }

  

  POSTUserMaster(json) {
    return this.http.post(this.ip + '/POSTUserMaster', json)
      .map(res => res.json());
  }

  POSTinsert_acme_shipping_address(json) {
    return this.http.post(this.ip + '/POSTinsert_acme_shipping_address',json,)
      .map(res => res.json());
  }


  GETCategoryMaster(category_id,delete_status,user_id) {
    return this.http.get(this.ip + '/GETCategoryMaster/' + category_id+'/'+delete_status+'/'+user_id)
      .map(res => res.json());
  }

  view_category_master(json) {
    return this.http.post('http://localhost:1000/view_category_master',json)
      .map(res => res.json());
  }

  view_product_master(json) {
    return this.http.post('http://localhost:1000/view_product_master',json)
      .map(res => res.json());
  }


  delete_category(json) {
    return this.http.post('http://localhost:1000/delete_category',json)
      .map(res => res.json());
  }

  delete_product(json) {
    return this.http.post('http://localhost:1000/delete_product',json)
      .map(res => res.json());
  }
  insert_category_master(json) {
    return this.http.post('http://localhost:1000/insert_category_master',json)
      .map(res => res.json());
  }
  insert_product_master(json) {
    return this.http.post('http://localhost:1000/insert_product_master',json)
      .map(res => res.json());
  }


  view_brand_user(user_id) {
    return this.http.get(this.ip + '/view_brand_user/'+user_id)
      .map(res => res.json());
  }


  view_category_user(user_id) {
    return this.http.get(this.ip + '/view_category_user/'+user_id)
      .map(res => res.json());
  }
  GETCategory_user(category_id) {
    return this.http.get(this.ip + '/GETMappedcatusers/' + category_id)
      .map(res => res.json());
  }

  GETBrand_user(brand_id) {
    return this.http.get(this.ip + '/GETMappedbrandusers/' + brand_id)
      .map(res => res.json());
  }

  view_country_master(country_id,delete_status){
return this.http.get(this.ip +'/view_country_master/'+country_id+'/'+delete_status)
.map(res=>res.json());
  } 

  POSTCountryMaster(json){
return this.http.post(this.ip +'/POSTCountryMaster',json)
.map(res => res.json());
  }

  change_status(json) {
    return this.http.post(this.ip+'/change_product_status',json)
      .map(res => res.json());
  }

  change_review_status(json) {
    return this.http.post(this.ip+'/change_review_status',json)
      .map(res => res.json());
  }

  change_country(json) {
    return this.http.post(this.ip+'/change_product_country',json)
      .map(res => res.json());
  }


  change_all_status(json) {
    return this.http.post(this.ip+'/change_all_product_status',json)
      .map(res => res.json());
  }

  allow_user(json) {
    return this.http.post(this.ip+'/allow_user_status',json)
      .map(res => res.json());
  }
  GETBrandMaster(brand_id,delete_status,user_id) {
    return this.http.get(this.ip + '/GETBrandMaster/' + brand_id+'/'+delete_status+'/'+user_id)
      .map(res => res.json());
  }

  ViewMessage(json) {
    return this.http.post(this.ip + '/ViewMessage',json)
      .map(res => res.json());
  }
  

  GETweightMaster(freight_id,user_id) {
    return this.http.get(this.ip + '/view_weight_master/' + freight_id+'/'+user_id)
      .map(res => res.json());
  }

  GETcubicMaster(freight_id,user_id) {
    return this.http.get(this.ip + '/view_cubic_master/' + freight_id+'/'+user_id)
      .map(res => res.json());
  }


  GETVehicleMasterFilter(vehicle_id, make_id, model_id, variant_id, sub_model_id, to, from,year1,year2,delete_status) {
    return this.http.get(this.ip + '/GETVehicleMaster/' + vehicle_id + '/' + make_id + '/' + model_id + '/' + variant_id + '/' + sub_model_id + '/'  + to + '/' + from+'/'+year1+'/'+year2+'/'+delete_status)
    .map(res => res.json());
    } 

    GETVehicleMaster(vehicle_id, make_id, model_id, variant_id, sub_model_id, to, from,year1,year2,delete_status) {
      return this.http.get(this.ip + '/GETVehicleMaster/' + vehicle_id + '/' + make_id + '/' + model_id + '/' + variant_id + '/' + sub_model_id + '/'  + to + '/' + from+'/'+year1+'/'+year2+'/'+delete_status)
        .map(res => res.json());
    }

    searchGETVehicleMaster(vehicle_id, make_id, model_id, variant_id, sub_model_id, to, from,year1,year2,delete_status) {
      return this.http.get(this.ip + '/GETVehicleMaster/' + vehicle_id + '/' + make_id + '/' + model_id + '/' + variant_id + '/' + sub_model_id + '/'  + to + '/' + from+'/'+year1+'/'+year2+'/'+delete_status)
        .map(res => res.json());
    }

  POSTBrandMaster(json) {
    return this.http.post(this.ip + '/POSTBrandMaster', json)
      .map(res => res.json());
  } 
  DELETEBrandMaster(json) {
    return this.http.post(this.ip + '/delete_brands', json)
      .map(res => res.json());
  }

  delete_message(json) {
    return this.http.post(this.ip + '/delete_message', json)
      .map(res => res.json());
  }
  

  delete_countries(json) {
    return this.http.post(this.ip + '/delete_countries', json)
      .map(res => res.json());
  }

  POSTCountryMapping(json){
    return this.http.post(this.ip +'/POSTCountryMapping',json)
    .map(res =>res.json());
  }

  DELETEweight(json) {
    return this.http.post(this.ip + '/delete_weight', json)
      .map(res => res.json());
  }
  DELETECategoryMaster(json) {
    return this.http.post(this.ip + '/delete_category_master', json)
      .map(res => res.json());
  }

  DELETESubCategoryMaster(json) {
    return this.http.post(this.ip + '/delete_sub_category_master', json)
      .map(res => res.json());
  }
  DELETEMakeMaster(json) {
    return this.http.post(this.ip + '/delete_make_master', json)
      .map(res => res.json());
  }

  DELETEModelMaster(json) {
    return this.http.post(this.ip + '/delete_model_master', json)
      .map(res => res.json());
  }
  DELETESubmodelMaster(json) {
    return this.http.post(this.ip + '/delete_sub_model_master', json)
      .map(res => res.json());
  }
  DELETECatuser(json) {
    return this.http.post(this.ip + '/delete_category_user', json)
      .map(res => res.json());
  }
  DELETEBranduser(json) {
    return this.http.post(this.ip + '/delete_brand_user', json)
      .map(res => res.json());
  }
  DELETEMAppedproduct(json) {
    return this.http.post(this.ip + '/delete_mapped_product', json)
      .map(res => res.json());
  }
  DELETEProductMaster(json) {
    return this.http.post(this.ip + '/delete_product_master', json)
      .map(res => res.json());
  }


  delete_reviews(json) {
    return this.http.post(this.ip + '/delete_reviews', json)
      .map(res => res.json());
  }
  ChangeProductMaster(json) {
    return this.http.post(this.ip + '/excel_product_master', json)
      .map(res => res.json());
  }

  view_no_image(json) {
    return this.http.post(this.ip + '/view_no_image', json)
      .map(res => res.json());
  }


  DELETEUserMaster(json) {
    return this.http.post(this.ip + '/delete_user_master', json)
      .map(res => res.json());
  }
  DELETEVehicleMaster(json) {
    return this.http.post(this.ip + '/delete_vehicle_master', json)
      .map(res => res.json());
  }
  //DELETEMdistributerproduct 
DELETEMdistributerproduct(json) {
  return this.http.post(this.ip + '/delete_product_user', json)
    .map(res => res.json());
}
  
get_products_prices(json) {
  return this.http.post(this.ip + '/get_products_prices', json)
    .map(res => res.json());
}


  GETMappedProducts(product_id,user_id) {
    return this.http.get(this.ip + '/GETMappedProducts/' + product_id+'/'+user_id)
      .map(res => res.json());
  }
  
  GETMappedDistributors(user_id) {
    return this.http.get(this.ip + '/GETMappeddistributors/null/'+user_id)
      .map(res => res.json());
  }

  POSTCategoryMaster(json) {
    return this.http.post(this.ip + '/POSTCategoryMaster', json)
      .map(res => res.json());
  }

  GETMakeMaster(acme_car_make_id,delete_status) {
    return this.http.get(this.ip + '/GETMakeMaster/' + acme_car_make_id+'/'+delete_status)
      .map(res => res.json());
  }

  POSTMakeMaster(json) {
    return this.http.post(this.ip + '/POSTMakeMaster', json)
      .map(res => res.json());
  }

  GETModelMaster(model_id, make_id,delete_status) {
    return this.http.get(this.ip + '/GETModelMaster/' + model_id + '/' + make_id+'/'+delete_status)
      .map(res => res.json());
  }

  GETSubModelMaster(sub_model_id, variant_id, model_id, make_id,delete_status) {
    return this.http.get(this.ip + '/GETSubModelMaster/' + sub_model_id + '/' + variant_id + '/' + model_id + '/' + make_id+'/'+delete_status)
      .map(res => res.json());
  }

  POSTModelMaster(json) {
    return this.http.post(this.ip + '/POSTModelMaster', json)
      .map(res => res.json());
  }

  POSTSubModelMaster(json) {
    return this.http.post(this.ip + '/POSTSubModelMaster', json)
      .map(res => res.json());
  }

  GETSubCategoryMaster(category_id, sub_category_id,delete_status) {
    return this.http.get(this.ip + '/GETSubCategoryMaster/' + category_id + '/' + sub_category_id+'/'+delete_status)
      .map(res => res.json());
  }

  POSTSubCategoryMaster(json) {
    return this.http.post(this.ip + '/POSTSubCategoryMaster', json)
      .map(res => res.json());
  }

  GETProductMaster(product_id, user_id, usertype, brand_id, category_id, sub_category_id, show_products_users,delete_status,approve) {
    return this.http.get(this.ip + '/GETProductMaster/' + product_id + '/' + user_id + '/' + usertype + '/' + brand_id + '/' + category_id + '/' + sub_category_id + '/' + show_products_users+'/'+delete_status+'/'+approve+'/null')
      .map(res => res.json());
  }

  view_product_master_combo(product_id, user_id, usertype, brand_id, category_id, sub_category_id, show_products_users,delete_status,approve,combo_id) {
    return this.http.get(this.ip + '/view_product_master_combo/' + product_id + '/' + user_id + '/' + usertype + '/' + brand_id + '/' + category_id + '/' + sub_category_id + '/' + show_products_users+'/'+delete_status+'/'+approve+'/null/'+combo_id)
      .map(res => res.json());
  }

  GETEcommerceProductMaster(product_id, user_id, usertype, brand_id, category_id, sub_category_id, show_products_users) {
    return this.http.get(this.ip + '/GETEcommerceProductMaster/' + product_id + '/' + user_id + '/' + usertype + '/' + brand_id + '/' + category_id + '/' + sub_category_id + '/' + show_products_users)
      .map(res => res.json());
  }

  GETMappedProductMaster(user_id) {
    return this.http.get(this.ip + '/GETMappedProductMaster/null/'+ user_id + '/null/null/null/null/null')
      .map(res => res.json());
  }
  LOOPProductMaster(product_id,user_id) {
    return this.http.get(this.ip + '/GETMappedProductMaster/'+product_id+'/'+ user_id + '/null/null/null/null/null')
      .map(res => res.json());
  }


  GETProductexcel(user_id): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/productExcelDownload/'+ user_id, options)
      .map(res => res.json());    
    
   }

   GETweightexcel(user_id): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/weightExcelDownload/'+ user_id, options)
      .map(res => res.json());    
    
   }

   ExcelDownload(): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/ExcelDownload/', options)
      .map(res => res.json());    
    
   }

   makeExcelDownload(): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/makeExcelDownload/', options)
      .map(res => res.json());    
    
   }

   modelExcelDownload(): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/modelExcelDownload/', options)
      .map(res => res.json());    
    
   }

   GETcubicexcel(user_id): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/cubicExcelDownload/'+ user_id, options)
      .map(res => res.json());    
    
   }


   GETVehicleexcel(): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/vehicleExcelDownload/', options)
      .map(res => res.json());    
    
   }


   GETtemplateexcel(user_id,category_id,sub_category_id): Observable<File>{
         
    let headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('responseType', 'arrayBuffer');
  let options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });
  
    return this.http.get(this.ip + '/templateExcelDownload/'+ user_id+'/'+category_id+'/'+sub_category_id, options)
      .map(res => res.json());    
    
   }

  GETProductSearch(json) {
    return this.http.post(this.ip + '/GETProductSearch', json)
      .map(res => res.json());
  }

  GETVehicleSearch(json) {
    return this.http.post(this.ip + '/GETVehicleSearch', json)
      .map(res => res.json());
  }

  POSTProductMaster(json) {
    return this.http.post(this.ip + '/POSTProductMaster_new', json)
      .map(res => res.json());
  }

  POSTProductMaster1(json) {
    return this.http.post(this.ip + '/POSTProductMaster', json)
      .map(res => res.json());
  }

  POST_ProductMaster(json) {
    return this.http.post(this.ip + '/POST_ProductMaster', json)
      .map(res => res.json());
  } 

  search_mapped_product(json) {
    return this.http.post(this.ip + '/search_mapped_product', json)
      .map(res => res.json());
  }

  POSTVehicleProductMapping(json) {
    return this.http.post(this.ip + '/POSTVehicleProductMapping', json)
      .map(res => res.json());
  }

  GETOrderMaster(json) {
    return this.http.post(this.ip + '/vieworderdetails',json,)
      .map(res => res.json());
  }

  view_reviews(json) {
    return this.http.post(this.ip + '/view_reviews',json,)
      .map(res => res.json());
  }

  updateorderstatus(json) {
    return this.http.post(this.ip + '/updateorderstatus', json)
      .map(res => res.json());
  }

  public  GETview_acme_shipping_address(shippingaddress_id, user_id) {
       
    return this.http.get(this.ip + '/GETview_acme_shipping_address/' + shippingaddress_id + '/' + user_id)

      .map(res => res.json());

  }

  view_users_contact(json) {
    return this.http.post(this.ip2 + '/view_users_contact', json,httpOptions)
      .map(res => res.json());
  }

  view_users_details(json) {
    return this.http.post(this.ip2 + '/view_users_details', json,httpOptions)
      .map(res => res.json());
  }

  delete_order(json) {
    return this.http.post(this.ip + '/delete_order', json)
      .map(res => res.json());
  }

  insert_make_request(json){
    return this.http.post(this.ip + '/insert_make_request', json)
    .map(res => res.json());
  }


  view_make_request(json){
    return this.http.post(this.ip + '/view_make_request', json)
    .map(res => res.json());
  }

  change_make_request(json){
    return this.http.post(this.ip + '/change_make_request', json)
    .map(res => res.json());
  }

  public getCountries(){
    return [ 
        {name: 'Afghanistan', code: 'AF'}, 
        {name: 'Aland Islands', code: 'AX'}, 
        {name: 'Albania', code: 'AL'}, 
        {name: 'Algeria', code: 'DZ'}, 
        {name: 'American Samoa', code: 'AS'}, 
        {name: 'AndorrA', code: 'AD'}, 
        {name: 'Angola', code: 'AO'}, 
        {name: 'Anguilla', code: 'AI'}, 
        {name: 'Antarctica', code: 'AQ'}, 
        {name: 'Antigua and Barbuda', code: 'AG'}, 
        {name: 'Argentina', code: 'AR'}, 
        {name: 'Armenia', code: 'AM'}, 
        {name: 'Aruba', code: 'AW'}, 
        {name: 'Australia', code: 'AU'}, 
        {name: 'Austria', code: 'AT'}, 
        {name: 'Azerbaijan', code: 'AZ'}, 
        {name: 'Bahamas', code: 'BS'}, 
        {name: 'Bahrain', code: 'BH'}, 
        {name: 'Bangladesh', code: 'BD'}, 
        {name: 'Barbados', code: 'BB'}, 
        {name: 'Belarus', code: 'BY'}, 
        {name: 'Belgium', code: 'BE'}, 
        {name: 'Belize', code: 'BZ'}, 
        {name: 'Benin', code: 'BJ'}, 
        {name: 'Bermuda', code: 'BM'}, 
        {name: 'Bhutan', code: 'BT'}, 
        {name: 'Bolivia', code: 'BO'}, 
        {name: 'Bosnia and Herzegovina', code: 'BA'}, 
        {name: 'Botswana', code: 'BW'}, 
        {name: 'Bouvet Island', code: 'BV'}, 
        {name: 'Brazil', code: 'BR'}, 
        {name: 'British Indian Ocean Territory', code: 'IO'}, 
        {name: 'Brunei Darussalam', code: 'BN'}, 
        {name: 'Bulgaria', code: 'BG'}, 
        {name: 'Burkina Faso', code: 'BF'}, 
        {name: 'Burundi', code: 'BI'}, 
        {name: 'Cambodia', code: 'KH'}, 
        {name: 'Cameroon', code: 'CM'}, 
        {name: 'Canada', code: 'CA'}, 
        {name: 'Cape Verde', code: 'CV'}, 
        {name: 'Cayman Islands', code: 'KY'}, 
        {name: 'Central African Republic', code: 'CF'}, 
        {name: 'Chad', code: 'TD'}, 
        {name: 'Chile', code: 'CL'}, 
        {name: 'China', code: 'CN'}, 
        {name: 'Christmas Island', code: 'CX'}, 
        {name: 'Cocos (Keeling) Islands', code: 'CC'}, 
        {name: 'Colombia', code: 'CO'}, 
        {name: 'Comoros', code: 'KM'}, 
        {name: 'Congo', code: 'CG'}, 
        {name: 'Congo, The Democratic Republic of the', code: 'CD'}, 
        {name: 'Cook Islands', code: 'CK'}, 
        {name: 'Costa Rica', code: 'CR'}, 
        {name: 'Cote D\'Ivoire', code: 'CI'}, 
        {name: 'Croatia', code: 'HR'}, 
        {name: 'Cuba', code: 'CU'}, 
        {name: 'Cyprus', code: 'CY'}, 
        {name: 'Czech Republic', code: 'CZ'}, 
        {name: 'Denmark', code: 'DK'}, 
        {name: 'Djibouti', code: 'DJ'}, 
        {name: 'Dominica', code: 'DM'}, 
        {name: 'Dominican Republic', code: 'DO'}, 
        {name: 'Ecuador', code: 'EC'}, 
        {name: 'Egypt', code: 'EG'}, 
        {name: 'El Salvador', code: 'SV'}, 
        {name: 'Equatorial Guinea', code: 'GQ'}, 
        {name: 'Eritrea', code: 'ER'}, 
        {name: 'Estonia', code: 'EE'}, 
        {name: 'Ethiopia', code: 'ET'}, 
        {name: 'Falkland Islands (Malvinas)', code: 'FK'}, 
        {name: 'Faroe Islands', code: 'FO'}, 
        {name: 'Fiji', code: 'FJ'}, 
        {name: 'Finland', code: 'FI'}, 
        {name: 'France', code: 'FR'}, 
        {name: 'French Guiana', code: 'GF'}, 
        {name: 'French Polynesia', code: 'PF'}, 
        {name: 'French Southern Territories', code: 'TF'}, 
        {name: 'Gabon', code: 'GA'}, 
        {name: 'Gambia', code: 'GM'}, 
        {name: 'Georgia', code: 'GE'}, 
        {name: 'Germany', code: 'DE'}, 
        {name: 'Ghana', code: 'GH'}, 
        {name: 'Gibraltar', code: 'GI'}, 
        {name: 'Greece', code: 'GR'}, 
        {name: 'Greenland', code: 'GL'}, 
        {name: 'Grenada', code: 'GD'}, 
        {name: 'Guadeloupe', code: 'GP'}, 
        {name: 'Guam', code: 'GU'}, 
        {name: 'Guatemala', code: 'GT'}, 
        {name: 'Guernsey', code: 'GG'}, 
        {name: 'Guinea', code: 'GN'}, 
        {name: 'Guinea-Bissau', code: 'GW'}, 
        {name: 'Guyana', code: 'GY'}, 
        {name: 'Haiti', code: 'HT'}, 
        {name: 'Heard Island and Mcdonald Islands', code: 'HM'}, 
        {name: 'Holy See (Vatican City State)', code: 'VA'}, 
        {name: 'Honduras', code: 'HN'}, 
        {name: 'Hong Kong', code: 'HK'}, 
        {name: 'Hungary', code: 'HU'}, 
        {name: 'Iceland', code: 'IS'}, 
        {name: 'India', code: 'IND'}, 
        {name: 'Indonesia', code: 'ID'}, 
        {name: 'Iran, Islamic Republic Of', code: 'IR'}, 
        {name: 'Iraq', code: 'IQ'}, 
        {name: 'Ireland', code: 'IE'}, 
        {name: 'Isle of Man', code: 'IM'}, 
        {name: 'Israel', code: 'IL'}, 
        {name: 'Italy', code: 'IT'}, 
        {name: 'Jamaica', code: 'JM'}, 
        {name: 'Japan', code: 'JP'}, 
        {name: 'Jersey', code: 'JE'}, 
        {name: 'Jordan', code: 'JO'}, 
        {name: 'Kazakhstan', code: 'KZ'}, 
        {name: 'Kenya', code: 'KE'}, 
        {name: 'Kiribati', code: 'KI'}, 
        {name: 'Korea, Democratic People\'S Republic of', code: 'KP'}, 
        {name: 'Korea, Republic of', code: 'KR'}, 
        {name: 'Kuwait', code: 'KW'}, 
        {name: 'Kyrgyzstan', code: 'KG'}, 
        {name: 'Lao People\'S Democratic Republic', code: 'LA'}, 
        {name: 'Latvia', code: 'LV'}, 
        {name: 'Lebanon', code: 'LB'}, 
        {name: 'Lesotho', code: 'LS'}, 
        {name: 'Liberia', code: 'LR'}, 
        {name: 'Libyan Arab Jamahiriya', code: 'LY'}, 
        {name: 'Liechtenstein', code: 'LI'}, 
        {name: 'Lithuania', code: 'LT'}, 
        {name: 'Luxembourg', code: 'LU'}, 
        {name: 'Macao', code: 'MO'}, 
        {name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'}, 
        {name: 'Madagascar', code: 'MG'}, 
        {name: 'Malawi', code: 'MW'}, 
        {name: 'Malaysia', code: 'MY'}, 
        {name: 'Maldives', code: 'MV'}, 
        {name: 'Mali', code: 'ML'}, 
        {name: 'Malta', code: 'MT'}, 
        {name: 'Marshall Islands', code: 'MH'}, 
        {name: 'Martinique', code: 'MQ'}, 
        {name: 'Mauritania', code: 'MR'}, 
        {name: 'Mauritius', code: 'MU'}, 
        {name: 'Mayotte', code: 'YT'}, 
        {name: 'Mexico', code: 'MX'}, 
        {name: 'Micronesia, Federated States of', code: 'FM'}, 
        {name: 'Moldova, Republic of', code: 'MD'}, 
        {name: 'Monaco', code: 'MC'}, 
        {name: 'Mongolia', code: 'MN'}, 
        {name: 'Montserrat', code: 'MS'}, 
        {name: 'Morocco', code: 'MA'}, 
        {name: 'Mozambique', code: 'MZ'}, 
        {name: 'Myanmar', code: 'MM'}, 
        {name: 'Namibia', code: 'NA'}, 
        {name: 'Nauru', code: 'NR'}, 
        {name: 'Nepal', code: 'NP'}, 
        {name: 'Netherlands', code: 'NL'}, 
        {name: 'Netherlands Antilles', code: 'AN'}, 
        {name: 'New Caledonia', code: 'NC'}, 
        {name: 'New Zealand', code: 'NZ'}, 
        {name: 'Nicaragua', code: 'NI'}, 
        {name: 'Niger', code: 'NE'}, 
        {name: 'Nigeria', code: 'NG'}, 
        {name: 'Niue', code: 'NU'}, 
        {name: 'Norfolk Island', code: 'NF'}, 
        {name: 'Northern Mariana Islands', code: 'MP'}, 
        {name: 'Norway', code: 'NO'}, 
        {name: 'Oman', code: 'OM'}, 
        {name: 'Pakistan', code: 'PK'}, 
        {name: 'Palau', code: 'PW'}, 
        {name: 'Palestinian Territory, Occupied', code: 'PS'}, 
        {name: 'Panama', code: 'PA'}, 
        {name: 'Papua New Guinea', code: 'PG'}, 
        {name: 'Paraguay', code: 'PY'}, 
        {name: 'Peru', code: 'PE'}, 
        {name: 'Philippines', code: 'PH'}, 
        {name: 'Pitcairn', code: 'PN'}, 
        {name: 'Poland', code: 'PL'}, 
        {name: 'Portugal', code: 'PT'}, 
        {name: 'Puerto Rico', code: 'PR'}, 
        {name: 'Qatar', code: 'QA'}, 
        {name: 'Reunion', code: 'RE'}, 
        {name: 'Romania', code: 'RO'}, 
        {name: 'Russian Federation', code: 'RU'}, 
        {name: 'RWANDA', code: 'RW'}, 
        {name: 'Saint Helena', code: 'SH'}, 
        {name: 'Saint Kitts and Nevis', code: 'KN'}, 
        {name: 'Saint Lucia', code: 'LC'}, 
        {name: 'Saint Pierre and Miquelon', code: 'PM'}, 
        {name: 'Saint Vincent and the Grenadines', code: 'VC'}, 
        {name: 'Samoa', code: 'WS'}, 
        {name: 'San Marino', code: 'SM'}, 
        {name: 'Sao Tome and Principe', code: 'ST'}, 
        {name: 'Saudi Arabia', code: 'SA'}, 
        {name: 'Senegal', code: 'SN'}, 
        {name: 'Serbia and Montenegro', code: 'CS'}, 
        {name: 'Seychelles', code: 'SC'}, 
        {name: 'Sierra Leone', code: 'SL'}, 
        {name: 'Singapore', code: 'SG'}, 
        {name: 'Slovakia', code: 'SK'}, 
        {name: 'Slovenia', code: 'SI'}, 
        {name: 'Solomon Islands', code: 'SB'}, 
        {name: 'Somalia', code: 'SO'}, 
        {name: 'South Africa', code: 'ZA'}, 
        {name: 'South Georgia and the South Sandwich Islands', code: 'GS'}, 
        {name: 'Spain', code: 'ES'}, 
        {name: 'Sri Lanka', code: 'LK'}, 
        {name: 'Sudan', code: 'SD'}, 
        {name: 'Suriname', code: 'SR'}, 
        {name: 'Svalbard and Jan Mayen', code: 'SJ'}, 
        {name: 'Swaziland', code: 'SZ'}, 
        {name: 'Sweden', code: 'SE'}, 
        {name: 'Switzerland', code: 'CH'}, 
        {name: 'Syrian Arab Republic', code: 'SY'}, 
        {name: 'Taiwan, Province of China', code: 'TW'}, 
        {name: 'Tajikistan', code: 'TJ'}, 
        {name: 'Tanzania, United Republic of', code: 'TZ'}, 
        {name: 'Thailand', code: 'TH'}, 
        {name: 'Timor-Leste', code: 'TL'}, 
        {name: 'Togo', code: 'TG'}, 
        {name: 'Tokelau', code: 'TK'}, 
        {name: 'Tonga', code: 'TO'}, 
        {name: 'Trinidad and Tobago', code: 'TT'}, 
        {name: 'Tunisia', code: 'TN'}, 
        {name: 'Turkey', code: 'TR'}, 
        {name: 'Turkmenistan', code: 'TM'}, 
        {name: 'Turks and Caicos Islands', code: 'TC'}, 
        {name: 'Tuvalu', code: 'TV'}, 
        {name: 'Uganda', code: 'UG'}, 
        {name: 'Ukraine', code: 'UA'}, 
        {name: 'United Arab Emirates', code: 'AE'}, 
        {name: 'United Kingdom', code: 'GB'}, 
        {name: 'United States', code: 'US'}, 
        {name: 'United States Minor Outlying Islands', code: 'UM'}, 
        {name: 'Uruguay', code: 'UY'}, 
        {name: 'Uzbekistan', code: 'UZ'}, 
        {name: 'Vanuatu', code: 'VU'}, 
        {name: 'Venezuela', code: 'VE'}, 
        {name: 'Viet Nam', code: 'VN'}, 
        {name: 'Virgin Islands, British', code: 'VG'}, 
        {name: 'Virgin Islands, U.S.', code: 'VI'}, 
        {name: 'Wallis and Futuna', code: 'WF'}, 
        {name: 'Western Sahara', code: 'EH'}, 
        {name: 'Yemen', code: 'YE'}, 
        {name: 'Zambia', code: 'ZM'}, 
        {name: 'Zimbabwe', code: 'ZW'} 
    ]
}

}
  