import { Component, OnInit,ViewChild } from '@angular/core';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';

@Component({
  selector: 'app-deleted-car',
  templateUrl: './deleted-car.component.html',
  styleUrls: ['./deleted-car.component.css'],
  providers: [DatacallsService]
})
export class DeletedCarComponent implements OnInit {

  posts;
  totalPosts;
  showPosts = 12;
  make_id;
  model_id;
  sub_model_id;
  searchForm;
  posts1;
  posts2;
  posts3;
  dataSource;
  finished = 'true';
  checkUsers=[];


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _DatacallsService: DatacallsService, private Router: Router,private _fb: FormBuilder, public snackBar: MatSnackBar) {
    this.searchForm = this._fb.group({
   
      make_id: [''],
      model_id: [''],
      sub_model_id: ['']
    });
   }

  ngOnInit() {
    this._DatacallsService.GETVehicleMaster(null,null,null,null,null,null,null,null,null,1).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    
    this._DatacallsService.GETMakeMaster(null,0).subscribe(posts => {
      this.posts1 = posts.result;
      console.log('data1',this.posts1)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });


    this._DatacallsService.GETModelMaster(null, null,0).subscribe(posts => {
      this.posts2 = posts.result;
      console.log('data2',this.posts2)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    this._DatacallsService.GETSubModelMaster(null, null, null, null,0).subscribe(posts => {
      this.posts3 = posts.result;
      console.log('data3',this.posts3)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });


  }
  sendIDS(a, id) {
    if (a == true) {
      console.log('a1',a);
      console.log('id1',id);
      this.checkUsers.push(id)
    }
    else {
      const index = this.checkUsers.indexOf(id);
      if (index !== -1) {
        this.checkUsers.splice(index, 1);
      }
    }
    //this.shareData(this.checkProduct);
  }
  delete(){
    var data={
      "delete_status":0,
      "vehicle_id":this.checkUsers
    }
    if(this.checkUsers.length==0){
      this.snackBar.open('please select atleast one vehicle.', '', {
        duration: 4000
    
      }); 
    }
    else{
      this._DatacallsService.DELETEVehicleMaster(data).subscribe(posts => {
    console.log('checkUsers',this.checkUsers);
    this.Router.navigate(['vehicles']);
      });
    }
      
    }
  // onScrollDown() {
  //   this.finished = 'true'
  //   //this.totalPostsData = this.totalPosts.length;
  //   //this.showPostCard = 'true';
  //   this.showPosts += this.showPosts
  //   this.posts = this.totalPosts.slice(0, this.showPosts);
  //   console.log("scrolled!!", this.posts);
  //   if (this.posts.length == this.totalPosts.length) {
  //     this.finished = 'false'
  //   }
  //   console.log('not', this.showPosts + 'total', this.totalPosts)

  // }
  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        make_id: [''],
        model_id: [''],
        sub_model_id: ['']
      })
    }
  }


  onSubmit() {

    this.make_id = this.searchForm.value.make_id == '' ? null : this.searchForm.value.make_id;
    this.model_id = this.searchForm.value.model_id == '' ? null : this.searchForm.value.model_id;
    this.sub_model_id = this.searchForm.value.sub_model_id == '' ? null : this.searchForm.value.sub_model_id

    console.log('make_id',this.make_id);
    console.log('model_id',this.model_id);
    console.log('sub_model_id',this.sub_model_id);
   
    this._DatacallsService.GETVehicleMaster(null,this.make_id,this.model_id,null,this.sub_model_id,null,null,null,null,1).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }


}

