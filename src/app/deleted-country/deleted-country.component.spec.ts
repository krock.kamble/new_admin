import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletedCountryComponent } from './deleted-country.component';

describe('DeletedCountryComponent', () => {
  let component: DeletedCountryComponent;
  let fixture: ComponentFixture<DeletedCountryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletedCountryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletedCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
