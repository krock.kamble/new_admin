import { DatacallsService } from '../datacalls.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-deleted-product',
  templateUrl: './deleted-product.component.html',
  styleUrls: ['./deleted-product.component.css']
})
export class DeletedProductComponent implements OnInit {

  posts;
  postsSearch;
  type;
  totalPosts;
  brand_id;
  posts1;
  posts2;
  posts3;
  category_id;
  sub_category_id;
  json = {};
  searchForm;
  dataSource;
  sel = false;
  allsel = false;
  intsel = false;
  filteredData;
  totalPostsSearch;
  checkProduct = [];
  usertype;
  showPosts = 12;
  finished = 'true';

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
    var form = {
      "product_search": filterValue
    }

    this._DatacallsService.GETProductSearch(form).subscribe(posts => {
      this.posts = posts.result;
      this.totalPostsSearch = this.posts;
      this.postsSearch = posts.result.slice(0, this.showPosts);
      // if(posts.result.length == 0){
      //   this.finished = 'false';
      // }
      console.log('this', this.posts)
    });

  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _DatacallsService: DatacallsService,private Router: Router,private _fb: FormBuilder, public snackBar: MatSnackBar) {
    this.searchForm = this._fb.group({
      type: [''],
      brand_id: [''],
      category_id: [''],
      sub_category_id: ['']
    });

   }

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    this._DatacallsService.GETProductMaster(null, this.usertype.user == 'Admin' ? null : this.usertype.user_id, null, null, null, null, null,1,0).subscribe(posts => 
        {

      console.log('posts.result :', posts.result)

      // this.totalPosts = posts.result;
      // if (this.totalPosts != undefined) {
      //   this.posts = posts.result.slice(0, this.showPosts);
      //   if (this.totalPosts.length == 0) {
      //     this.finished = 'undefined';
      //   }
      // }
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'))
    //user datacalls
    
    // this._DatacallsService.GETUserMaster(null, null, 0).subscribe(posts => {
    //   this.posts4 = posts.result;
    //   console.log('GETUserMaster', this.kiran[0].usertype);
    //   console.log('data yetoy', this.kiran);
    // });


    //brand datacalls
    this._DatacallsService.GETBrandMaster(null, 0,null).subscribe(posts => {
      this.posts1 = posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //category datacalls
    this._DatacallsService.GETCategoryMaster(null, 0,null).subscribe(posts => {
      this.posts2 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //subcategory datacalls
    this._DatacallsService.GETSubCategoryMaster(null, null, 0).subscribe(posts => {
      this.posts3 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }


  onSubmit() {
    this.type = this.searchForm.value.type == '' ? null : this.searchForm.value.type;
    console.log('usertype', this.type);
    console.log('usertype1', this.searchForm.value.type)
    this.brand_id = this.searchForm.value.brand_id == '' ? null : this.searchForm.value.brand_id;
    this.category_id = this.searchForm.value.category_id == '' ? null : this.searchForm.value.category_id;
    this.sub_category_id = this.searchForm.value.sub_category_id == '' ? null : this.searchForm.value.sub_category_id
    
    
    if(this.usertype.user == 'Admin'){
      this.json = {
        "product_id": null,
        "user_id": null,
        "usertype": this.type,
        "brand_id": this.brand_id,
        "category_id": this.category_id,
        "sub_category_id": this.sub_category_id,
        "show_products_users": null,
        "delete_status": 1,
        "approve":0
      }
     }
     else{
      this.json = {
        "product_id": null,
        "user_id": this.usertype.user_id,
        "usertype": this.type,
        "brand_id": this.brand_id,
        "category_id": this.category_id,
        "sub_category_id": this.sub_category_id,
        "show_products_users": null,
        "delete_status": 1,
        "approve":0
      }
     }
    // this.json = {
    //   "product_id": null,
    //   "user_id": this.usertype.user_id,
    //   "usertype": this.type,
    //   "brand_id": this.brand_id,
    //   "category_id": this.category_id,
    //   "sub_category_id": this.sub_category_id,
    //   "show_products_users": null,
    //   "delete_status": 1,
    //   "approve":0
    // }
    this._DatacallsService.POST_ProductMaster(this.json).subscribe(posts => {

      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }


  alltogglecheck(b, array) {
    // if (b == true) {
    //   this.checkProduct = [];
    //   array.forEach(element => { this.checkProduct.push(element.product_id) })

    //   this.sel = !this.sel
    //   console.log('all_id', this.checkProduct);
    //   console.log('chacked', b);


    // }
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.product_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.product_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }

    else {
      this.sel = !this.sel
      this.checkProduct = []
      console.log('unchaked', b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai', id);


      this.checkProduct.push(id)
      console.log('data', this.checkProduct)
    }
    else {
      this.checkProduct = deleteintarray(this.checkProduct, id)
      console.log('deletearray :', this.checkProduct);

    }
    console.log('check thi list ', this.checkProduct)
    console.log('length ', length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel = !this.intsel && this.checkProduct.length != 0
    console.log('intsel : ' + this.intsel + ' && allsel:', this.allsel)
  }
  // sendIDS(a, id) {
  //   if (a == true) {
  //     console.log('a',a);
  //     console.log('id',id);
  //     this.checkProduct.push(id)
  //   }
  //   else {
  //     const index = this.checkProduct.indexOf(id);
  //     if (index !== -1) {
  //       this.checkProduct.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // } 

  shareData(data) {
    this._DatacallsService.sendAnything(data) // to send something
  }

  delete(){
    var data={
      "delete_status":0,
      "product_id":this.checkProduct
    }
    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one product.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.DELETEProductMaster(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.Router.navigate(['products']);
      });
    }
      
    }
 

}

			  
			