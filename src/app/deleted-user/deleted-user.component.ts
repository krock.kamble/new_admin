import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-deleted-user',
  templateUrl: './deleted-user.component.html',
  styleUrls: ['./deleted-user.component.css']
})
export class DeletedUserComponent implements OnInit {

  posts;
  id;
  brands;
  dataSource;
  categories;
  subcategories;
  checkUsers = [];
  json={};
  config = new MatSnackBarConfig();
  userForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);
 

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.userForm = this._formBuilder.group({
      id: [0],
      password: ['', Validators.required],
      name: ['', Validators.required],
      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern(/^$|^[0-9]+$/)])],
      address: ['', Validators.required],
      usertype: ['', Validators.required],
      email: ['', [Validators.required, Validators.email,]],
      unique_code: ['', Validators.required],
      brand: ['', Validators.required],
      category: ['', Validators.required],
      sub_category: ['', Validators.required],
      country: ['', Validators.required]
    });
  }

  ngOnInit() {
    this._DatacallsService.GETUserMaster(null, null,1).subscribe(posts => {
      this.posts = posts.result;
      for (var i = 0; i < this.posts.length; i++) {
        if (this.posts[i].usertype == 'Admin') {
          this.posts.splice(i, 1);
        }
      }
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    this._DatacallsService.GETBrandMaster(null,0,null).subscribe(posts => {
      this.brands = posts.result;
    });

    this._DatacallsService.GETCategoryMaster(null,0,null).subscribe(posts => {
      this.categories = posts.result;
    });
  }

  sendIDS(a, id) {
    if (a == true) {
      console.log('a1',a);
      console.log('id1',id);
      this.checkUsers.push(id)
    }
    else {
      const index = this.checkUsers.indexOf(id);
      if (index !== -1) {
        this.checkUsers.splice(index, 1);
      }
    }
    //this.shareData(this.checkProduct);
  }
  shareData(data2) {

    var data = {"data": this.checkUsers};

          localStorage.setItem('data', JSON.stringify(data));

    this._DatacallsService.sendAnything2(data2) // to send something
    console.log('data yetoy',data2);

   
  }

  delete(){
    var data={
      "delete_status":0,
      "id":this.checkUsers
    }
    if(this.checkUsers.length==0){
      this.snackBar.open('please select atleast one user.', '', {
        duration: 4000
    
      }); 
    }
    else{
      this._DatacallsService.DELETEUserMaster(data).subscribe(posts => {
    console.log('checkUsers',this.checkUsers);
    this.Router.navigate(['users']);
      });
    
    }
    }
  resetform() {
    this.userForm = this._formBuilder.group({
      id: [0],
      password: ['', Validators.required],
      name: ['', Validators.required],
      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern(/^$|^[0-9]+$/)])],
      address: ['', Validators.required],
      usertype: ['', Validators.required],
      email: ['', [Validators.required, Validators.email,]],
      unique_code: ['', Validators.required],
      brand: ['', Validators.required],
      category: ['', Validators.required],
      sub_category: ['', Validators.required],
      country: ['', Validators.required]
    });
    this.id = 0;
  }

  edit(id, usertype) {
    this._DatacallsService.GETUserMaster(usertype, id,1).subscribe(
      data => {
        this.id = data.result[0].id;
        this.userForm.patchValue({
          'id': data.result[0].id,
          'password': data.result[0].password,
          'name': data.result[0].name,
          'mobile_no': data.result[0].mobile_no,
          'address': data.result[0].address,
          'usertype': data.result[0].usertype,
          'email': data.result[0].email,
          'unique_code': data.result[0].unique_code,
          'brand': data.result[0].brand_id,
          'category': data.result[0].category_id,
          'sub_category': data.result[0].sub_category_id,
          'country': data.result[0].country
        });
      }
    );
  }

  getSubCategory(category_id) {
    this._DatacallsService.GETSubCategoryMaster(category_id, null,0).subscribe(posts => {
      this.subcategories = posts.result;
    });
  }

  submit() {
    console.log('tetet',this.userForm.value.brand)
    var bc = this.userForm.value.id;
    var form: any = new FormData();
    form.append("id", this.userForm.value.id)
    form.append("category_id", this.userForm.value.category)
    form.append("sub_category_id", this.userForm.value.sub_category)
    form.append("brand_id", this.userForm.value.brand)
    form.append("country", this.userForm.value.country)
    form.append("password", this.userForm.value.password)
    form.append("name", this.userForm.value.name)
    form.append("mobile_no", this.userForm.value.mobile_no)
    form.append("address", this.userForm.value.address)
    form.append("usertype", this.userForm.value.usertype)
    form.append("email", this.userForm.value.email)
    form.append("unique_code", this.userForm.value.unique_code)
    form.append("active_status", 1)

    this._DatacallsService.POSTUserMaster(form).subscribe(posts => {
      if (posts.result) {
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Brand Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Brand Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform();
  }

}

