import { Component, OnInit, NgModule,ElementRef,ViewChild, Inject } from '@angular/core';
import {DatacallsService} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router'
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
// import {HttpClient} from '@angular/common/http';
// import {merge, Observable, of as observableOf} from 'rxjs';
// import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {SelectionModel} from '@angular/cdk/collections';
import {deleteobj} from '../deletearray'

export interface UserData {
  id: string;
  name: string;
  mobile_no: string;
  password: string;
  email:string;
  city:string;
  state:string;
  country:string
}

@Component({
  selector: 'app-distributerlist',
  templateUrl: './distributerlist.component.html',
  styleUrls: ['./distributerlist.component.css'],
  providers: [DatacallsService]

})


export class DistributerlistComponent implements OnInit {
  sel=false;
  jsonPara3={};
  jsonPara4={};
  
  idarray=[];
  allsel=false;
  intsel=false;
  displayedColumns = ['id', 'name', 'mobile_no','email', 'address','country','unique_code','action'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
   posts;
   checkProduct = [];
   resultsLength = 0;
   checkDistributor = [];
   json={};
   filteredData;
   usertype;
   isLoadingResults = true;
   isRateLimitReached = false;

   applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  constructor(private _DatacallsService:DatacallsService,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute,private breakpointObserver: BreakpointObserver,private fb:FormBuilder,private Router:Router,public dialog: MatDialog) { 

  }

  ngOnInit() {



    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    var form = {
      "receiver_id" :this.usertype.user_id,
      "status":1
  }
  this._DatacallsService.view_make_request(form).subscribe(posts => {
    console.log(posts);
     
    this.posts = posts.result
 
    console.log('result',this.posts)
    this.dataSource = new MatTableDataSource(this.posts);
     
         
   });

    // this._DatacallsService.GETUserMaster('Distributor', null,0).subscribe(posts => {
    //   console.log(posts);
       
    //   this.posts = posts.result
   
    //   console.log('result',this.posts)
    //   this.dataSource = new MatTableDataSource(this.posts);
       
           
    //  });

  }

  

  alltogglecheck(b,array){
    // if(b==true){
    //   this.checkDistributor=[];   
    //   array.forEach(element => {this.checkDistributor.push(element)})
    //   this.sel=!this.sel
    //   console.log('all_id',this.checkDistributor);
    //   console.log('chacked',b);
    // } 

    if (b == true) {
      this.checkDistributor = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkDistributor.push(element.sender_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkDistributor =this.filteredData.map(x=> x.sender_id)
        console.log('this.checkProduct',this.checkDistributor)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkDistributor);
      console.log('chacked', b);
      

    }

    else {
      this.sel=!this.sel
      this.checkDistributor=[]
      console.log('unchaked',b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
      this.json={
        "id":id
      }

      this.checkDistributor.push(this.json)
      console.log('data',this.checkDistributor)
    }
    else {
      this.checkDistributor =deleteobj(this.checkDistributor,"id",id)
      console.log('deletearray :',this.checkDistributor);
    //   console.log(this.checkDistributor[0]["id"])
    // for(var i=0;i<this.checkDistributor.length;i++){
    //   if(this.checkDistributor[i].id==id){
    //     this.checkDistributor.splice(i, 1);
    //   }
    // }



   
    }
    console.log('check thi list ',this.checkDistributor)
    console.log('length ',length)
    this.intsel = this.checkDistributor.length != 0 && this.checkDistributor.length != length ? true : false;
    this.allsel=!this.intsel && this.checkDistributor.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    //this.shareData(this.checkProduct);
  }


  shareData(data1) {

    var data = {"data": this.checkDistributor};

    localStorage.setItem('data', JSON.stringify(data));

    this._DatacallsService.sendAnything1(data1) // to send something
    console.log('data yetoy',data1);

   
  }
 

 

}


