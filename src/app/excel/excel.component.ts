import { DatacallsService } from '../datacalls.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { deleteobj, deleteintarray } from '../deletearray'
import { element } from '../../../node_modules/protractor';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { resource } from 'selenium-webdriver/http';


@Component({
  selector: 'app-excel',
  templateUrl: './excel.component.html',
  styleUrls: ['./excel.component.css']
})
export class ExcelComponent implements OnInit {
  posts;
  posts1;
  posts2;
  posts3;
  posts4;
  kiran;
  searchForm;
  json = {};
  categories;
  subcategories
  sel = false;
  allsel = false;
  intsel = false;
  brand_id;
  category_id;
  sub_category_id;
  subcategoriesfields
  postsSearch;
  totalPosts;
  jsonPara3 = {};
  jsonPara4 = {};
  dataSource;
  totalPostsSearch;
  checkProduct = [];
  usertype;
  public productForm: FormGroup;
  type;
  config = new MatSnackBarConfig();
  showPosts = 12;
  finished = 'true';

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _fb: FormBuilder, private _DatacallsService: DatacallsService, private Router: Router, public snackBar: MatSnackBar) {

    this.searchForm = this._fb.group({
      type: [''],
      brand_id: [''],
      category_id: [''],
      sub_category_id: ['']
    });

    this.productForm = this._fb.group({

      Excelfile: ['', Validators.required]

  });

  }

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    if(this.usertype.user=='Admin'){
      this._DatacallsService.GETCategoryMaster(null, 0,null).subscribe(posts => {
        this.categories = posts.result;
      });
    }
    else{

      this._DatacallsService.GETCategoryMaster(null, 0,this.usertype.user_id).subscribe(posts => {
        this.categories = posts.result;
      });
    }
   
    //subcategory datacalls
    // this._DatacallsService.GETSubCategoryMaster(null, null, 0).subscribe(posts => {
    //   this.posts3 = posts.result;
    //   this.dataSource = new MatTableDataSource(this.posts);
    //   this.dataSource.sort = this.sort;
    //   if (this.posts != undefined) {
    //     this.dataSource.paginator = this.paginator;
    //   }
    // });
  }


  getSubCategory(category_id) {
    this._DatacallsService.GETSubCategoryMaster(category_id, null, 0).subscribe(posts => {
      this.subcategories = posts.result;
    });
  } 


  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        type: [''],
        brand_id: [''],
        category_id: [''],
        sub_category_id: ['']
      })
    }
  }


  downloadexcel(){
  
    if(this.usertype.user=='Admin'){
      this._DatacallsService.GETtemplateexcel(null,this.searchForm.value.category_id,this.searchForm.value.sub_category_id).subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="productExcelDownload.xlsx";
                    link.click();
                    
      });
    }
    else{

      this._DatacallsService.GETtemplateexcel(this.usertype.user_id,this.searchForm.value.category_id,this.searchForm.value.sub_category_id).subscribe(blob => {

      // this._DatacallsService.GETProductexcel(this.usertype.user_id).subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="productExcelDownload.xlsx";
                    link.click();
                    
      });
    }
  

}
file: File;
filesToUpload: Array<File>;

onChange(fileInput: any) {
  this.filesToUpload = <Array<File>>fileInput.target.files;
}


UploadExcel(){
  debugger
  console.log(this.filesToUpload)
      var formData: any = new FormData();
    // formData.append("app_id",15); 
 
  this.makeFileRequest("http://13.236.78.106/api/uploadacmeexcel_new",formData, this.filesToUpload).then((result) => {
       console.log('jhdbjhbjhsdbfhsdb ',result); 
        if(result['Success']==true){
          //  alert('excel updated successfully');
          this.snackBar.open("Excel Uploaded Successfully", "Done");
   // window.setTimeout(() => {
   //           this.Router.navigate(['productlist']);
   //         }, 200);
     this.Router.navigate(['approve-product']);
    // location.reload();
         }
         else if(result['Success']==false){
          this.snackBar.open(result['Message'], "Done");
         }
         else {
      
         }
     }, (error) => {
       console.error(error);
     });
 
 }

makeFileRequest(url: string, formData: FormData, files: Array<File>) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    for (var i = 0; i < files.length; i++) {
      formData.append("file", files[i], files[i].name);
    }
  
    xhr.onreadystatechange = function () { 
      if (xhr.readyState == 4) {
        if (xhr.status == 200) {
          resolve(JSON.parse(xhr.response));
        } else {
          reject(xhr.response);
        }
      }
    }
    xhr.open("POST", url, true);
    xhr.send(formData);
  });
}

}


