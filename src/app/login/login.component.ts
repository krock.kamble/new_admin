import { Component, OnInit } from '@angular/core';
import { User } from '../_models/User';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  alertClass;
  errorMsg;
  user: User;

  constructor(private fb: FormBuilder, public _datacalls: DatacallsService, private Router: Router) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  doLogin() {
    console.log('inside login')
    let formData = {
      'mobile_no': this.loginForm.value.username,
      'password': this.loginForm.value.password  };

    this._datacalls.POSTLogin(formData).subscribe(data => {

      console.log('data',data.result);

      if (data.result.length > 0 && data.result[0].delete_status==0) {
        console.log('tyyyyyyyyyyyyyyy', data.result[0].delete_status)
        if (data.result) {
          this.user = { "mobile": data.result[0].mobile_no, "user": data.result[0].usertype, "user_id": data.result[0].id, "name": data.result[0].name,"allow_user": data.result[0].allow_user,"email":data.result[0].email};

          sessionStorage.setItem('currentuser', JSON.stringify(this.user));
          console.log('current:', this.user)

          if (this.user.user == 'Admin') {
            this.Router.navigate(['dashboard'])
          }
          else {
            this.alertClass = "alert alert-danger text-center alert-dismissible";
            this.errorMsg = 'Something went wrong';
          }
        }
      }
      else {
        this.alertClass = "alert alert-danger text-center alert-dismissible";
        this.errorMsg = 'Wrong username or password';
      }
    });
  }  
   
}

