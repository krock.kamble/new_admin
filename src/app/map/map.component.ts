import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  posts;
  id;
  lat;
  lng;
  type;
  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) { 

  }

  ngOnInit() {


    this.id=this.activatedRoute.snapshot.params['id'];

    console.log('iddd',this.id);
    this._DatacallsService.get_user_master(this.id,0).subscribe(posts => {
      
      this.posts= posts.result;
     
      console.log('lat yet ahe',this.posts);

      if(this.posts[0].coordinates == null){
        this.lat = 0;
        this.lng = 0;
      }
      else{

        this.lat =  posts.result[0].coordinates.x;

        this.lng =  posts.result[0].coordinates.y;

      }

  });
  // this.loop();
  }
  
  loop(){
    window.setTimeout(() => {
      this.ngOnInit();
    }, 25000);
  }
}
