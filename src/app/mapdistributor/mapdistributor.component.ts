import { Component, OnInit, ViewChild } from '@angular/core';
import { DatacallsService } from '../datacalls.service';
import { Router } from '../../../node_modules/@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';
@Component({
  selector: 'app-mapdistributor',
  templateUrl: './mapdistributor.component.html',
  styleUrls: ['./mapdistributor.component.css']
})
export class MapdistributorComponent implements OnInit {


  posts;
  postsSearch;
  totalPosts;
  type;
  searchForm;
  json = {};
  kiran;
  brand_id;
  category_id;
  sub_category_id;
  totalPostsSearch;
  checkDistributor = [];
  usertype;
  posts4;
  posts1;
  posts2;
  posts3;
  distributor;
  dataSource;
  sel=false;
  allsel=false;
  intsel=false;
  showPosts = 12;
  finished = 'true';
  Distributor_ids;
  filteredData;
  jsonPara3 = {};
  jsonPara4 = {};
  config = new MatSnackBarConfig();

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    this.filteredData = this.dataSource.filteredData
    var form = {
      "product_search": filterValue
    }

    this._DatacallsService.GETProductSearch(form).subscribe(posts => {
      this.posts = posts.result;
      this.totalPostsSearch = this.posts;
      this.postsSearch = posts.result.slice(0, this.showPosts);
      // if(posts.result.length == 0){
      //   this.finished = 'false';
      // }
      console.log('this', this.posts)
    });

  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _fb: FormBuilder, private _DatacallsService: DatacallsService, private Router: Router, public snackBar: MatSnackBar) {
    this.searchForm = this._fb.group({
      type: [''],
      brand_id: [''],
      category_id: [''],
      sub_category_id: ['']
    });

  }

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    this.distributor = JSON.parse(localStorage.getItem('data'));
    console.log('distributor yrtoy', this.distributor);

    this._DatacallsService.GETProductMaster(null, this.usertype.user == 'Admin' ? null : this.usertype.user_id, null, null, null, null, null, 0,0).subscribe(posts => {

      console.log('posts.result :', posts.result)

      this._DatacallsService.mapdistributor.subscribe(res => this.Distributor_ids = res)
      console.log('id yet ahet', this.Distributor_ids);

      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }


      //      this.totalPosts = posts.result;
      // if (this.totalPosts != undefined) {
      //   this.posts = posts.result.slice(0, this.showPosts);
      //   if (this.totalPosts.length == 0) {
      //     this.finished = 'undefined';
      //   }
      // }

    });

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'))
    //user datacalls
    this._DatacallsService.GETUserMaster(this.usertype.user, this.usertype.user_id, 0).subscribe(posts => {
      this.kiran = posts.result;
      console.log('GETUserMaster', this.kiran[0].usertype);
      console.log('data yetoy', this.kiran);
    });

    this._DatacallsService.GETUserMaster(null, null, 0).subscribe(posts => {
      this.posts4 = posts.result;
      console.log('GETUserMaster', this.kiran[0].usertype);
      console.log('data yetoy', this.kiran);
    });
    //brand datacalls
    this._DatacallsService.GETBrandMaster(null, 0,null).subscribe(posts => {
      this.posts1 = posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //category datacalls
    this._DatacallsService.GETCategoryMaster(null, 0,null).subscribe(posts => {
      this.posts2 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //subcategory datacalls
    this._DatacallsService.GETSubCategoryMaster(null, null, 0).subscribe(posts => {
      this.posts3 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });


  }

  alltogglecheck(b,array){
    // if(b==true){
    //   this.checkDistributor=[];   
    //   array.forEach(element => {this.checkDistributor.push(element.product_id)})
     
    //   this.sel=!this.sel
    //   console.log('all_id',this.checkDistributor);
    //   console.log('chacked',b);
      
  
    // }  
    
    if (b == true) {
      this.checkDistributor = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkDistributor.push(element.product_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkDistributor =this.filteredData.map(x =>  x.product_id)
        console.log('this.checkDistributor',this.checkDistributor)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkDistributor);
      console.log('chacked', b);


    }
    else {
      this.sel=!this.sel
      this.checkDistributor=[]
      console.log('unchaked',b)
    }
  }
  onchange(a, id) {

    console.log(a)
    console.log(id)

    if (a == true && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer') && this.kiran[0].allow_user == 1) {
      console.log('user type', this.kiran[0].usertype)
      this.jsonPara3 = { "online_status": 1, "product_id": id }
      this._DatacallsService.change_status(this.jsonPara3).subscribe(posts => {
        this.posts = posts.result
        this.snackBar.open('you allow this produt to sale on E-commerce portal.', '', {
          duration: 4000

        });
      });
    }
    else {
      console.log('direct ithe yetoy')
      if (a == false && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer') && this.kiran[0].allow_user == 1) {
        this.jsonPara4 = { "online_status": 0, "product_id": id }
        this._DatacallsService.change_status(this.jsonPara4).subscribe(posts => {
          this.posts = posts.result
          this.snackBar.open('you are not allowing this products to sale on E-commerce portal.', '', {
            duration: 4000

          });
        });
      }
      else {
        this.snackBar.open('you are not allow to sale products on E-commerce portal.', '', {
          duration: 4000

        });
      }

    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
     
      this.checkDistributor.push(id)
      console.log('data',this.checkDistributor)
    }
    else {
      this.checkDistributor =deleteintarray(this.checkDistributor,id)
      console.log('deletearray :',this.checkDistributor);
     
    }
    console.log('check thi list ',this.checkDistributor)
    console.log('length ',length)
    this.intsel = this.checkDistributor.length != 0 && this.checkDistributor.length != length ? true : false;
    this.allsel=!this.intsel && this.checkDistributor.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    //this.shareData(this.checkProduct);
  }
  // sendIDS(a, id) {
  //   if (a == true) {
  //     this.checkDistributor.push(id)
  //     console.log('a', a);
  //     console.log('id', id);
  //   }
  //   else {
  //     const index = this.checkDistributor.indexOf(id);
  //     if (index !== -1) {
  //       this.checkDistributor.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // }

  shareData(data1) {
    this._DatacallsService.sendAnything1(data1) // to send something
    console.log('he pan yetoy', data1);
  }

  // onScrollDown() {
  //   this.finished = 'true'

  //   if (this.postsSearch != undefined) {
  //     this.showPosts += this.showPosts
  //     this.postsSearch = this.totalPostsSearch.slice(0, this.showPosts);
  //     console.log("inside if scrolled!!", this.posts);
  //     if (this.postsSearch.length == this.totalPostsSearch.length) {
  //       this.finished = 'false'
  //     }
  //   }
  //   else {
  //     this.showPosts += this.showPosts
  //     this.posts = this.totalPosts.slice(0, this.showPosts);
  //     console.log("inside else scrolled!!", this.posts);
  //     if (this.posts.length == this.totalPosts.length) {
  //       this.finished = 'false'
  //     }
  //   }

  // }

  setDistributor() {

    JSON.stringify(this.distributor), this.checkDistributor

    let formData = {
      "distributers_id": JSON.stringify(this.distributor.data),
      "product_id": JSON.stringify(this.checkDistributor),
    }

    this._DatacallsService.setDistributor(formData).subscribe(posts => {
      this.posts = posts.result
      console.log('product_id', this.checkDistributor);
      console.log('distributor_id', this.distributor);
      localStorage.removeItem('data')

      this.Router.navigate(['mappeddistributor']);
    })

  }
  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        type: [''],
        brand_id: [''],
        category_id: [''],
        sub_category_id: ['']
      })
    }
  }
  onSubmit() {
    this.type = this.searchForm.value.type == '' ? null : this.searchForm.value.type;
    console.log('usertype', this.type);
    console.log('usertype1', this.searchForm.value.type)
    this.brand_id = this.searchForm.value.brand_id == '' ? null : this.searchForm.value.brand_id;
    this.category_id = this.searchForm.value.category_id == '' ? null : this.searchForm.value.category_id;
    this.sub_category_id = this.searchForm.value.sub_category_id == '' ? null : this.searchForm.value.sub_category_id
    this.json = {
      "product_id": null,
      "user_id": this.usertype.user_id,
      "usertype": this.type,
      "brand_id": this.brand_id,
      "category_id": this.category_id,
      "sub_category_id": this.sub_category_id,
      "show_products_users": null,
      "delete_status": 0,
      "approve":0
    }
    this._DatacallsService.POST_ProductMaster(this.json).subscribe(posts => {

      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

  }

}
