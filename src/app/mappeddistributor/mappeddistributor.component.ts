import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';
@Component({
  selector: 'app-mappeddistributor',
  templateUrl: './mappeddistributor.component.html',
  styleUrls: ['./mappeddistributor.component.css']
})
export class MappeddistributorComponent implements OnInit {

  posts;
  category_id;
  usertype;
  dataSource;
  sel=false;
  allsel=false;
  intsel=false;
  filteredData;
  checkProduct=[];
  config = new MatSnackBarConfig();
  // categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    
    this.filteredData = this.dataSource.filteredData    
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    // this.categoryForm = this._formBuilder.group({
    //   category_id: [0],
    //   category_name: ['', Validators.required]
    // });
  }

  ngOnInit() {

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));
    console.log('data1230',this.usertype.user_id);
    this._DatacallsService.GETMappedDistributors(this.usertype.user_id).subscribe(posts => {
      
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator; 
      }
    });
  }

  alltogglecheck(b,array){
    // if(b==true){
    //   this.checkProduct=[];   
    //   array.forEach(element => {this.checkProduct.push(element.product_id)})
     
    //   this.sel=!this.sel
    //   console.log('all_id',this.checkProduct);
    //   console.log('chacked',b);
      
  
    // }  
    
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.product_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.product_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }
  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
     
      this.checkProduct.push(id)
      console.log('data',this.checkProduct)
    }
    else {
      this.checkProduct =deleteintarray(this.checkProduct,id)
      console.log('deletearray :',this.checkProduct);
     
    }
    console.log('check thi list ',this.checkProduct)
    console.log('length ',length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel=!this.intsel && this.checkProduct.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    //this.shareData(this.checkProduct);
  }
  // sendIDS(a, id) {
  //   if (a == true) {
  //     this.checkProduct.push(id)
  //     console.log('a',a);
  //     console.log('id',id);
  //   }
  //   else {
  //     const index = this.checkProduct.indexOf(id);
  //     if (index !== -1) {
  //       this.checkProduct.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // }

  onClick(){

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    if(this.usertype.user=='Admin'){
      this.Router.navigate(['dashboard']);
    }
    else if(this.usertype.user=='Manufacturer'){
      this.Router.navigate(['mf-dashboard']);
    }
    else{
      this.Router.navigate(['d-dashboard']);
    }

  }

  delete(){
    
    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one mapped user.', '', {
        duration: 4000

      });
    }
    else{
    
    var data={
      "product_id":this.checkProduct
    }
      this._DatacallsService.DELETEMdistributerproduct(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.snackBar.open('Mapped users removed successfully.', '', {
      duration: 4000
     
    });
  

    setTimeout(function(){
      // location.reload();
      this.ngOnInit();
    },2000);
      });
    }
      
    }


}
