import { Component, OnInit, ViewChild, ElementRef,Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-mappedusers',
  templateUrl: './mappedusers.component.html',
  styleUrls: ['./mappedusers.component.css']
})
export class MappedusersComponent implements OnInit {
  posts;
  category_id;
  users;
  checkCategory = [];
  checkBrand = [];
  dataSource;
  dataSource1;
  posts1;
  sel=false;
  allsel=false;
  intsel=false;

  sel1=false;
  allsel1=false;
  intsel1=false;
  posts3;
  posts4;
  filteredData;
  user_ids;
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    
    this.filteredData = this.dataSource.filteredData
  }

  applyFilter1(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource1.filter = filterValue;

    
    this.filteredData = this.dataSource1.filteredData
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute,public dialog: MatDialog) {
    this.categoryForm = this._formBuilder.group({
      category_id: [0],
      category_name: ['', Validators.required]
    });
  }

  ngOnInit() {

    this.users =  JSON.parse(localStorage.getItem('data'));
    console.log('distributor yrtoy',this.users);

   


    this._DatacallsService.GETCategoryMaster(null,0,null).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    
    this._DatacallsService.GETBrandMaster(null,0,null).subscribe(posts => {
      console.log('brands',this.posts)
      this.posts1 = posts.result;
      this.dataSource1 = new MatTableDataSource(this.posts1);
      this.dataSource1.sort = this.sort;
      if (this.posts1 != undefined) {
        this.dataSource1.paginator = this.paginator;
      }
    });

    this._DatacallsService.mapdistributor.subscribe(res => this.user_ids = res)
    console.log('id yet ahet',this.user_ids);
  }

  alltogglecheck(b,array){
    // if(b==true){
    //   this.checkCategory=[];   
    //   array.forEach(element => {this.checkCategory.push(element.category_id)})
     
    //   this.sel=!this.sel
    //   console.log('all_id',this.checkCategory);
    //   console.log('chacked',b);
      
  
    // }  
    if (b == true) {
      this.checkCategory = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkCategory.push(element.category_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkCategory =this.filteredData.map(x =>  x.category_id)
        console.log('this.checkProduct',this.checkCategory)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkCategory);
      console.log('chacked', b);


    }

    else {
      this.sel=!this.sel
      this.checkCategory=[]
      console.log('unchaked',b)
    }
  }

  alltogglecheck1(b,array){
   
    // if(b==true){
    //   this.checkBrand=[];   
    //   array.forEach(element => {this.checkBrand.push(element.brand_id)})
     
    //   this.sel1=!this.sel1
    //   console.log('all_id',this.checkBrand);
    //   console.log('chacked',b);
      
  
    // }  

    if (b == true) {
      this.checkBrand = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkBrand.push(element.brand_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkBrand =this.filteredData.map(x =>  x.brand_id)
        console.log('this.checkProduct',this.checkBrand)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkBrand);
      console.log('chacked', b);


    }
    else {
      this.sel1=!this.sel1
      this.checkBrand=[]
      console.log('unchaked',b)
    }
  }

  show_category_user(category_id){

 this._DatacallsService.GETCategory_user(category_id).subscribe(posts => {
      this.posts3 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

  }


  show_brand_user(brand_id){

this._DatacallsService.GETBrand_user(brand_id).subscribe(posts => {
      console.log('brands',this.posts)
      this.posts4 = posts.result;
      this.dataSource1 = new MatTableDataSource(this.posts1);
      this.dataSource1.sort = this.sort;
      if (this.posts1 != undefined) {
        this.dataSource1.paginator = this.paginator;
      }
    });

  }
  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
     
      this.checkCategory.push(id)
      console.log('data',this.checkCategory)
    }
    else {
      this.checkCategory =deleteintarray(this.checkCategory,id)
      console.log('deletearray :',this.checkCategory);
     
    }
    console.log('check thi list ',this.checkCategory)
    console.log('length ',length)
    this.intsel = this.checkCategory.length != 0 && this.checkCategory.length != length ? true : false;
    this.allsel=!this.intsel && this.checkCategory.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    //this.shareData(this.checkProduct);
  }
  // sendIDS(a, id) {
  //   console.log('a',a);
  //   console.log('id',id);
  //   if (a == true) {
  //     this.checkCategory.push(id)
  //   }
  //   else {
  //     const index = this.checkCategory.indexOf(id);
  //     if (index !== -1) {
  //       this.checkCategory.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // }
  sendIDS1(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
     
      this.checkBrand.push(id)
      console.log('data',this.checkBrand)
    }
    else {
      this.checkBrand =deleteintarray(this.checkBrand,id)
      console.log('deletearray :',this.checkBrand);
     
    }
    console.log('check thi list ',this.checkBrand)
    console.log('length ',length)
    this.intsel = this.checkBrand.length != 0 && this.checkBrand.length != length ? true : false;
    this.allsel=!this.intsel && this.checkBrand.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    //this.shareData(this.checkProduct);
  }
  // sendIDS1(a, id) {
  //   console.log('a',a);
  //   console.log('id',id);
  //   if (a == true) {
  //     this.checkBrand.push(id)
  //   }
  //   else {
  //     const index = this.checkBrand.indexOf(id);
  //     if (index !== -1) {
  //       this.checkBrand.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // }

  setUsers(){

    JSON.stringify(this.users),this.checkCategory,this.checkBrand
    
    let formData={
      "users_id":JSON.stringify(this.users.data),
      "category_id":JSON.stringify(this.checkCategory),
      "brand_id":JSON.stringify(this.checkBrand)
    }

    this._DatacallsService.setUsers(formData).subscribe(posts => {
      this.posts = posts.result
      console.log('category_id',this.checkCategory);
      console.log('brand_id',this.checkBrand);
      localStorage.removeItem('data')

    })

  }
  delete(){
    var data={
      "category_id":this.checkCategory
    }

    if(this.checkCategory.length==0){
      this.snackBar.open('please select atleast one mapped-category.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.DELETECatuser(data).subscribe(posts => {
    console.log('checkCategory',this.checkCategory);
    // this.Router.navigate(['deleted-model']);
    this.snackBar.open('Users from these categories remove successfully.', '', {
      duration: 4000
     
    });
    // location.reload();

    setTimeout(function(){
      location.reload();
    },1000);

      });
    
    }
    }


    delete1(){
      var data1={
        "brand_id":this.checkBrand
      }

      if(this.checkBrand.length==0){
        this.snackBar.open('please select atleast one mapped-brand.', '', {
          duration: 4000
      
        });  
      }
      else{
        this._DatacallsService.DELETEBranduser(data1).subscribe(posts => {
      console.log('checkBrand',this.checkBrand);
      // this.Router.navigate(['deleted-model']);
      this.snackBar.open('Users from these brands remove successfully.', '', {
        duration: 4000
       
      });

      setTimeout(function(){
        location.reload();
      },1000);

        });
      }
        
      }

      openDialog(id,type): void {
        console.log('delete this id',id)
     console.log('type',type);
        const dialogRef = this.dialog.open(catDialog, {
          width: '250px',
          data: {"id":id,"type":type}  });
      }


    //   openDialog1(id,brand): void {
    //     console.log('delete this id',id)
    //  console.log('brand_yetoyyyyy',brand);
    //     const dialogRef = this.dialog.open(catDialog, {
    //       width: '250px',
    //       data: {"id":id,"brand":"brand"}  });
    //   }

}




@Component({
  selector: 'catDialog',
  templateUrl: 'catDialog.html',
  providers : [DatacallsService]
})
export class catDialog {
  jsonPara2={};
  deleteid;
  posts4;
 
  constructor(
    public dialogRef: MatDialogRef<catDialog>,
    @Inject(MAT_DIALOG_DATA) public data,private _DatacallsService:DatacallsService) {

this.data.id;
// this.data.category;
// this.data.brand;
console.log('dadadadadata', this.data.id);
console.log('categorrrrrrrrrrrrry',this.data.category);

if(this.data.type=='category'){
  this._DatacallsService.GETCategory_user(this.data.id).subscribe(posts => {
    this.posts4 = posts.result;
  });
  
}
else if(this.data.type=='brand'){
  this._DatacallsService.GETBrand_user(this.data.id).subscribe(posts => {
    this.posts4 = posts.result;
  });
}

console.log('ttttttaaaa0',this.posts4);
    }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
