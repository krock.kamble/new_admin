
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupName } from '@angular/forms';
import { DatacallsService } from '../datacalls.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as _ from "lodash";
import { ProductsComponent } from '../products/products.component';

@Component({
  selector: 'app-mapproducts',
  templateUrl: './mapproducts.component.html',
  styleUrls: ['./mapproducts.component.css'],
  // providers: [DatacallsService,ProductsComponent]
})
export class MapproductsComponent implements OnInit {

  product_id;
  sub;
  categories;
  subcategories;
  manufacturers;
  total_make;
  total_models;
  total_years;
  total_vehicles;
  usertype;
  makes;
  models;
  variants;
  sub_models;
  year;
  products = [];
  product_ids;
  checkvehicle = [];
  mappingGroup: FormGroup;
  config = new MatSnackBarConfig();
  outdata;
  yearfrom;
  from;
  to;
  yearto;
  showPN = 'hidden';

  constructor(private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.mappingGroup = this._formBuilder.group({
      make_id: [''],
      model_id: [''],
      variant_id: [''],
      sub_model_id: [''],
      // year_id: [''],
      from:[''],
      to:[''],
      year_to:[''],
      year_from:['']
    });
  }

  ngOnInit() {

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    this._DatacallsService.products.subscribe(res => this.product_ids = res)
    //console.log('pro', JSON.stringify(this.product_ids).replace(/\[/g, '{').replace(/]/g, '}'))
    for (var i = 0; i < this.product_ids.length; i++) {
      this._DatacallsService.GETProductMaster(this.product_ids[i], null, null, null, null, null, null,0,0).subscribe(posts => {
        this.products.push(posts.result[0]);
      });
      //console.log('pro', this.products)
    }

    this._DatacallsService.GETMakeMaster(null,0).subscribe(posts => {
      this.makes = posts.result;
      var test;
      console.log(this.makes[0].make_name.charAt(0));
    });
  }

  getModel() {
    console.log('inside getModel');
    this.showPN = 'true';

    this.models = [];
    this.total_vehicles = [];
    for (var i = 0; i < this.mappingGroup.value.make_id.length; i++) {
      this._DatacallsService.GETModelMaster(null, this.mappingGroup.value.make_id[i],0).subscribe(posts => {
        for (var j = 0; j < posts.result.length; j++) {
          this.models.push(posts.result[j]);
        }
      });
      this._DatacallsService.GETVehicleMasterFilter(null, this.mappingGroup.value.make_id[i], null, null, null, null, null, null,null,0).subscribe(posts => {
        for (var k = 0; k < posts.result.length; k++) {
          this.total_vehicles.push(posts.result[k]);
        }
      });
    }
  }

  getVariant() {
    this.variants = [];
    this.total_vehicles = [];
    // for (var k = 0; k < this.mappingGroup.value.make_id.length; k++) {
    for (var i = 0; i < this.mappingGroup.value.model_id.length; i++) {
      this._DatacallsService.GETVehicleMasterFilter(null, null, this.mappingGroup.value.model_id[i], null, null, null, null, null,null,0).subscribe(posts => {
        for (var j = 0; j < posts.result.length; j++) {
          this.variants.push(posts.result[j]);
        }
      });
      this._DatacallsService.GETVehicleMasterFilter(null, null, this.mappingGroup.value.model_id[i], null, null, null, null, null,null,0).subscribe(posts => {
        for (var k = 0; k < posts.result.length; k++) {
          this.total_vehicles.push(posts.result[k]);
        
        }
      });
    }
    // }
    // console.log('dd',this.variants)
  }

  getSubModel() {
    this.sub_models = [];
    this.total_vehicles = [];
    for (var i = 0; i < this.mappingGroup.value.make_id.length; i++) {
      for (var j = 0; j < this.mappingGroup.value.model_id.length; j++) {
        for (var l = 0; l < this.mappingGroup.value.variant_id.length; l++) {
          this._DatacallsService.GETVehicleMasterFilter(null, this.mappingGroup.value.make_id[i], this.mappingGroup.value.model_id[j], this.mappingGroup.value.variant_id[l], null, null, null, null,null,0).subscribe(posts => {
            for (var k = 0; k < posts.result.length; k++) {

              if(posts.result[k].sub_model_name !=null){
         
                var json={
                  "sub_model_id":posts.result[k].sub_model_id,
                  "sub_model":posts.result[k].sub_model_name,
                  "month_name":posts.result[k].month_name,
                  "year_name":posts.result[k].year_name,
                  "to_month":posts.result[k].to_month,
                  "to_year":posts.result[k].to_year
                }
                this.sub_models.push(json);
                setTimeout(() => {
               this.sub=  _.uniqBy(this.sub_models,'sub_model')
        
    
                }, 1000);
                
                
                }
          

            }
          });
          this._DatacallsService.GETVehicleMasterFilter(null, this.mappingGroup.value.make_id[i], this.mappingGroup.value.model_id[j], this.mappingGroup.value.variant_id[l], null, null, null, null,null,0).subscribe(posts => {
            for (var z = 0; z < posts.result.length; z++) {
              this.total_vehicles.push(posts.result[z]);
            }
          });
        }
      }
    }
  }

  getYear() {
    this.total_vehicles = [];
    this.yearto = [];
    this.yearfrom = [];
 
    for (var i = 0; i < this.mappingGroup.value.make_id.length; i++) {
      for (var j = 0; j < this.mappingGroup.value.model_id.length; j++) {
        for (var k = 0; k < this.mappingGroup.value.variant_id.length; k++) {
          for (var l = 0; l < this.mappingGroup.value.sub_model_id.length; l++) {
           
            this._DatacallsService.GETVehicleMasterFilter(null, this.mappingGroup.value.make_id[i], this.mappingGroup.value.model_id[j], this.mappingGroup.value.variant_id[k], this.mappingGroup.value.sub_model_id[l], null,null,null,null,0).subscribe(posts => {
              for (var z = 0; z < posts.result.length; z++) {
                this.total_vehicles.push(posts.result[z]);
              }
            });
          }
        }  
      }
    }
  }

  getVehicle(value) {
    console.log('to', this.mappingGroup.value.to)
    console.log('from', this.mappingGroup.value.from)
    console.log('year_to', value)
    console.log('year_from', this.mappingGroup.value.year_from)

    this.total_vehicles = [];
    for (var i = 0; i < this.mappingGroup.value.make_id.length; i++) {
      
      for (var j = 0; j < this.mappingGroup.value.model_id.length; j++) {
        //console.log('year2', this.mappingGroup.value.year_id, '--------------->', value)
        for (var k = 0; k < this.mappingGroup.value.variant_id.length; k++) {
          //console.log('year3', this.mappingGroup.value.year_id, '--------------->', value)
          for (var l = 0; l < this.mappingGroup.value.sub_model_id.length; l++) {
           // console.log('year4', this.mappingGroup.value.sub_model_id.length,'--------------->',value)
            //for (var m = 0; m < this.mappingGroup.value.year_id.length; m++) {
             // console.log('year', this.mappingGroup.value.year_id,'--------------->',value)
              this._DatacallsService.GETVehicleMasterFilter(null, this.mappingGroup.value.make_id[i], this.mappingGroup.value.model_id[j], this.mappingGroup.value.variant_id[k], this.mappingGroup.value.sub_model_id[l], this.mappingGroup.value.from, this.mappingGroup.value.to, this.mappingGroup.value.year_from,value,0).subscribe(posts => {
                for (var z = 0; z < posts.result.length; z++) {
                  this.total_vehicles.push(posts.result[z]);
                }
                
              });
            }
          }
        }
      }
    
    console.log('hey',this.total_vehicles)
  }
  // getMonthlyVehicle(){
  //   console.log('to', this.mappingGroup.value.to)
  //   console.log('to', this.mappingGroup.value.from)
  //   this.total_vehicles = [];
  //   for (var i = 0; i < this.mappingGroup.value.make_id.length; i++) {
  //     for (var j = 0; j < this.mappingGroup.value.model_id.length; j++) {
  //       for (var k = 0; k < this.mappingGroup.value.variant_id.length; k++) {
  //         for (var l = 0; l < this.mappingGroup.value.sub_model_id.length; l++) {
  //           for (var m = 0; m < this.mappingGroup.value.year_id.length; m++) {
  //             this._DatacallsService.GETVehicleMasterFilter(null, this.mappingGroup.value.make_id[i], this.mappingGroup.value.model_id[j], this.mappingGroup.value.variant_id[k], this.mappingGroup.value.sub_model_id[l], this.mappingGroup.value.year_id[m], ).subscribe(posts => {
  //               for (var z = 0; z < posts.result.length; z++) {
  //                 this.total_vehicles.push(posts.result[z]);
  //               }
  //             });
  //           }
  //         }
  //       }
  //     }
  //   }
  //   console.log('hey', this.total_vehicles)

  // }

  // onChange(category_id) {
  //   this._DatacallsService.GETSubCategoryMaster(category_id, null,0).subscribe(posts => {
  //     this.subcategories = posts.result;
  //   });
  // }

  matcheck(a, id) {
    console.log(a)
    if (a == true) {
      this.checkvehicle.push(id)
    }
    else {
      const index = this.checkvehicle.indexOf(id);
      if (index !== -1) {
        this.checkvehicle.splice(index, 1);
      }
    }
    console.log("checkvehicle", this.checkvehicle)
  }

  submit() {

if(this.checkvehicle.length!=0){

  console.log('checked vehicle',this.checkvehicle.length)
  var form: any = new FormData();

  form.append("product_id", JSON.stringify(this.product_ids).replace(/\[/g, '{').replace(/]/g, '}'))
  form.append("vehicle_id", JSON.stringify(this.checkvehicle).replace(/\[/g, '{').replace(/]/g, '}'));

  this._DatacallsService.POSTVehicleProductMapping(form).subscribe(posts => {
    if (posts.result) {
      if (posts.result[0].vehicle_product_mapping == -1) {
        this.config.duration = 3000
        this.snackBar.open("This Product Is Already Mapped", "Done", this.config);
      }
      else {
        this.config.duration = 3000
        this.snackBar.open("Product Mapped Successfully", "Done", this.config);

        window.setTimeout(() => {
          this.Router.navigate(['products']);
        }, 3000);
      }
    }
    else {
      this.snackBar.open("Please try again later", "Done");
    }
  });
}
else{


  this.snackBar.open('please select atleast one vehicle.', '', {
    duration: 4000

  });

}
   
  }

}