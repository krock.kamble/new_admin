import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-mapusers',
  templateUrl: './mapusers.component.html',
  styleUrls: ['./mapusers.component.css']
})
export class MapusersComponent implements OnInit {
  posts;
  posts1;
  posts2;
  category_id;
  checked_category=[];
  checked_brand=[];
  cat;
  bat;
  users;
  checkCategory = [];
  checkBrand = [];
  checkCategory_user = [];
  checkBrand_user = [];
  dataSource;
  dataSource1;
  user_ids;
  filteredData
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  applyFilter1(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource1.filter = filterValue;

    
    this.filteredData = this.dataSource1.filteredData
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.categoryForm = this._formBuilder.group({
      category_id: [0],
      category_name: ['', Validators.required]
    });
  }

  ngOnInit() {

    this.users =  JSON.parse(localStorage.getItem('data'));
    console.log('distributor yrtoy',this.users);
    for(var t = 0 ; t < this.users.data.length; t++){
    this.checkBrand_user.push(this.users.data[t].id)
    this.checkCategory_user.push(this.users.data[t].id)
    }

    console.log('initial value fro checkbrand',this.checkBrand_user[0])
    this._DatacallsService.GETCategoryMaster(null,0,this.checkBrand_user[0]).subscribe(posts => {
      this.posts = posts.result;
     var a= posts.result.filter(x => x.mapped_id == 1).map(y => y.category_id)
     this.checkCategory=a;
     console.log('a',a);
     console.log('category push hotoy',this.checkCategory)
      console.log('hvasjhkvafhafh',this.posts);
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    // this._DatacallsService.GETCategoryMaster(null,0,this.checkBrand_user[0]).subscribe(posts => {
    //   this.posts1 = posts.result;
    //   for(var i=0;i<this.posts1.length;i++){
    //     this.checked_category.push(this.posts1[i].category_id)
    //     this.checkCategory.push(this.posts1[i].category_id)
    //   }
    //   console.log('check box sathi category',this.checked_category)
    // });


    this._DatacallsService.GETBrandMaster(null,0,this.checkBrand_user[0]).subscribe(posts => {
      console.log('brands',this.posts)
      this.posts = posts.result;
      var a= posts.result.filter(x => x.mapped_id == 1).map(y => y.brand_id)
     this.checkBrand=a;
     console.log('brand push hotoy',this.checkBrand)
      this.dataSource1 = new MatTableDataSource(this.posts);
      this.dataSource1.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource1.paginator = this.paginator;
      }
    });

    // this._DatacallsService.GETBrandMaster(null,0,this.checkBrand_user[0]).subscribe(posts => {
    //   this.posts2 = posts.result;
    //   for(var i=0;i<this.posts2.length;i++){
    //     this.checked_brand.push(this.posts2[i].brand_id)
    //     this.checkBrand.push(this.posts2[i].brand_id)
    //   }
    //   console.log('check box sathi brand',this.checked_brand)
    // });

    this._DatacallsService.mapdistributor.subscribe(res => this.user_ids = res)
    console.log('id yet ahet',this.user_ids);
  }

  sendIDS(a, id) {
    
    console.log('a',a);
    console.log('id',id);
    console.log("users",this.users)
    if (a == true) {
      console.log('this.checkCategory',this.checkCategory)
      this.checkCategory.push(id)
    }
    else {
      const index = this.checkCategory.indexOf(id);
      if (index !== -1) {
        this.checkCategory.splice(index, 1);
      }
    }
    //this.shareData(this.checkProduct);
  
  } 
  sendIDS1(a, id) {
    
    console.log('a',a);
    console.log('id',id);
    console.log("users",this.users)
    
    if (a == true) {
        console.log('this.checkBrand',this.checkBrand)
      this.checkBrand.push(id)
    
    }
    else {
      const index = this.checkBrand.indexOf(id);
      if (index !== -1) {
        this.checkBrand.splice(index, 1);
      }
    }
  
    //this.shareData(this.checkProduct);
  }

  setUsers(){

    JSON.stringify(this.users),this.checkCategory,this.checkBrand
    
    let formData={
      "users_id":this.checkBrand_user[0],
      "category_id":JSON.stringify(this.checkCategory),
      "brand_id":JSON.stringify(this.checkBrand)
    }  

    this._DatacallsService.setUsers(formData).subscribe(posts => {
      this.posts = posts.result
      console.log('category_id',this.checkCategory);
      console.log('brand_id',this.checkBrand);
      localStorage.removeItem('data')
      this.Router.navigate(['mappedusers']);
    })

  }


}

 