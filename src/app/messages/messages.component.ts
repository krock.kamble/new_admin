import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  posts;
  sel=false;
  brand_id;
  checkProduct = [];
  dataSource;
  showLogo;
  data=[];
  json={};
  imagepath1;
  filteredData;
  allsel=false;
  uploadImg;
  intsel=false;
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
   
  } 

  ngOnInit() {
    var form ={
      "message_id":null
    }
    this._DatacallsService.ViewMessage(form).subscribe(posts => {
      this.posts = posts.result;
     
 
      console.log(this.posts.hasValue);
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  alltogglecheck(b,array){
  

    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.message_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.message_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }

    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai',id);
    
      this.checkProduct.push(id)
      console.log('data',this.checkProduct)
    }
    else {
      this.checkProduct =deleteintarray(this.checkProduct,id)
      console.log('deletearray :',this.checkProduct);  
    }
    console.log('check thi list ',this.checkProduct)
    console.log('length ',length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel=!this.intsel && this.checkProduct.length !=0
    console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    //this.shareData(this.checkProduct);
  }



delete(){
var data={
  "message_id":this.checkProduct
}


if(this.checkProduct.length==0){
  this.snackBar.open('please select atleast one message.', '', {
    duration: 4000

  });  
}
else{

  this._DatacallsService.delete_message(data).subscribe(posts => {
console.log('checkproduct',this.checkProduct);
this.ngOnInit();
  });
}
  
}

}


