import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DatacallsService } from '../datacalls.service';

@Component({
  selector: 'app-mf-dashboard',
  templateUrl: './mf-dashboard.component.html',
  styleUrls: ['./mf-dashboard.component.css']
})
export class MFDashboardComponent implements OnInit {

  vehicles;
  products;
  users;
  usertype;
  mappedproduct;
  posts;
  constructor(private _DatacallsService: DatacallsService,) { }

  ngOnInit() {

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));


    this._DatacallsService.viewmanufacturerdashboard(this.usertype.user_id).subscribe(posts => {
      
      this.posts = posts.result[0];
      console.log('data',this.posts);

      this.vehicles = this.posts['vehiclecount'];
      this.products = this.posts['productcount'];
      this.users = this.posts['usercount'];
    });
  }

}

