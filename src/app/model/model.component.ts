import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';
@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {

  posts;
  model_id;
  dataSource;
  sel=false;
  allsel=false;
  intsel=false;
  makes;
  filteredData;
  checkProduct=[];
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    
    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.categoryForm = this._formBuilder.group({
      model_id: [0],
      make_id: ['', Validators.required],
      model_name: ['', Validators.required],
      addresses: this._formBuilder.array([
        this.initAddress(),
      ]),
    });
  }

  ngOnInit() {

    this._DatacallsService.GETMakeMaster(null,0).subscribe(posts => {
      this.makes = posts.result;
    });

    this._DatacallsService.GETModelMaster(null, null,0).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }
  delete(){
    var data={
      "delete_status":1,
      "model_id":this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one model.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.DELETEModelMaster(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.Router.navigate(['deleted-model']);
      });
    }
      
    }
    alltogglecheck(b,array){
      // if(b==true){
      //   this.checkProduct=[];   
      //   array.forEach(element => {this.checkProduct.push(element.model_id)})
       
      //   this.sel=!this.sel
      //   console.log('all_id',this.checkProduct);
      //   console.log('chacked',b);
        
    
      // }  

      if (b == true) {
        this.checkProduct = [];
  
        if(this.filteredData == undefined){
         
          console.log("outside this.filteredData != ''")
          array.forEach(element => { this.checkProduct.push(element.model_id) })
        }else{
          console.log("inside this.filteredData",this.filteredData)
          this.checkProduct =this.filteredData.map(x =>  x.model_id)
          console.log('this.checkProduct',this.checkProduct)
  
          
        }
        
  
        this.sel = !this.sel
        console.log('all_id', this.checkProduct);
        console.log('chacked', b);
  
  
      }

      else {
        this.sel=!this.sel
        this.checkProduct=[]
        console.log('unchaked',b)
      }
    }
  
    file: File;
    filesToUpload: Array<File>;
  
  
    onChange(fileInput: any) {
      this.filesToUpload = <Array<File>>fileInput.target.files;
    }
  
  
    UploadExcel(){
      debugger
      console.log(this.filesToUpload)
          var formData: any = new FormData();
        // formData.append("app_id",15); 
     
      this.makeFileRequest("http://13.236.78.106/api/uploadmodelexcel",formData, this.filesToUpload).then((result) => {
           console.log(result);
            if(result['Success']==true){
               this.snackBar.open("Excel Uploaded Successfully", "Done");
               this.ngOnInit();
        // location.reload();
             }
             else if(result['Success']==false){
              this.snackBar.open(result['Message'], "Done");
             }
             else {
          
             }
         }, (error) => {
           console.error(error);
         });
     
     }
  
  makeFileRequest(url: string, formData: FormData, files: Array<File>) {
      return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        for (var i = 0; i < files.length; i++) {
          formData.append("file", files[i], files[i].name);
        }
      
        xhr.onreadystatechange = function () { 
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              resolve(JSON.parse(xhr.response));
            } else {
              reject(xhr.response);
            }
          }
        }
        xhr.open("POST", url, true);
        xhr.send(formData);
      });
    }
  
    downloadexcel(){
  
      this._DatacallsService.modelExcelDownload().subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="modelExcelDownload.xlsx";
                    link.click();
                    
      });
    
  }

    sendIDS(a, id, length) {
      if (a == true) {
        console.log('id araha hai',id);
        // this.json={
        //   "id":id
        // }
  
        this.checkProduct.push(id)
        console.log('data',this.checkProduct)
      }
      else {
        this.checkProduct =deleteintarray(this.checkProduct,id)
        console.log('deletearray :',this.checkProduct);
      //   console.log(this.checkProduct[0]["id"])
      // for(var i=0;i<this.checkProduct.length;i++){
      //   if(this.checkProduct[i].id==id){
      //     this.checkProduct.splice(i, 1);
      //   }
      // }
  
  
  //--> [{id:3},{id:7}]
  //-->[3,7]
  //-->"3,7"   
      }
      console.log('check thi list ',this.checkProduct)
      console.log('length ',length)
      this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
      this.allsel=!this.intsel && this.checkProduct.length !=0
      console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
      //this.shareData(this.checkProduct);
    }
    // sendIDS(a, id) {
    //   if (a == true) {
    //     this.checkProduct.push(id)
    //     console.log('a',a);
    //     console.log('id',id);
    //   }
    //   else {
    //     const index = this.checkProduct.indexOf(id);
    //     if (index !== -1) {
    //       this.checkProduct.splice(index, 1);
    //     }
    //   }
    //   //this.shareData(this.checkProduct);
    // }
  resetform() {
    this.categoryForm = this._formBuilder.group({
      model_id: [0],
      make_id: ['', Validators.required],
      model_name: ['', Validators.required],
      addresses: this._formBuilder.array([
        this.initAddress(),
      ]),
    });
    this.model_id = 0;
  }

  initAddress() {
    return this._formBuilder.group({
      variant_name: ['']
    });
  }

  addAddress() {
    const control = <FormArray>this.categoryForm.controls['addresses'];
    control.push(this.initAddress());
  }

  removeAddress(i: number) {
    const control = <FormArray>this.categoryForm.controls['addresses'];
    control.removeAt(i);
  }

  edit(id) {
    this._DatacallsService.GETModelMaster(id, null,0).subscribe(
      data => {
        this.categoryForm.patchValue({
          'model_id': data.result[0].model_id,
          'make_id': data.result[0].make_id,
          'model_name': data.result[0].model_name
        });

        const control = <FormArray>this.categoryForm.controls['addresses'];

        for (var i = 0; i < data.result[0].variant_name.length; i++) {
          control.push(this._formBuilder.group({
            variant_name: data.result[0].variant_name[i].variant_name
          }));
        }
        this.removeAddress(0);
      }
    );
  }

  submit() {
    console.log('give', this.categoryForm.value.addresses)
    var variant_names = [];
    for (var i = 0; i < this.categoryForm.value.addresses.length; i++) {
      variant_names.push(this.categoryForm.value.addresses[i].variant_name);
    }
    console.log('variant_names', variant_names)
    var form: any = new FormData();
    form.append("model_id", this.categoryForm.value.model_id)
    form.append("make_id", this.categoryForm.value.make_id)
    form.append("model_name", this.categoryForm.value.model_name)
    form.append("variant_name", JSON.stringify(variant_names).replace(/\[/g, '{').replace(/]/g, '}'))

    this._DatacallsService.POSTModelMaster(form).subscribe(posts => {
      if (posts.result) {
        if (posts.result[0].insert_model_master == -2) {
          this.snackBar.open("Model Name Already Exists", "Done", this.config);
        }
        else{
        if (this.categoryForm.value.model_id == 0) {
          this.config.duration = 3000
          this.snackBar.open("Model Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Model Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform();
  }

}


