import { DatacallsService } from '../datacalls.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';

import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-online-products',
  templateUrl: './online-products.component.html',
  styleUrls: ['./online-products.component.css']
})
export class OnlineProductsComponent implements OnInit {

  posts;
  kiran;
  postsSearch;
  totalPosts;
  jsonPara3 = {};
  jsonPara4 = {};
  dataSource;
  totalPostsSearch;
  checkProduct = [];
  usertype;
  config = new MatSnackBarConfig();
  showPosts = 12;
  finished = 'true';

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _DatacallsService: DatacallsService,private Router: Router,public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    this._DatacallsService.GETEcommerceProductMaster(null, this.usertype.user == 'Admin' ? null : this.usertype.user_id, null, null, null, null, null).subscribe(posts => 
        {

      console.log('posts.result :', posts.result)

      // this.totalPosts = posts.result;
      // if (this.totalPosts != undefined) {
      //   this.posts = posts.result.slice(0, this.showPosts);
      //   if (this.totalPosts.length == 0) {
      //     this.finished = 'undefined';
      //   }
      // }
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'))

    this._DatacallsService.GETUserMaster(this.usertype.user, this.usertype.user_id,0).subscribe(posts => {
      this.kiran = posts.result;
      console.log('GETUserMaster',this.kiran[0].usertype);
    });
  }

  sendIDS(a, id) { 
    if (a == true) {
      console.log('a',a);
      console.log('id',id);
      this.checkProduct.push(id)
    }
    else {
      const index = this.checkProduct.indexOf(id);
      if (index !== -1) {
        this.checkProduct.splice(index, 1);
      }
    }
    //this.shareData(this.checkProduct);
  } 

  shareData(data) {
    this._DatacallsService.sendAnything(data) // to send something
  }

  delete(){
    var data={
      "delete_status":1,
      "product_id":this.checkProduct
    }
      this._DatacallsService.DELETEProductMaster(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.Router.navigate(['deleted-product']);
      });
    
      
    }
    onchange(a, id) {

      console.log(a)
      console.log(id)
  
      if (a == true && (this.kiran[0].usertype == 'Distributor'||this.kiran[0].usertype=='Manufacturer' || this.kiran[0].usertype == 'Admin') && this.kiran[0].allow_user==1) {
        console.log('user type',this.kiran[0].usertype)
        this.jsonPara3 = { "online_status": 1, "product_id": id }
        this._DatacallsService.change_status(this.jsonPara3).subscribe(posts => {
          this.posts = posts.result
          this.snackBar.open('you allow this produt to sale on E-commerce portal.', '', {
            duration: 4000
           
          });
        });
      }
      else {
        console.log('direct ithe yetoy')
  if(a== false && (this.kiran[0].usertype == 'Distributor'||this.kiran[0].usertype=='Manufacturer' || this.kiran[0].usertype == 'Admin') && this.kiran[0].allow_user==1){
    this.jsonPara4 = { "online_status": 0, "product_id": id }
    this._DatacallsService.change_status(this.jsonPara4).subscribe(posts => {
      this.posts = posts.result
      this.snackBar.open('you are not allowing this products to sale on E-commerce portal.', '', {
        duration: 4000
       
      });
    });
  }
  else{
    this.snackBar.open('you are not allow to sale products on E-commerce portal.', '', {
      duration: 4000
     
    });
  }
       
      }
    }

} 

			  
			