import { Component, OnInit, NgModule,ElementRef,ViewChild, Inject } from '@angular/core';
import {DatacallsService} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router'
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
  providers : [DatacallsService]
})
export class OrderComponent implements OnInit {
  dispatch;
  config = new MatSnackBarConfig();
  delivered_extra;
  order;
  customer_id;
  jsonPara3={};
  jsonPara4={};
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches) 
    );

    displayedColumns = ['id', 'name', 'to_char','address','delivery_method', 'payment_type','order_total','shipping_charges','order_status','delete'];
    dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
 
  posts;
  deleteid;
  usertype;
  jsonPara2={};
  data={};
  constructor(private _DatacallsService:DatacallsService,public snackBar: MatSnackBar,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute,private breakpointObserver: BreakpointObserver, private fb:FormBuilder,private Router:Router,public dialog: MatDialog) { 

  }

  ngOnInit() { 

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    var user = this.usertype.user;

    this.data={"order_id":null,"customer_id":null,"users_id":null}

    this._DatacallsService.GETOrderMaster(this.data).subscribe(posts => {
      console.log(posts);
       
      this.posts = posts.Order;

      console.log('result',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
       
      this.dataSource.paginator=this.paginator
      this.dataSource.sort=this.sort
     });
     
  }

  run(event,orderid ,customer1_id) {



   console.log('event:',event);
   console.log('orderid:',orderid);
    console.log('customer1_id:',customer1_id);
     
    var formData={    
      "order_status": event,                //formdata is json created 
      "order_id":orderid,
      "customer_id":customer1_id
    };
  
    this._DatacallsService.updateorderstatus(formData).subscribe(posts => {
      // this.ngOnInit();
      console.log('Updated Successfully',posts.result);
      if(posts.result.length == 0){
        
        this.snackBar.open('Order Status Updated Successfully.', '', {
          duration: 4000
      
        });

    console.log('Updated Successfully');
      }
    else{
      console.log('Error in Updating');
    }
    });

      }
  
      delete(id){

        var data = {
          order_id:id
        }

   this._DatacallsService.delete_order(data).subscribe((data)=>{

          this.posts=data.result; 
    
          console.log('this.adddress.posts',data.result);

          if(data.result.length == 0){
        
            this.snackBar.open('Order Deleted Successfully.', '', {
              duration: 4000
        
            });
            this.ngOnInit();
        console.log('Deleted Successfully');
          }
        else{
          console.log('Error in Deleted');
        }
      
          })

      }


      openDialog(id): void {
        console.log('delete this id',id)
     
        const dialogRef = this.dialog.open(OrdersDialog, {
          width: '250px',
          data: id    });
      }
  

      
      openDialog1(id): void {
        console.log('delete this id',id)
     
        const dialogRef = this.dialog.open(ComboDialog, {
          width: '550px',
          data: id    });
      }




  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  onClick(){

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    if(this.usertype.user=='Admin'){
      this.Router.navigate(['dashboard']);
    }
    else if(this.usertype.user=='Manufacturer'){
      this.Router.navigate(['mf-dashboard']);
    }
    else{
      this.Router.navigate(['d-dashboard']);
    }

  }
 


  addproduct(){
this.Router.navigate(['add-product'])
  }

  

}

export interface UserData {
  id: string;
  product_name: string;
  product_season: string;
  quantity: string;
  product_image:string
 
}


@Component({
  selector: 'OrdersDialog',
  templateUrl: 'OrdersDialog.html',
})
export class OrdersDialog {
  jsonPara2={};
  deleteid;
  postsq;
  posts1=[];
  posts2=[];
  constructor(
    public dialogRef: MatDialogRef<OrdersDialog>,
    @Inject(MAT_DIALOG_DATA) public data,private _DatacallsService:DatacallsService,) {


      this._DatacallsService.GETview_acme_shipping_address(null,data).subscribe((data)=>{

        this.postsq=data.result;  
  
        for(var i=0;i < this.postsq.length;i++){
          if(this.postsq[i].app_id==2){
          this.posts2.push(this.postsq[i])
          }
              }
        console.log('this.adddress.posts',data.result);
    
        })

    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}



@Component({
  selector: 'ComboDialog',
  templateUrl: 'ComboDialog.html',
})
export class ComboDialog {
  jsonPara2={};
  deleteid;
  postsq;
  posts1=[];
  posts2=[];
  constructor(
    public dialogRef: MatDialogRef<ComboDialog>,
    @Inject(MAT_DIALOG_DATA) public data) {


     console.log('ddddata',data);

    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}