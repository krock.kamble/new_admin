import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  posts;
  category_id;
  sel=false;
  allsel=false;
  intsel=false;
  checkProduct=[];
  uploadImg;
  dataSource;
  imagepath1;
  data=[];
  showLogo;
  filteredData;
  posts2;
  product_id;
  config = new MatSnackBarConfig();
  productForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.productForm = this._formBuilder.group({
      product_id:[0],
      product_name: ['', Validators.required],
      category_id: ['',Validators.required],
    });
  }

  ngOnInit() {
    var json={
      "product_id":null
    }
    this._DatacallsService.view_product_master(json).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    var json2={
      "category_id":null
    }
    this._DatacallsService.view_category_master(json2).subscribe(posts => {
      this.posts2 = posts.result;
    });
  }

  alltogglecheck(b,array){
    
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.category_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.category_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }

  delete(){
    var data={
      "product_id":this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one product.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.delete_product(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.snackBar.open('product deleted.', '', {
      duration: 4000
    }); 
       this.ngOnInit();

  
      });
    
    }
    }
   
    sendIDS(a, id, length) {
      if (a == true) {
        console.log('id araha hai',id);
       
  
        this.checkProduct.push(id)
        console.log('data',this.checkProduct)
      }
      else {
        this.checkProduct =deleteintarray(this.checkProduct,id)
        console.log('deletearray :',this.checkProduct);
      
      }
      console.log('check thi list ',this.checkProduct)
      console.log('length ',length)
      this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
      this.allsel=!this.intsel && this.checkProduct.length !=0
      console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
    }



  resetform() {
    this.productForm = this._formBuilder.group({
      product_id:[0],
      category_id: ['', Validators.required],
      product_name: ['', Validators.required]
    });
    this.product_id = 0;
  }

  edit(id) {

    this.showLogo=[];
    console.log('logooooooo',this.showLogo);
    var json ={
      "product_id":id
    }
    this._DatacallsService.view_product_master(json).subscribe(
      data => {

        this.product_id = data.result[0].category_id;
        this.productForm.patchValue({
          'product_id':data.result[0].product_id,
          'category_id': data.result[0].category_id,
          'product_name': data.result[0].product_name
        });
      }
    );
  }

  submit() {  

    var bc = this.productForm.value.product_id

    var form ={
      "product_id":this.productForm.value.product_id,
      "category_id":this.productForm.value.category_id,
      "product_name":this.productForm.value.product_name
    }

    console.log('form',form);

    this._DatacallsService.insert_product_master(form).subscribe(posts => {
      if (posts.result) {
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Product Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Product Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform();
  }

}

