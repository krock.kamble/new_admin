  
import { DatacallsService } from '../datacalls.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { deleteobj, deleteintarray } from '../deletearray'
import { element } from '../../../node_modules/protractor';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
ddmm;
  posts;
  posts1;
  posts2;
  posts3;
  posts4;
  kiran;
  searchForm;
  json = {};
  sel = false;
  allsel = false;
  intsel = false;
  brand_id;
  category_id;
  sub_category_id;
  postsSearch;
  totalPosts;
  users_id;
  jsonPara3 = {};
  jsonPara4 = {};
  dataSource;

  posts5;
  product_ids=[];
  arrays=[];

  totalPostsSearch;
  checkProduct = [];
  usertype;
  update_id;
  update_country;
  public productForm: FormGroup;
  type;
  config = new MatSnackBarConfig();
  showPosts = 12;
  finished = 'true';
  filteredData;
  countries;
  country_id;
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    

    this.filteredData = this.dataSource.filteredData
    console.log('dataSource',this.dataSource.filteredData )
  }
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _fb: FormBuilder, private _DatacallsService: DatacallsService, private Router: Router, public snackBar: MatSnackBar) {

    this.searchForm = this._fb.group({
      type: [''],
      brand_id: [''],
      category_id: [''],
      sub_category_id: ['']
    });

    this.productForm = this._fb.group({

      Excelfile: ['', Validators.required]

  });
 
  } 

  ngOnInit() {
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    this.ddmm = this.usertype.user;
    
    this._DatacallsService.view_country_master(null,0).subscribe(posts => {
      this.countries = posts.result;
    });
 
 
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'))
    //user datacalls
    
    this._DatacallsService.GETUserMaster(this.usertype.user, this.usertype.user_id, 0).subscribe(posts => {
      this.kiran = posts.result;
      console.log('GETUserMaster', this.kiran[0].usertype);
      console.log('data yetoy', this.kiran);
    });


    //brand datacalls
    this._DatacallsService.GETBrandMaster(null, 0,null).subscribe(posts => {
      this.posts1 = posts.result;

      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
    //category datacalls
    this._DatacallsService.GETCategoryMaster(null, 0,null).subscribe(posts => {
      this.posts2 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    }); 
    //subcategory datacalls
    this._DatacallsService.GETSubCategoryMaster(null, null, 0).subscribe(posts => {
      this.posts3 = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    this._DatacallsService.GETProductMaster(null, this.usertype.user == 'Admin' ? null : this.usertype.user_id, null, null, null, null, null, 0,0).subscribe(posts => {

      // console.log('posts.result :', posts.result)

      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

  }

  
  alltogglecheck(b, array) {
    console.log('aaaaaaaaaa',this.filteredData )
  
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.product_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.product_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel = !this.sel
      this.checkProduct = []
      console.log('unchaked', b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai', id);


      this.checkProduct.push(id)
      console.log('data', this.checkProduct)
    }
    else {
      this.checkProduct = deleteintarray(this.checkProduct, id)
      console.log('deletearray :', this.checkProduct);

    }
    console.log('check thi list ', this.checkProduct)
    console.log('length ', length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel = !this.intsel && this.checkProduct.length != 0
    console.log('intsel : ' + this.intsel + ' && allsel:', this.allsel)
  }








  // sendIDS(a, id) { 
  //   if (a == true) {
  //     console.log('a',a);
  //     console.log('id',id);
  //     this.checkProduct.push(id)
  //   }
  //   else {
  //     const index = this.checkProduct.indexOf(id);
  //     if (index !== -1) {
  //       this.checkProduct.splice(index, 1);
  //     }
  //   }
  //   //this.shareData(this.checkProduct);
  // } 

  shareData(data) {
    this._DatacallsService.sendAnything(data) // to send something
  }

  delete() {
    console.log('inside delete')
    var data = {
      "delete_status": 1,
      "product_id": this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one product.', '', {
        duration: 4000

      });
    }
    else{
      this._DatacallsService.DELETEProductMaster(data).subscribe(posts => {
        console.log('checkproduct', this.checkProduct);
        this.Router.navigate(['deleted-product']);
      });
  
    }
   

  }
  onchange(a, id) {

    console.log(a)
    console.log(id)

    var data = {
      "product_id": id
    }

    this._DatacallsService.view_no_image(data).subscribe(post => {
    
      this.posts5 = post.result;
  
      console.log('posts5',this.posts5);

      for(var i=0;i< this.posts5.length;i++){
  
  if(this.posts5[i].lengths==null || this.posts5[i].lengths== 0){
  
    this.product_ids.push(this.posts5[i].product_id)
  
    this.arrays.push(this.posts5[i].product_name)

  }
      }  
      
      console.log('length',this.product_ids.length);
  
      // console.log('array',JSON.stringify(this.array).replace(/\[/g, "").replace(/\]/g, ""));
   
  
      if(this.product_ids.length==0){
  

        console.log('length555',this.product_ids.length);


        if (a == true && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer' || this.kiran[0].usertype == 'Admin') && this.kiran[0].allow_user == 1) {
          console.log('user type', this.kiran[0].usertype)
          this.jsonPara3 = { "online_status": 1, "product_id": id }
          this._DatacallsService.change_status(this.jsonPara3).subscribe(posts => {
            this.posts = posts.result
            this.snackBar.open('you allow this product to sale on E-commerce portal.', '', {
              duration: 4000
    
            });
          });
        }
        else {
          console.log('direct ithe yetoy')
          if (a == false && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer' || this.kiran[0].usertype == 'Admin') && this.kiran[0].allow_user == 1) {
            this.jsonPara4 = { "online_status": 0, "product_id": id }
            this._DatacallsService.change_status(this.jsonPara4).subscribe(posts => {
              this.posts = posts.result
              this.snackBar.open('you are not allowing this products to sale on E-commerce portal.', '', {
                duration: 4000
    
              });
            });
          }
          else {
            this.snackBar.open('you are not allow to sale products on E-commerce portal.', '', {
              duration: 4000
    
            });
          }
    
        }
  
      }
      else{ 
  
        var names = JSON.stringify(this.arrays).replace(/\[/g, "").replace(/\]/g, "")
  
      // this.snackBar.open(names+",Thease Products images are not available please put images,without images we can not approve","OK");
      
      var  message = names+",Products images are not available please put images,without images we can not put this product online"; 
        status = 'success';  
   this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'bottom', duration: 5000 });
  
      console.log('names',names);
  
     this.arrays=[];
      }
  
      });

  }

  change_all_product_status() {


    var data = {
      "product_id": this.checkProduct
    }

    this._DatacallsService.view_no_image(data).subscribe(post => {
    
      this.posts5 = post.result;
  
      console.log('posts5',this.posts5);

      for(var i=0;i< this.posts5.length;i++){
  
  if(this.posts5[i].lengths==null || this.posts5[i].lengths== 0){
  
    this.product_ids.push(this.posts5[i].product_id)
  
    this.arrays.push(this.posts5[i].product_name)

  }
      }  
      
      console.log('length',this.product_ids.length);
  
      // console.log('array',JSON.stringify(this.array).replace(/\[/g, "").replace(/\]/g, ""));
   
  
      if(this.product_ids.length==0){
  


console.log('checkproduct159753', this.checkProduct.length);
    if (this.checkProduct.length!=0 && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer' || this.kiran[0].usertype == 'Admin') && this.kiran[0].allow_user == 1) {
      console.log('user type', this.kiran[0].usertype)
      var data = {
        "product_id": this.checkProduct,
        "online_status":1
      }
      this._DatacallsService.change_all_status(data).subscribe(posts => {
        this.posts = posts.result
        console.log('checkproduct', this.checkProduct);
        this.snackBar.open('you allow all produts to sale on E-commerce portal.', '', {
          duration: 4000
        });
        setTimeout(function(){
          location.reload();
        },2000);
      });
    }
    else if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one product.', '', {
        duration: 4000

      });
    }
     else {
        this.snackBar.open('you are not allow to sale products on E-commerce portal.', '', {
          duration: 4000

        });
      }

    }

    else{ 
  
      var names = JSON.stringify(this.arrays).replace(/\[/g, "").replace(/\]/g, "")

    // this.snackBar.open(names+",Thease Products images are not available please put images,without images we can not approve","OK");
    
    var  message = names+",Products images are not available please put images,without images we can not put this product online"; 
      status = 'success';  
 this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'bottom', duration: 5000 });

    console.log('names',names);

   this.arrays=[];
    }

  });

  }


  not_all_product_status() {
    console.log('checkproduct159753', this.checkProduct.length);
        if (this.checkProduct.length!=0 && (this.kiran[0].usertype == 'Distributor' || this.kiran[0].usertype == 'Manufacturer' || this.kiran[0].usertype == 'Admin') && this.kiran[0].allow_user == 1) {
          console.log('user type', this.kiran[0].usertype)
          var data = {
            "product_id": this.checkProduct,
            "online_status":0
          }
          this._DatacallsService.change_all_status(data).subscribe(posts => {
            this.posts = posts.result
            console.log('checkproduct', this.checkProduct);
            this.snackBar.open('you are not allowing these produts to sale on E-commerce portal.', '', {
              duration: 4000
            });
            setTimeout(function(){
              location.reload();
            },2000);
          });
        }
        else if(this.checkProduct.length==0){
          this.snackBar.open('please select atleast one product.', '', {
            duration: 4000
    
          });
        }
         else {
            this.snackBar.open('you are not allow to offline the products of E-commerce portal.', '', {
              duration: 4000
    
            });
          }
    
        
      }
    

  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        type: [''],
        brand_id: [''],
        category_id: [''],
        sub_category_id: ['']
      })
    }
  }

  
  onSubmit() {
    this.type = this.searchForm.value.type == '' ? null : this.searchForm.value.type;
    console.log('usertype', this.type);
    console.log('usertype1', this.searchForm.value.type)
    this.brand_id = this.searchForm.value.brand_id == '' ? null : this.searchForm.value.brand_id;
    this.category_id = this.searchForm.value.category_id == '' ? null : this.searchForm.value.category_id;
    this.sub_category_id = this.searchForm.value.sub_category_id == '' ? null : this.searchForm.value.sub_category_id
     if(this.usertype.user == 'Admin'){
      this.json = {
        "product_id": null,
        "user_id": null,
        "usertype": this.type,
        "brand_id": this.brand_id,
        "category_id": this.category_id,
        "sub_category_id": this.sub_category_id,
        "show_products_users": null,
        "delete_status": 0,
        "approve":0
      }
     }
     else{
      this.json = {
        "product_id": null,
        "user_id": this.usertype.user_id,
        "usertype": this.type,
        "brand_id": this.brand_id,
        "category_id": this.category_id,
        "sub_category_id": this.sub_category_id,
        "show_products_users": null,
        "delete_status": 0,
        "approve":0
      }
     }
  
    this._DatacallsService.POST_ProductMaster(this.json).subscribe(posts => {

      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }
  
  file: File;
  filesToUpload: Array<File>;


  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }


  UploadExcel(){
    debugger
    console.log(this.filesToUpload)
        var formData: any = new FormData();
      // formData.append("app_id",15); 
   
    this.makeFileRequest("http://13.236.78.106/api/uploadacmeexcel_new",formData, this.filesToUpload).then((result) => {
         console.log(result);
          if(result['Success']==true){
             this.snackBar.open("Excel Uploaded Successfully", "Done");
             this.ngOnInit();
      // location.reload();
           }
           else if(result['Success']==false){
            this.snackBar.open(result['Message'], "Done");
           }
           else {   
         
           }
       }, (error) => {
         console.error(error);
       });
   
   }

makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


  downloadexcel(){
  
    if(this.usertype.user=='Admin'){
      this._DatacallsService.GETProductexcel(null).subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="productExcelDownload.xlsx";
                    link.click();
                    
      });
    }
    else{
      this._DatacallsService.GETProductexcel(this.usertype.user_id).subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="productExcelDownload.xlsx";
                    link.click();
                    
      });
    }
  

}


leadStatus(country,product_id) {
    
  
  this.update_country = country;
  this.update_id = product_id;

  var form = new FormData();

  form.append("product_id", this.update_id);
  form.append("country", this.update_country);

  this._DatacallsService.change_country(form).subscribe(posts => {
    if (this.update_country == 'ALL') {
      this.config.duration = 3000
      this.snackBar.open("Country Changed To ALL", "Done", this.config);
    }
    if (this.update_country == 'AU') {
      this.config.duration = 3000
      this.snackBar.open("Country Changed To AU", "Done", this.config);
    }
    if (this.update_country =='NZ') {
      this.config.duration = 3000
      this.snackBar.open("Country Changed To NZ", "Done", this.config);
    }
  });
  this.ngOnInit();
}

getCountry(country_id) {

  this.country_id = country_id;

  console.log('inside getModel',country_id);

}

submit_countries(){

  console.log('checkProduct',this.checkProduct)
  var a=[] 
  for(var k=0;k<this.country_id.length;k++){

    for(var i=0;i<this.checkProduct.length;i++){
    
      a.push({"country_id":this.country_id[k],"product_id":this.checkProduct[i]})
     
    }
  }
  var form ={
    "country_config":a
  }
  console.log('country_config',form)

  this._DatacallsService.POSTCountryMapping(form).subscribe(posts => {
   
    location.reload();
    
  });

}

changecountry(country){

  if(this.checkProduct.length==0){
    this.snackBar.open('please select atleast one product.', '', {
      duration: 4000

    });
  }
  else{
    console.log('inside changecountry',country)
    this.update_country = country; 
   
    var form = {
      "product_id":this.checkProduct,
      "country":this.update_country
    }
    console.log('inside product_id',this.checkProduct)
    console.log('inside country',country)
  
    // form.append("product_id",this.checkProduct);
    // form.append("country", this.update_country);
  
    this._DatacallsService.change_country(form).subscribe(posts => {
      if (this.update_country == 'ALL') {
        this.config.duration = 3000
        this.snackBar.open("Country Changed To ALL", "Done", this.config);
      }
      if (this.update_country == 'AU') {
        this.config.duration = 3000
        this.snackBar.open("Country Changed To AU", "Done", this.config);
      }
      if (this.update_country =='NZ') {
        this.config.duration = 3000
        this.snackBar.open("Country Changed To NZ", "Done", this.config);
      }
    });
    setTimeout(() => {
      location.reload();
    }, 1500);
  }

 
}


}


