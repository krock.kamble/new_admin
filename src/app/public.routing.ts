import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardGuard } from './authguard.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddvehiclesComponent } from './addvehicles/addvehicles.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { MFDashboardComponent } from './mf-dashboard/mf-dashboard.component';
import { AddproductsComponent } from './addproducts/addproducts.component';
import { CategoriesComponent } from './categories/categories.component';
import { SubcategoriesComponent } from './subcategories/subcategories.component';
import { MakeComponent } from './make/make.component';
import { ModelComponent } from './model/model.component';
import { MapproductsComponent } from './mapproducts/mapproducts.component';
import { MappedlistComponent } from './mappedlist/mappedlist.component';
import { BrandsComponent } from './brands/brands.component';
import { SubmodelComponent } from './submodel/submodel.component';
import {DistributerlistComponent} from './distributerlist/distributerlist.component';
import {OrderComponent} from './order/order.component';
import {MapdistributorComponent} from './mapdistributor/mapdistributor.component';
import{MappeddistributorComponent} from './mappeddistributor/mappeddistributor.component';
import{ApprovedproductComponent} from './approvedproduct/approvedproduct.component';
import{MapusersComponent} from './mapusers/mapusers.component';
import{MappedusersComponent} from './mappedusers/mappedusers.component';
import{AllproductsComponent} from './allproducts/allproducts.component';
import{DeletedBrandComponent} from './deleted-brand/deleted-brand.component';
import {DeletedCategoryComponent} from './deleted-category/deleted-category.component';
import{DeletedSubcategoryComponent} from './deleted-subcategory/deleted-subcategory.component';
import {DeletedMakeComponent} from './deleted-make/deleted-make.component';
import {DeletedModelComponent} from './deleted-model/deleted-model.component';
import {DeletedSubmodelComponent} from './deleted-submodel/deleted-submodel.component';
import {DeletedProductComponent} from './deleted-product/deleted-product.component';
import{DeletedUserComponent} from './deleted-user/deleted-user.component';
import {DeletedCarComponent} from'./deleted-car/deleted-car.component';
import {OnlineProductsComponent} from './online-products/online-products.component';
import {DDashboardComponent} from './d-dashboard/d-dashboard.component';
import{ExcelComponent} from './excel/excel.component';
import{ApproveProductComponent} from './approve-product/approve-product.component';
import{WeightFreightComponent} from './weight-freight/weight-freight.component';
import{CubicFreightComponent} from './cubic-freight/cubic-freight.component';
import {CountryComponent} from './country/country.component';
import {DeletedCountryComponent} from './deleted-country/deleted-country.component';
import{ReviewComponent} from './review/review.component';
import {MessagesComponent} from './messages/messages.component';
import {MakeRequestComponent} from './make-request/make-request.component';
import {AllRequestComponent} from './all-request/all-request.component';
import {MapComponent} from './map/map.component';
import{ProductComponent} from './product/product.component';

export const PUBLIC_ROUTES: Routes = [
    { path: 'dashboard', component: DashboardComponent,canActivate: [AuthguardGuard]},
    { path:'product',component:ProductComponent,canActivate:[AuthguardGuard]},
    { path: 'vehicles', component: VehiclesComponent,canActivate: [AuthguardGuard]  },
    { path: 'addvehicles', component: AddvehiclesComponent,canActivate: [AuthguardGuard] },
    { path: 'editvehicle/:id', component: AddvehiclesComponent,canActivate: [AuthguardGuard] },
    { path: 'users', component: UsersComponent,canActivate: [AuthguardGuard]  },
    { path: 'products', component: ProductsComponent ,canActivate: [AuthguardGuard] },
    { path: 'addproducts', component: AddproductsComponent,canActivate: [AuthguardGuard] },
    { path: 'editproduct/:id', component: AddproductsComponent,canActivate: [AuthguardGuard] },
    { path: 'mapproducts', component: MapproductsComponent,canActivate: [AuthguardGuard] },
    { path: 'mappedlist', component: MappedlistComponent,canActivate: [AuthguardGuard] },
    { path: 'mf-dashboard', component: MFDashboardComponent,canActivate: [AuthguardGuard] },
    { path: 'categories', component: CategoriesComponent ,canActivate: [AuthguardGuard]},
    { path: 'subcategories', component: SubcategoriesComponent,canActivate: [AuthguardGuard] },
    { path: 'make', component: MakeComponent,canActivate: [AuthguardGuard] },
    { path: 'model', component: ModelComponent,canActivate: [AuthguardGuard] },
    { path: 'brands', component: BrandsComponent,canActivate: [AuthguardGuard] },
    { path: 'submodel', component: SubmodelComponent,canActivate: [AuthguardGuard]},
    {path: 'distributerlist',component:DistributerlistComponent,canActivate: [AuthguardGuard]},
    {path:'order',component:OrderComponent,canActivate: [AuthguardGuard]},
    {path: 'mapdistributor',component:MapdistributorComponent,canActivate: [AuthguardGuard]},
    {path:'mappeddistributor',component:MappeddistributorComponent,canActivate: [AuthguardGuard]},
    {path: 'approvedproduct',component:ApprovedproductComponent,canActivate: [AuthguardGuard]},
    {path: 'mapusers',component:MapusersComponent,canActivate: [AuthguardGuard]},
    {path:'mappedusers',component:MappedusersComponent,canActivate: [AuthguardGuard]},
    {path:'allproducts',component:AllproductsComponent,canActivate: [AuthguardGuard]},
    {path:'deleted-brand',component:DeletedBrandComponent,canActivate: [AuthguardGuard]},
    {path: 'deleted-category',component:DeletedCategoryComponent,canActivate: [AuthguardGuard]},
    {path : 'deleted-subcategory',component:DeletedSubcategoryComponent,canActivate: [AuthguardGuard]},
    {path: 'deleted-make',component:DeletedMakeComponent,canActivate: [AuthguardGuard]},
    {path: 'deleted-model',component:DeletedModelComponent,canActivate: [AuthguardGuard]},
    {path: 'deleted-submodel',component:DeletedSubmodelComponent,canActivate: [AuthguardGuard]},
    {path: 'deleted-product',component:DeletedProductComponent,canActivate: [AuthguardGuard]},
    {path:'deleted-user',component:DeletedUserComponent,canActivate: [AuthguardGuard]},
    {path:'deleted-car',component:DeletedCarComponent,canActivate: [AuthguardGuard]},
    {path: 'online-products',component:OnlineProductsComponent,canActivate: [AuthguardGuard]},
    {path:'d-dashboard',component:DDashboardComponent,canActivate: [AuthguardGuard]},
    {path:'excel',component:ExcelComponent,canActivate: [AuthguardGuard]},
    {path:'approve-product',component:ApproveProductComponent,canActivate: [AuthguardGuard]},
    {path:'weight-freight',component:WeightFreightComponent,canActivate: [AuthguardGuard]},
    {path:'cubic-freight',component:CubicFreightComponent,canActivate: [AuthguardGuard]},
    {path:'country',component:CountryComponent,canActivate: [AuthguardGuard]},
    {path:'deleted-country',component:DeletedCountryComponent,canActivate: [AuthguardGuard]},
    {path:'review',component:ReviewComponent,canActivate: [AuthguardGuard]},
    {path :'messages',component:MessagesComponent,canActivate: [AuthguardGuard]},
    {path :'make-request',component:MakeRequestComponent,canActivate:[AuthguardGuard]},
    {path:'all-request',component:AllRequestComponent,canActivate:[AuthguardGuard]},
    {path:'map/:id',component:MapComponent,canActivate:[AuthguardGuard]}


];