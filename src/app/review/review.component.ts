import { Component, OnInit, NgModule,ElementRef,ViewChild, Inject } from '@angular/core';
import {DatacallsService} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router'
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { deleteobj, deleteintarray } from '../deletearray'


@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  number=[1,2,3,4,5]
  jsonPara3={};
  jsonPara4={};
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches) 
    );

    displayedColumns = ['id', 'user_name', 'product_name','product_image','review','rating','currentdate','currenttime','status','select'];
    dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
 
  posts;
  deleteid;
  usertype;
  jsonPara2={};
  data={};
  checkProduct=[];
  filteredData;
  sel = false;
  allsel = false;
  intsel = false;
  constructor(private _DatacallsService:DatacallsService,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute,private breakpointObserver: BreakpointObserver, private fb:FormBuilder,private Router:Router,public dialog: MatDialog,public snackBar: MatSnackBar) { 

  }

  ngOnInit() { 

    console.log('handset',this.isHandset$);
    

    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'));

    var user = this.usertype.user;

    var form ={
      "user_id":null,
      "product_id":null,
      "online_status":null
    } 
  

    this._DatacallsService.view_reviews(form).subscribe(posts=>{
      console.log(posts);
       
      this.posts = posts.result;

      console.log('result',this.posts)
      this.dataSource = new MatTableDataSource(this.posts);
       
      this.dataSource.paginator=this.paginator
      this.dataSource.sort=this.sort
     });
     
  }


  onchange(a, id) {

    console.log(a)
    console.log(id)

    if (a == true) {

      this.jsonPara3 = { "online_status": 1, "review_id": id }
      this._DatacallsService.change_review_status(this.jsonPara3).subscribe(posts => {
        this.posts = posts.result
        this.snackBar.open('you allow this review to show on E-commerce portal.', '', {
          duration: 4000

        });
      });
    }
      else {
        this.jsonPara3 = { "online_status": 0, "review_id": id }
        this._DatacallsService.change_review_status(this.jsonPara3).subscribe(posts => {
          this.posts = posts.result
          this.snackBar.open('you are not allow to show review on E-commerce portal.', '', {
            duration: 4000
  
          });
        });
       
      }

    
  }


  
  alltogglecheck(b, array) {
    console.log('aaaaaaaaaa',this.filteredData )
  
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.review_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.review_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel = !this.sel
      this.checkProduct = []
      console.log('unchaked', b)
    }
  }

  sendIDS(a, id, length) {
    if (a == true) {
      console.log('id araha hai', id);


      this.checkProduct.push(id)
      console.log('data', this.checkProduct)
    }
    else {
      this.checkProduct = deleteintarray(this.checkProduct, id)
      console.log('deletearray :', this.checkProduct);

    }
    console.log('check thi list ', this.checkProduct)
    console.log('length ', length)
    this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
    this.allsel = !this.intsel && this.checkProduct.length != 0
    console.log('intsel : ' + this.intsel + ' && allsel:', this.allsel)
  }

  delete() {
    console.log(this.checkProduct,'inside delete')
    var data = {
      "review_id": this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one review.', '', {
        duration: 4000

      });
    }
    else{
      this._DatacallsService.delete_reviews(data).subscribe(posts => {
        console.log('checkproduct', this.checkProduct);
        this.snackBar.open('review deleted sucessfully.', '', {
          duration: 4000
  
        });
        this.ngOnInit();
      });
  
    }
   

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    this.filteredData = this.dataSource.filteredData
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}

export interface UserData {
  id: string;
  product_name: string;
  product_season: string;
  quantity: string;
  product_image:string
 
}

