import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';

export const SECURE_ROUTES: Routes = [
    { path: 'login', component: LoginComponent }
];