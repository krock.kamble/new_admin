import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import {DatacallsService} from '../datacalls.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  posts;
  stripe;
  usertype;
  account;
  type;
  country;
  sidebarMenu = [];
  sidebarMenu2 = [];
  nestedsidebarMenu = [];
  nestedsidebarMenu1 = [];
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, private activatedRoute: ActivatedRoute,private _DatacallsService:DatacallsService ) { }

  ngOnInit() {

    
    this.usertype = JSON.parse(sessionStorage.getItem('currentuser'))

    this._DatacallsService.getAccount_id().subscribe(posts=>{
      this.posts = posts;
      this.usertype;
      console.log('user-->>',this.usertype.email)
      // console.log('posts',this.posts.Transfer.data[0].email);

      for(var i=0;i< this.posts.Transfer.data.length;i++){

        // console.log('api',this.posts.Transfer.data[i].email,'local',this.usertype.email)
    
              if(this.usertype.email == this.posts.Transfer.data[i].email ){

                this.account = this.posts.Transfer.data[i].id
                this.country = this.posts.Transfer.data[i].country
                this.type = this.posts.Transfer.data[i].legal_entity.type
        this.stripe =1;

      }

  
      }
if(this.stripe ==1){
var form ={
  "account_no":this.account,
  "country_code":this.country,
  "account_type":this.type,
  "id":this.usertype.user_id
}
console.log('account',this.account);
      console.log('country',this.country);
      console.log('type',this.type);
      
  this._DatacallsService.update_users_stripe(form).subscribe(posts=>{

    this.posts = posts;
  })
  
  

}
      
      
      
      console.log('stripe1',this.stripe);

     });


    if (this.usertype.user == 'Admin') {
      this.sidebarMenu = [
        {
          name: "DASHBOARD",
          path: "/dashboard",
          icon: "dashboard",
          class: ""
        },
        // {
        //   name: "USERS",
        //   path: "/users",
        //   icon: "dashboard",
        //   class: ""
        // },
        {
          name: "PRODUCTS",
          path: "/product",
          icon: "people",
          class: ""
        },
        {
              name: "CATEGORY",
              path: "/categories",
              icon: "drive_eta",
              class: ""
            },
        // {
        //   name: "COUNTRIES",
        //   path: "/country",
        //   icon: "dashboard",
        //   class: ""
        // },
        // {
        //   name: "VEHICLES",
        //   path: "/vehicles",
        //   icon: "people",
        //   class: ""
        // },
        // {
        //   name: "APR-PRODUCTS",
        //   path: "/approve-product",
        //   icon: "people",
        //   class: ""
        // },
        // {
        //   name: "PRODUCTS",
        //   path: "/products",
        //   icon: "people",
        //   class: ""
        // },
        // {
        //   name: "MESSAGES",
        //   path: "/messages",
        //   icon: "people",
        //   class: ""
        // }, 
        // { 
        //   name: "EXCEL",
        //   path: "/excel",
        //   icon: "dashboard",
        //   class: ""
        // },
        // {
        //   name: "MAPPED LIST",
        //   path: "/mappedlist",
        //   icon: "people",
        //   class: ""
        // },
        // {
        //   name: "MAPPED USER",
        //   path: "/mappedusers",
        //   icon: "people",
        //   class: ""
        // },
        // {
        //   name: "ORDER",
        //   path: "/order",
        //   icon: "people",
        //   class: ""
        // },
        // {
        //   name: "REVIEW",
        //   path: "/review",
        //   icon: "people",
        //   class: ""
        // }
       
       
      ]

      // this.nestedsidebarMenu = [
      //   {
      //     name: "BRANDS",
      //     path: "/brands",
      //     icon: "drive_eta",
      //     class: ""
      //   },
      //   {
      //     name: "CATEGORY",
      //     path: "/categories",
      //     icon: "drive_eta",
      //     class: ""
      //   },
      //   {
      //     name: "SUB CATEGORY",
      //     path: "/subcategories",
      //     icon: "drive_eta",
      //     class: ""
      //   },
      //   {
      //     name: "MAKE",
      //     path: "/make",
      //     icon: "drive_eta",
      //     class: ""
      //   },
      //   {
      //     name: "MODEL",
      //     path: "/model",
      //     icon: "drive_eta",
      //     class: ""
      //   },
      //   {
      //     name: "SUB MODEL",
      //     path: "/submodel",
      //     icon: "drive_eta",
      //     class: ""
      //   },
      // ]

      // this.nestedsidebarMenu1 = [
      //   {
      //     name: "WEIGHT",
      //     path: "/weight-freight",
      //     icon: "drive_eta",
      //     class: ""
      //   },
      //   {
      //     name: "CUBIC",
      //     path: "/cubic-freight",
      //     icon: "drive_eta",
      //     class: ""
      //   }
      // ]

       
      
    }

    // if (this.usertype.user == 'Manufacturer') {
    //   this.sidebarMenu = [
    //     {
    //       name: "DASHBOARD",
    //       path: "/mf-dashboard",
    //       icon: "dashboard",
    //       class: ""
    //     },
    //     {
    //       name: "ALL REQUEST",
    //       path: "/all-request",
    //       icon: "people",
    //       class: ""
    //     },
    //     {
    //       name: "MY PRODUCTS",
    //       path: "/products",
    //       icon: "people",
    //       class: ""
    //     },
    //     {
    //       name: "EXCEL",
    //       path: "/excel",
    //       icon: "dashboard",
    //       class: ""
    //     },
    //     {
    //       name: "DISTRIBUTER LIST",
    //       path: "/distributerlist",
    //       icon: "people",
    //       class: ""
    //     },
    //     {
    //       name: "MAPPED DISTRI..",
    //       path: "/mappeddistributor",
    //       icon: "people",
    //       class: ""
    //     },
    //     {
    //       name: "MAPPED PROD..",
    //       path: "/mappedlist",
    //       icon: "people",
    //       class: ""
    //     }
    //     // {
    //     //   name: "ORDER",
    //     //   path: "/order",
    //     //   icon: "people",
    //     //   class: ""
    //     // }
    //   ]
    //   this.nestedsidebarMenu1 = [
    //     {
    //       name: "WEIGHT",
    //       path: "/weight-freight",
    //       icon: "drive_eta",
    //       class: ""
    //     },
    //     {
    //       name: "CUBIC",
    //       path: "/cubic-freight",
    //       icon: "drive_eta",
    //       class: ""
    //     }
    //   ]
    // }
    // if (this.usertype.user == 'Distributor') {
    //   this.sidebarMenu = [
    //     {
    //       name: "DASHBOARD",
    //       path: "/d-dashboard",
    //       icon: "dashboard",
    //       class: ""
    //     },
    //     {
    //       name: "MAKE REQUEST",
    //       path: "/make-request",
    //       icon: "people",
    //       class: ""
    //     },
    //     {
    //       name: "MY PRODUCTS",
    //       path: "/products",
    //       icon: "people",
    //       class: ""
    //     },
    //     {
    //       name: "EXCEL",
    //       path: "/excel",
    //       icon: "dashboard",
    //       class: ""
    //     },
    //     {
    //       name: "MAPPED PROD..",
    //       path: "/mappedlist",
    //       icon: "people",
    //       class: ""
    //     },
    //     {
    //       name: "APPROVED PROD..",
    //       path: "/approvedproduct",
    //       icon: "people",
    //       class: ""
    //     }
    //     // {
    //     //   name: "ORDER",
    //     //   path: "/order",
    //     //   icon: "people",
    //     //   class: ""
    //     // }
    //   ]
    //   this.nestedsidebarMenu1 = [
    //     { 
    //       name: "WEIGHT",
    //       path: "/weight-freight",
    //       icon: "drive_eta",
    //       class: ""
    //     },
    //     {
    //       name: "CUBIC",
    //       path: "/cubic-freight",
    //       icon: "drive_eta",
    //       class: "" 
    //     }
    //   ]
    // }

   
  }

  logout() {
    sessionStorage.removeItem("currentuser");
    this.router.navigate(['login']);
  }
  stripe_click(){
    window.open("https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_E1TqZaVaMYWeUQ4m7hNotxgpjnSaJcQx&scope=read_write","Stripe", "width=500,height=500");
  }

}
