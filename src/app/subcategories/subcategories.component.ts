import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';
@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.component.html',
  styleUrls: ['./subcategories.component.css'],
  providers: [DatacallsService]
})
export class SubcategoriesComponent implements OnInit {

  posts;
  sub_category_id;
  sel=false;
  allsel=false;
  intsel=false;
  categories;
  checkProduct=[];
  dataSource;
  data=[];
  showLogo;
  uploadImg;
  imagepath1;
  filteredData;
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.categoryForm = this._formBuilder.group({
      sub_category_id: [0],
      category_id: ['', Validators.required],
      sub_category_name: ['', Validators.required],
      addresses: this._formBuilder.array([
        this.initAddress(),
      ]),
      dropdown: this._formBuilder.array([
        this.initDropdown(),
      ]),
    });
  }

  ngOnInit() {
    console.log('test', this.categoryForm)
    this._DatacallsService.GETCategoryMaster(null,0,null).subscribe(posts => {
      this.categories = posts.result;
    });

    this._DatacallsService.GETSubCategoryMaster(null, null,0).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    //this.removeAddress(0);
  }

  alltogglecheck(b,array){
    // if(b==true){
    //   this.checkProduct=[];   
    //   array.forEach(element => {this.checkProduct.push(element.sub_category_id)})
     
    //   this.sel=!this.sel
    //   console.log('all_id',this.checkProduct);
    //   console.log('chacked',b);
      
  
    // }  
    if (b == true) {
      this.checkProduct = [];

      if(this.filteredData == undefined){
       
        console.log("outside this.filteredData != ''")
        array.forEach(element => { this.checkProduct.push(element.sub_category_id) })
      }else{
        console.log("inside this.filteredData",this.filteredData)
        this.checkProduct =this.filteredData.map(x =>  x.sub_category_id)
        console.log('this.checkProduct',this.checkProduct)

        
      }
      

      this.sel = !this.sel
      console.log('all_id', this.checkProduct);
      console.log('chacked', b);


    }
    else {
      this.sel=!this.sel
      this.checkProduct=[]
      console.log('unchaked',b)
    }
  }


  
  uploadImage(fileInput: any) {
    this.uploadImg = fileInput.target.files['0'];

    this.data.push(fileInput.target.files['0'])
    console.log('data',this.data);
    console.log('files', fileInput.target.files);
    console.log('logo', this.showLogo)
  }

  delete(){
    var data={
      "delete_status":1,
      "sub_category_id":this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one sub-category.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.DELETESubCategoryMaster(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.Router.navigate(['deleted-subcategory']);
      });
    }
      
    }
    sendIDS(a, id, length) {
      if (a == true) {
        console.log('id araha hai',id);
        // this.json={
        //   "id":id
        // }
  
        this.checkProduct.push(id)
        console.log('data',this.checkProduct)
      }
      else {
        this.checkProduct =deleteintarray(this.checkProduct,id)
        console.log('deletearray :',this.checkProduct);
      //   console.log(this.checkProduct[0]["id"])
      // for(var i=0;i<this.checkProduct.length;i++){
      //   if(this.checkProduct[i].id==id){
      //     this.checkProduct.splice(i, 1);
      //   }
      // }
  
  
  //--> [{id:3},{id:7}]
  //-->[3,7]
  //-->"3,7"   
      }
      console.log('check thi list ',this.checkProduct)
      console.log('length ',length)
      this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
      this.allsel=!this.intsel && this.checkProduct.length !=0
      console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
      //this.shareData(this.checkProduct);
    }
    // sendIDS(a, id) {
    //   if (a == true) {
    //     this.checkProduct.push(id)
    //     console.log('a',a);
    //     console.log('id',id);
    //   }
    //   else {
    //     const index = this.checkProduct.indexOf(id);
    //     if (index !== -1) {
    //       this.checkProduct.splice(index, 1);
    //     }
    //   }
    //   //this.shareData(this.checkProduct);
    // }
  initAddress() {
    return this._formBuilder.group({
      input: ['']
    });
  }

  addAddress() {
    const control = <FormArray>this.categoryForm.controls['addresses'];
    control.push(this.initAddress());
  }

  removeAddress(i: number) {
    const control = <FormArray>this.categoryForm.controls['addresses'];
    control.removeAt(i);
    //this.waypoints_details.splice(i, 1);
  }

  initDropdown() {
    return this._formBuilder.group({
      dropdown: [''],
      dropdownList: this._formBuilder.array([
        this.initOption(),
      ])
    });
  }

  addDropdown() {
    const control = <FormArray>this.categoryForm.controls['dropdown'];
    control.push(this.initDropdown());
  }

  removeDropdown(i: number) {
    const control = <FormArray>this.categoryForm.controls['dropdown'];
    control.removeAt(i);
  }

  initOption() {
    return this._formBuilder.group({
      option: ['']
    });
  }

  addOption(i) {
    const control = (<FormArray>this.categoryForm.controls['dropdown']).at(i).get('dropdownList') as FormArray;
    control.push(this.initOption());
  }

  removeOption(i: number) {
    const control = (<FormArray>this.categoryForm.controls['dropdown']).at(i).get('dropdownList') as FormArray;
    control.removeAt(i);
  }

  resetform(file) {
    this.categoryForm = this._formBuilder.group({
      sub_category_id: [0],
      category_id: ['', Validators.required],
      sub_category_name: ['', Validators.required],
      addresses: this._formBuilder.array([
        this.initAddress(),
      ]),
      dropdown: this._formBuilder.array([
        this.initDropdown(),
      ]),
    });
    file.files = []
    this.sub_category_id = 0;
  }

  edit(id) {

    this.showLogo=[];

    console.log('id', id)

    this._DatacallsService.GETSubCategoryMaster(null, id,0).subscribe(
      data => {
        this.showLogo=[data.result[0].sub_category_image];
        this.imagepath1 = data.result[0].sub_category_image;
        this.sub_category_id = data.result[0].sub_category_id;
        this.categoryForm.patchValue({
          'category_id': data.result[0].category_id,
          'sub_category_id': data.result[0].sub_category_id,
          'sub_category_name': data.result[0].sub_category_name
        });


        const control = <FormArray>this.categoryForm.controls['addresses'];
        const control1 = <FormArray>this.categoryForm.controls['dropdown'];

        for (var i = 0; i < data.result[0].fields.inputFields.length; i++) {
          control.push(this._formBuilder.group({
            input: data.result[0].fields.inputFields[i].input
          }));
        }
        this.removeAddress(0);

        data.result[0].fields.dropdownFields.forEach(x => {
          control1.push(this._formBuilder.group({
            dropdown: x.dropdown,
            dropdownList: this.initOptionEdit(x)
          }));
        });
        this.removeDropdown(0);
        //this.removeOption(0);
        console.log('control', control1)

      }
    );
  }

  initOptionEdit(x) {
    let arr = new FormArray([])
    x.dropdownList.forEach(y => {
      arr.push(this._formBuilder.group({
        option: y.option
      }))
    });
    return arr;
  }

  submit(file) {

    // var files1 = this.data[0]
    
    var fields;
    fields = ({
      'inputFields': this.categoryForm.value.addresses,
      'dropdownFields': this.categoryForm.value.dropdown[0].dropdown == '' ? null : this.categoryForm.value.dropdown
    });

    console.log('this.categoryForm.value.dropdown', this.categoryForm.value.dropdown[0].dropdown)
    // for(var i = 0; i<this.categoryForm.value.addresses.length; i++)
    var bc = this.categoryForm.value.sub_category_id;

    var form: any = new FormData();
    form.append("sub_category_id", bc)
    form.append("category_id", this.categoryForm.value.category_id)
    form.append("sub_category_name", this.categoryForm.value.sub_category_name)
    form.append("fields", JSON.stringify(fields))
    // form.append("file1",files1);
    form.append("file1",this.uploadImg);
    form.append("imagepath1", this.imagepath1);

    this._DatacallsService.POSTSubCategoryMaster(form).subscribe(posts => {
      if (posts.result) {
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Sub-Category Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Sub-Category Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform(file);
  }

}

