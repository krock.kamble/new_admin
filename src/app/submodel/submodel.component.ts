
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';
@Component({
  selector: 'app-submodel',
  templateUrl: './submodel.component.html',
  styleUrls: ['./submodel.component.css']
})
export class SubmodelComponent implements OnInit {

  posts;
  model_id;
  dataSource;
  sel=false;
  allsel=false;
  intsel=false;
  makes;
  models;
  variants;
  filteredData;
  checkProduct=[];
  years;
  year={};
  config = new MatSnackBarConfig();
  categoryForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    this.filteredData = this.dataSource.filteredData
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute) {
    this.categoryForm = this._formBuilder.group({
      sub_model_id: [0],
      make_id: ['', Validators.required],
      model_id: ['', Validators.required],
      variant_id: ['', Validators.required],
      sub_model_name: ['', Validators.required],
      addresses: this._formBuilder.array([
        this.initAddress(),
      ]),
    });
  }

  ngOnInit() {

    this._DatacallsService.GETMakeMaster(null,0).subscribe(posts => {
      this.makes = posts.result;
    });

    this._DatacallsService.GETSubModelMaster(null, null, null, null,0).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }
  delete(){
    var data={
      "delete_status":1,
      "sub_model_id":this.checkProduct
    }

    if(this.checkProduct.length==0){
      this.snackBar.open('please select atleast one sub-model.', '', {
        duration: 4000
    
      });  
    }
    else{
      this._DatacallsService.DELETESubmodelMaster(data).subscribe(posts => {
    console.log('checkproduct',this.checkProduct);
    this.Router.navigate(['deleted-submodel']);
      });
    
    } 
    }
    // sendIDS(a, id) {
    //   if (a == true) {
    //     this.checkProduct.push(id)
    //     console.log('a',a);
    //     console.log('id',id);
    //   }
    //   else {
    //     const index = this.checkProduct.indexOf(id);
    //     if (index !== -1) {
    //       this.checkProduct.splice(index, 1);
    //     }
    //   }
    //   //this.shareData(this.checkProduct);
    // }

      
  file: File;
  filesToUpload: Array<File>;


  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }


  UploadExcel(){
    debugger
    console.log(this.filesToUpload)
        var formData: any = new FormData();
      // formData.append("app_id",15); 
   
    this.makeFileRequest("http://13.236.78.106/api/uploadsubmodeleexcel",formData, this.filesToUpload).then((result) => {
         console.log(result);
          if(result['Success']==true){
             this.snackBar.open("Excel Uploaded Successfully", "Done");
             this.ngOnInit();
      // location.reload();
           }
           else if(result['Success']==false){
            this.snackBar.open(result['Message'], "Done");
           }
           else {
        
           }
       }, (error) => {
         console.error(error);
       });
   
   }

makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }



    downloadexcel(){
  
        this._DatacallsService.ExcelDownload().subscribe(blob => {
          console.log(blob);
      
           var link=document.createElement('a');
                      link.href=window.URL.createObjectURL(blob);
                      link.download="ExcelDownload.xlsx";
                      link.click();
                      
        });
      
    }
    alltogglecheck(b,array){
      // if(b==true){
      //   this.checkProduct=[];   
      //   array.forEach(element => {this.checkProduct.push(element.sub_model_id)})
       
      //   this.sel=!this.sel
      //   console.log('all_id',this.checkProduct);
      //   console.log('chacked',b);
        
    
      // }  
      if (b == true) {
        this.checkProduct = [];
  
        if(this.filteredData == undefined){
         
          console.log("outside this.filteredData != ''")
          array.forEach(element => { this.checkProduct.push(element.sub_model_id) })
        }else{
          console.log("inside this.filteredData",this.filteredData)
          this.checkProduct =this.filteredData.map(x =>  x.sub_model_id)
          console.log('this.checkProduct',this.checkProduct)
  
          
        }
        
  
        this.sel = !this.sel
        console.log('all_id', this.checkProduct);
        console.log('chacked', b);
  
  
      }
      else {
        this.sel=!this.sel
        this.checkProduct=[]
        console.log('unchaked',b)
      }
    }
    sendIDS(a, id, length) {
      if (a == true) {
        console.log('id araha hai',id);
        // this.json={
        //   "id":id
        // }
  
        this.checkProduct.push(id)
        console.log('data',this.checkProduct)
      }
      else {
        this.checkProduct =deleteintarray(this.checkProduct,id)
        console.log('deletearray :',this.checkProduct);
      //   console.log(this.checkProduct[0]["id"])
      // for(var i=0;i<this.checkProduct.length;i++){
      //   if(this.checkProduct[i].id==id){
      //     this.checkProduct.splice(i, 1);
      //   }
      // }
  
  
  //--> [{id:3},{id:7}]
  //-->[3,7]
  //-->"3,7"   
      }
      console.log('check thi list ',this.checkProduct)
      console.log('length ',length)
      this.intsel = this.checkProduct.length != 0 && this.checkProduct.length != length ? true : false;
      this.allsel=!this.intsel && this.checkProduct.length !=0
      console.log('intsel : '+this.intsel+' && allsel:',this.allsel)
      //this.shareData(this.checkProduct);
    }
  getModel(make_id) {
    this._DatacallsService.GETModelMaster(null, make_id,0).subscribe(posts => {
      this.models = posts.result;
    });
  }

  getVariant(model_id) {
    this._DatacallsService.GETModelMaster(model_id, this.categoryForm.value.make_id,0).subscribe(posts => {
      for (var i = 0; i < posts.result.length; i++) {
        this.variants = posts.result[i].variant_name;
      }
    });
  }

  resetform() {
    this.categoryForm = this._formBuilder.group({
      sub_model_id: [0],
      make_id: ['', Validators.required],
      model_id: ['', Validators.required],
      variant_id: ['', Validators.required],
      sub_model_name: ['', Validators.required],
      addresses: this._formBuilder.array([
        this.initAddress(),
      ]),
    });
    this.model_id = 0;
  }

  initAddress() {
    return this._formBuilder.group({
      year: [''],
      month:[''],
      to_year:[''],
      to_month:['']
    });
  }

  addAddress() {
    const control = <FormArray>this.categoryForm.controls['addresses'];
    control.push(this.initAddress());
  }

  removeAddress(i: number) {
    const control = <FormArray>this.categoryForm.controls['addresses'];
    control.removeAt(i);
  }

  edit(id) {
    this._DatacallsService.GETModelMaster(null, null,0).subscribe(posts => {
      this.models = posts.result;
    });

    this._DatacallsService.GETSubModelMaster(null, null, null, null,0).subscribe(posts => {
      this.variants = posts.result;
    });

    this._DatacallsService.GETSubModelMaster(id, null, null, null,0).subscribe(
      data => {
        console.log(data.result)
        this.categoryForm.patchValue({
          'sub_model_id': data.result[0].sub_model_id,
          'model_id': data.result[0].model_id,
          'make_id': data.result[0].make_id,
          'variant_id': data.result[0].variant_id,
          'sub_model_name': data.result[0].sub_model_name
        });

        const control = <FormArray>this.categoryForm.controls['addresses'];

        for (var i = 0; i < data.result[0].year_name.length; i++) {
          control.push(this._formBuilder.group({
            year: data.result[0].year_name[i].year_name,
            month: data.result[0].year_name[i].month,
            to_year:data.result[0].year_name[i].to_year,
            to_month:data.result[0].year_name[i].to_month
          }));
        }
        this.removeAddress(0);
      }
    );
  }

  submit() {
    var bc =this.categoryForm.value.sub_model_id
    var years = [];
    for (var i = 0; i < this.categoryForm.value.addresses.length; i++) {
      this.year={
        "year": this.categoryForm.value.addresses[i].year,
        "month": this.categoryForm.value.addresses[i].month,
        "to_year":this.categoryForm.value.addresses[i].to_year,
        "to_month":this.categoryForm.value.addresses[i].to_month,
      }
      years.push(this.year);
    }

    var form: any = new FormData();
    form.append("sub_model_id", bc)
    form.append("model_id", this.categoryForm.value.model_id)
    form.append("make_id", this.categoryForm.value.make_id)
    form.append("variant_id", this.categoryForm.value.variant_id)
    form.append("sub_model_name", this.categoryForm.value.sub_model_name)
    form.append("year", JSON.stringify(years))

    this._DatacallsService.POSTSubModelMaster(form).subscribe(posts => {
      if (posts.result) {
        if (posts.result[0].insert_sub_model_master == -2) {
          this.snackBar.open("Sub Model Name Already Exists", "Done", this.config);
        }
        else{
        if (bc == 0) {
          this.config.duration = 3000
          this.snackBar.open("Sub Model Added Successfully", "Done", this.config);
          this.ngOnInit();
        }
        else {
          this.config.duration = 3000
          this.snackBar.open("Sub Model Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform();
  }

}

