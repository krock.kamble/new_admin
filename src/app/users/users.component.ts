import { Component, OnInit, ViewChild, ElementRef , Inject} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {deleteobj,deleteintarray} from '../deletearray'
import { element } from '../../../node_modules/protractor';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [DatacallsService]
})
export class UsersComponent implements OnInit {

  posts;
  id;
  brands;
  dataSource;
  categories;
  subcategories;
  checkUsers = [];
  checkUsers2 = [];
  json={};
  data;
  sel=false;
  allsel=false;
  intsel=false;
  jsonPara3 = {};
  jsonPara4 = {};
  countries=[];
  config = new MatSnackBarConfig();
  userForm: FormGroup;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);
 

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private breakpointObserver: BreakpointObserver, private _formBuilder: FormBuilder, private _DatacallsService: DatacallsService, public snackBar: MatSnackBar, private Router: Router, private activatedRoute: ActivatedRoute,public dialog: MatDialog) {
    this.userForm = this._formBuilder.group({
      id: [0],
      password: ['', Validators.required,],
      name: ['', Validators.required],
      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern(/^$|^[0-9]+$/)])],
      address: ['', Validators.required],
      usertype: ['', Validators.required],
      email: ['', [Validators.required, Validators.email,]],
      unique_code: ['', Validators.required],
      zipcode:['',Validators.required],
      brand: ['', Validators.required],
      category: ['', Validators.required],
      sub_category: ['', Validators.required],
      country: ['', Validators.required]
    });
    this.countries = this._DatacallsService.getCountries();
  }

  ngOnInit() {
    this._DatacallsService.get_user_master(null,0).subscribe(posts => {
      this.posts = posts.result;
      console.log('this.posts',this.posts);
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }



  alltogglecheck(b,array){
    if(b==true){
      this.checkUsers2=[];   
      array.forEach(element => {this.checkUsers2.push(element.category_id)})
     
      this.sel=!this.sel
      console.log('all_id',this.checkUsers2);
      console.log('chacked',b);
      
  
    }  
    else {
      this.sel=!this.sel
      this.checkUsers2=[]
      console.log('unchaked',b)
    }
  }

  onchange(a, id) {

    console.log(a)
    console.log(id)

    if (a == true) {
      this.jsonPara3 = { "allow_user": 1, "id": id }
      this._DatacallsService.allow_user(this.jsonPara3).subscribe(posts => {
        this.posts = posts.result

        this.snackBar.open('now this user can sale products on E-commerce portal.', '', {
          duration: 4000
         
        });
      });
    }
    else {

      this.jsonPara4 = { "allow_user": 0, "id": id }
      this._DatacallsService.allow_user(this.jsonPara4).subscribe(posts => {
        this.posts = posts.result

        this.snackBar.open('now this user can not sale products on E-commerce portal.', '', {
          duration: 4000
         
        });
      });
    }
  }
  sendIDS(a, id) {
    if (a == true) {
      console.log('a',a);
      console.log('id',id);
      this.json={
        "id":id
      }

      this.checkUsers.push(this.json)
      console.log('data',this.checkUsers)
    }
    else {
    for(var i=0;i<this.checkUsers.length;i++){
      if(this.checkUsers[i].id==id){
        this.checkUsers.splice(i, 1);
      }
    }  
    }
    //this.shareData(this.checkProduct);
  }
  sendIDS2(a, id) {
    if (a == true) {
      console.log('a1',a);
      console.log('id1',id);
      this.checkUsers2.push(id)
    }
    else {
      const index = this.checkUsers2.indexOf(id);
      if (index !== -1) {
        this.checkUsers2.splice(index, 1);
      }
    }
    //this.shareData(this.checkProduct);
  } 
  shareData(data2) {

    
    var data = {"data": this.checkUsers};

          localStorage.setItem('data', JSON.stringify(data));

    this._DatacallsService.sendAnything2(data2) // to send something
    console.log('data yetoy',data2);

   
  }

  delete(){
    var data={
      "delete_status":1,
      "id":this.checkUsers2
    }
    if(this.checkUsers2.length==0){
      this.snackBar.open('please select atleast one user.', '', {
        duration: 4000

      });
    }
    else{
      this._DatacallsService.DELETEUserMaster(data).subscribe(posts => {
    console.log('checkUsers',this.checkUsers2);
    this.Router.navigate(['deleted-user']);
      });
    }
      
    }
  resetform() {
    this.userForm = this._formBuilder.group({
      id: [0],
      password: ['', Validators.required],
      name: ['', Validators.required],
      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern(/^$|^[0-9]+$/)])],
      address: ['', Validators.required],
      usertype: ['', Validators.required],
      email: ['', [Validators.required, Validators.email,]],
      unique_code: ['', Validators.required],
      zipcode:['',Validators.required],
      country: ['', Validators.required]
    });
    this.id = 0;
  }

  edit(id, usertype) {
    this._DatacallsService.get_user_master(id,0).subscribe(
      data => {
        this.id = data.result[0].id;
        this.userForm.patchValue({
          'id': data.result[0].id,
          'password': data.result[0].password,
          'name': data.result[0].name,
          'mobile_no': data.result[0].mobile_no,
          'address': data.result[0].address,
          'usertype': data.result[0].usertype,
          'email': data.result[0].email,
          'unique_code': data.result[0].unique_code,
          'zipcode':data.result[0].zipcode,
          'brand': data.result[0].brand_id,
          'category': data.result[0].category_id,
          'sub_category': data.result[0].sub_category_id,
          'country': data.result[0].country
        });
      }
    );  
  }

  getSubCategory(category_id) {
    this._DatacallsService.GETSubCategoryMaster(category_id, null,0).subscribe(posts => {
      this.subcategories = posts.result;
    });
  }

  submit() {
    // console.log('tetet',this.userForm.value.brand)
    var bc = this.userForm.value.id;
    var form: any = new FormData();
    form.append("id", this.userForm.value.id)
    form.append("category_id", 0)
    form.append("sub_category_id", 0)
    form.append("brand_id", 0)
    form.append("country", this.userForm.value.country.name)
    form.append("password", this.userForm.value.password)
    form.append("name", this.userForm.value.name)
    form.append("mobile_no", this.userForm.value.mobile_no)
    form.append("address", this.userForm.value.address)
    form.append("usertype", this.userForm.value.usertype)
    form.append("email", this.userForm.value.email)
    form.append("unique_code", this.userForm.value.unique_code)
    form.append("zipcode", this.userForm.value.zipcode)
    form.append("active_status", 1)

    
    this._DatacallsService.POSTUserMaster(form).subscribe(posts => {
      if (posts.result) {
        console.log('possst',posts.result);
        if (bc == 0) {

          if(posts.result[0].insert_user_master==-1){
            this.config.duration = 5000
            this.snackBar.open("Mobile number already exixts", "Done", this.config);
          }
         else if(posts.result[0].insert_user_master==-2){
            this.config.duration = 5000
            this.snackBar.open("Email id already exixts", "Done", this.config);
          }
          else if(posts.result[0].insert_user_master==-3){
            this.config.duration = 5000
            this.snackBar.open("Both Mobile no and Email id already exixts", "Done", this.config);
          }
else{
 
  console.log('possst',bc);
          this.config.duration = 3000
          this.snackBar.open("User Added Successfully", "Done", this.config);
          // this.stripe();
          this.ngOnInit();

}
           
        }
        else {
          console.log('possst',bc);
          this.config.duration = 3000
          this.snackBar.open("User Edited Successfully", "Done", this.config);
          this.ngOnInit();
        }
      }
      else {
        this.snackBar.open("Please try again later", "Done");
      }
    });
    this.resetform();
  }
 
  openDialog(id): void {
    console.log('delete this id',id)
 
    const dialogRef = this.dialog.open(UsersDialog, {
      width: '490px',
      data: id    });
  }

  openDialog2(id): void {
    console.log('delete this id',id)
 
    const dialogRef = this.dialog.open(DetailDialog, {
      width: '850px',
      data: id    });
  }
}


@Component({
  selector: 'UsersDialog',
  templateUrl: 'UsersDialog.html',
})
export class UsersDialog {
  jsonPara2={};
  deleteid;
  postsq;
  posts1=[];
  posts2=[];
  constructor(
    public dialogRef: MatDialogRef<UsersDialog>,
    @Inject(MAT_DIALOG_DATA) public data,private _DatacallsService:DatacallsService,) {

      console.log('data',data);
      var user_id = {
        "user_id":data
      }
      this._DatacallsService.view_users_contact(user_id).subscribe((data)=>{

        this.postsq=data.result;  
  
        console.log('this.',data.result);
    
        })

    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}


@Component({
  selector: 'DetailDialog',
  templateUrl: 'DetailDialog.html',
})
export class DetailDialog {
  jsonPara2={};
  deleteid;
  postsr;
  posts1=[];
  posts2=[];
  constructor(
    public dialogRef: MatDialogRef<DetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data,private _DatacallsService:DatacallsService,) {

      console.log('data',data);
      var user_id = {
        "user_id":data
      }
      this._DatacallsService.view_users_details(user_id).subscribe((data)=>{

        this.postsr=data.result;  
  
        console.log('this.', this.postsr);
    
        })

    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
