import { Component, OnInit,ViewChild } from '@angular/core';
import { DatacallsService } from '../datacalls.service';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig, MatSidenav } from '@angular/material';
@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css'],
  providers: [DatacallsService]
})
export class VehiclesComponent implements OnInit {

  posts;
  totalPosts;
  dataSource;
  posts1;
  posts2;
  posts3;
  json = {};
  make_id;
  model_id;
  sub_model_id;
  searchForm;
  showPosts = 12;
  finished = 'true';
  checkUsers=[];
  public productForm: FormGroup;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _DatacallsService: DatacallsService, private Router: Router,private _fb: FormBuilder, public snackBar: MatSnackBar) {

    this.searchForm = this._fb.group({
   
      make_id: [''],
      model_id: [''],
      sub_model_id: ['']
    });


    
    this.productForm = this._fb.group({

      Excelfile: ['', Validators.required]

  });
   }

  ngOnInit() {
    this._DatacallsService.GETVehicleMaster(null,null,null,null,null,null,null,null,null,0).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    this._DatacallsService.GETMakeMaster(null,0).subscribe(posts => {
      this.posts1 = posts.result;
      console.log('data1',this.posts1)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });


    this._DatacallsService.GETModelMaster(null, null,0).subscribe(posts => {
      this.posts2 = posts.result;
      console.log('data2',this.posts2)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });

    this._DatacallsService.GETSubModelMaster(null, null, null, null,0).subscribe(posts => {
      this.posts3 = posts.result;
      console.log('data3',this.posts3)
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }


  file: File;
  filesToUpload: Array<File>;


  onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }


  UploadExcel(){
    
    console.log(this.filesToUpload)
        var formData: any = new FormData();
      // formData.append("app_id",15); 
   
    this.makeFileRequest("http://13.236.78.106/api/uploadvehicleexcel",formData, this.filesToUpload).then((result) => {
         console.log(result);
          if(result['Success']==true){
             this.snackBar.open("Excel Uploaded Successfully", "Done");
             this.ngOnInit();
      // location.reload();
           }
           
           else {
        
           }
       }, (error) => {
         console.error(error);
       });
   
   }

   makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }



  downloadexcel(){
  
      this._DatacallsService.GETVehicleexcel().subscribe(blob => {
        console.log(blob);
    
         var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="vehicleExcelDownload.xlsx";
                    link.click();
                    
      });
    
}


  sendIDS(a, id) {
    if (a == true) {
      console.log('a1',a);
      console.log('id1',id);
      this.checkUsers.push(id)
    }
    else {
      const index = this.checkUsers.indexOf(id);
      if (index !== -1) {
        this.checkUsers.splice(index, 1);
      }
    }
    //this.shareData(this.checkProduct);
  }
  delete(){
    var data={
      "delete_status":1,
      "vehicle_id":this.checkUsers
    }

    if(this.checkUsers.length==0){
      this.snackBar.open('please select atleast one vehicle.', '', {
        duration: 4000

      }); 
    }
    else{
      this._DatacallsService.DELETEVehicleMaster(data).subscribe(posts => {
    console.log('checkUsers',this.checkUsers);
    this.Router.navigate(['deleted-car']);
      });
    }
      
    }
  // onScrollDown() {
  //   this.finished = 'true'
    
  //   this.showPosts += this.showPosts
  //   this.posts = this.totalPosts.slice(0, this.showPosts);
  //   console.log("scrolled!!", this.posts);
  //   if (this.posts.length == this.totalPosts.length) {
  //     this.finished = 'false'
  //   }
  //   console.log('not', this.showPosts + 'total', this.totalPosts)

  // }
  onSubmit() {

    this.make_id = this.searchForm.value.make_id == '' ? null : this.searchForm.value.make_id;
    this.model_id = this.searchForm.value.model_id == '' ? null : this.searchForm.value.model_id;
    this.sub_model_id = this.searchForm.value.sub_model_id == '' ? null : this.searchForm.value.sub_model_id

    console.log('make_id',this.make_id);
    console.log('model_id',this.model_id);
    console.log('sub_model_id',this.sub_model_id);
   
    this._DatacallsService.GETVehicleMaster(null,this.make_id,this.model_id,null,this.sub_model_id,null,null,null,null,0).subscribe(posts => {
      this.posts = posts.result;
      this.dataSource = new MatTableDataSource(this.posts);
      this.dataSource.sort = this.sort;
      if (this.posts != undefined) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }


  all_products(){
    // location.reload();
    this.ngOnInit()
    {
      this.searchForm.patchValue({
        make_id: [''],
        model_id: [''],
        sub_model_id: ['']
      })
    }
  }


}
